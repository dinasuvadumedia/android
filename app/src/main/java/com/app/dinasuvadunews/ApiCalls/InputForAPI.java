package com.app.dinasuvadunews.ApiCalls;

import android.content.Context;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class InputForAPI {
    JSONObject jsonObject;
    Map<String, String> params;

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    String url;

    HashMap<String, String> headers;
    File file = null;

    Context context;


    public InputForAPI(Context context) {
        this.context = context;
    }

    public JSONObject getJsonObject() {

        if (jsonObject == null) {
            return new JSONObject();
        } else {
            return jsonObject;
        }

    }

    public void setJsonObject(JSONObject jsonObject) {

        this.jsonObject = jsonObject;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public HashMap<String, String> getHeaders() {
        if (headers == null) {
            return new HashMap<String, String>();
        } else {
            return headers;
        }
    }

    public void setHeaders(HashMap<String, String> headers) {
        this.headers = headers;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
