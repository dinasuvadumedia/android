package com.app.dinasuvadunews.ApiCalls;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.dinasuvadunews.Application.AppController;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class ApiCalls {
    private static String TAG = ApiCalls.class.getSimpleName();
    private static int MY_SOCKET_TIMEOUT_MS = 10000;
    private static int RAW_DATA_TIMEOUT = 20000;

    public static void GetMethod(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        final HashMap<String, String> headers = input.getHeaders();
        if (Utilities.isNetworkConnected(context)) {
            Utilities.log(TAG, "url:" + url + "--headers: " + headers.toString());

            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    url, null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            volleyCallback.setDataResponse(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));

                    } else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.authentication_error));

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.server_error));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.network_error));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.parse_error));

                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));
        }

    }


    public static void PostMethods(InputForAPI input, final ResponseHandler volleyCallback) {
        final String url = input.getUrl();
        final Context context = input.getContext();
        final JSONObject params = input.getJsonObject();
        final HashMap<String, String> headers = input.getHeaders();
        Utilities.log("Apicall", params.toString());
        if (Utilities.isNetworkConnected(context)) {

            Utilities.log(TAG, "url:" + url + "--input: " + params + "--headers: " + headers.toString());
            final JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Utilities.log(TAG, "url:" + url + ",response: " + response);

                            volleyCallback.setDataResponse(response);
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Utilities.log(TAG, "url:" + url + ", onErrorResponse: " + error);
                    if (error instanceof TimeoutError || error instanceof NoConnectionError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));

                    } else if (error instanceof AuthFailureError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.authentication_error));

                    } else if (error instanceof ServerError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.server_error));

                    } else if (error instanceof NetworkError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.network_error));

                    } else if (error instanceof ParseError) {

                        volleyCallback.setResponseError(context.getResources().getString(R.string.parse_error));

                    }

                }
            }) {
                @Override
                public Map<String, String> getHeaders() {
                    return headers;
                }
            };

            jsonObjReq.setRetryPolicy(new DefaultRetryPolicy(
                    MY_SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            AppController.getInstance().addToRequestQueue(jsonObjReq);

        } else {
            volleyCallback.setResponseError(context.getResources().getString(R.string.no_internet_connection));
        }
    }


    public interface ResponseHandler {

        void setDataResponse(JSONObject response);


        void setResponseError(String error);

    }


}
