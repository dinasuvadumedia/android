package com.app.dinasuvadunews.ApiCalls;

public class UrlHelpers {

    private static final String BASEURL = "http://13.235.125.196:3000/news";
    public static final String LIST_CATEGORY = BASEURL + "/listCategory";
    public static final String HOME_CATEGORY = BASEURL + "/listHomeCategory";
    public static final String NEWS_API = BASEURL + "/listNews";
    public static final String LIST_VIDEO_CATEGORY = BASEURL + "/listVideoCategory";
    public static final String LIST_VIDEOS = BASEURL + "/listvideos";
    public static final String LIST_VIRAL_CATEGORY = BASEURL + "/listViralCategory";
    public static final String LIST_VIRAL = BASEURL + "/listViralNews";
    public static final String ADD_USER_CATEGORY = BASEURL + "/addUserCategory";
    public static final String GET_NEWS = BASEURL + "/getNews";
    public static final String NEWS_LIKE = BASEURL + "/newsLike";
    public static final String LOGIN = BASEURL + "/login";
    public static final String SIGNUP = BASEURL + "/signup";
    public static final String LIST_COMMENTS = BASEURL + "/listComments";
    public static final String NEWS_COMMENT = BASEURL + "/newsComment";
    public static final String VIRAL_LIKE = BASEURL + "/viralLike";
    public static final String VIDEO_LIKE = BASEURL + "/videoLike";
    public static final String GET_NOTIFICATIONS = BASEURL + "/getNotifications";
    public static final String UPDATE_NOTI_VIEW = BASEURL + "/updateNotificationview";
    public static final String UPDATE_DEV_TOKEN = BASEURL + "/updateDeviceToken";
    public static final String LIST_BLOGS = BASEURL + "/listBlogs";
    public static final String ADD_FOLLOWERS = BASEURL + "/addFollowers";
    public static final String GET_INDIVIDUAL_BLOG = BASEURL + "/getIndividualBloggers";
    public static final String BLOG_LIKE = BASEURL + "/blogLike";
    public static final String BLOG_DETAILS = BASEURL + "/getBlogDetails";
    public static final String SEARCH_NEWS = BASEURL + "/searchNews";
}
