package com.app.dinasuvadunews.Repository;

import android.app.Application;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.app.dinasuvadunews.ApiCalls.ApiCalls;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.Models.AllCategories;
import com.app.dinasuvadunews.Models.AllNewsResponseModel;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsResponseModel;
import com.app.dinasuvadunews.Models.GetBloggersListResponseModel;
import com.app.dinasuvadunews.Models.GetCommentsListlResponseModel;
import com.app.dinasuvadunews.Models.GetNotificationsResponseModel;
import com.app.dinasuvadunews.Models.HomeCategoriesList;
import com.app.dinasuvadunews.Models.LoginResponseModel;
import com.app.dinasuvadunews.Models.MyBloggersDetailsListResponseModel;
import com.app.dinasuvadunews.Models.SingleBlogDetailsListResponseModel;
import com.app.dinasuvadunews.Models.VideosCategoriesList;
import com.app.dinasuvadunews.Models.VideosListResponseModel;
import com.app.dinasuvadunews.Models.ViralCategoriesList;
import com.app.dinasuvadunews.Models.ViralListResponseModel;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

public class CommonFragmentRespository {

    public CommonFragmentRespository(Application application) {
    }

    private LiveData<List<AllCategories>> mSkifiList = new MutableLiveData<>();


    public LiveData<HomeCategoriesList> getHomeCategoriesList(final InputForAPI input) {

        JSONObject jsonObject;
        final MutableLiveData<HomeCategoriesList> homeCategoriesListMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(input, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                HomeCategoriesList allCategories = gson.fromJson(response.toString(), HomeCategoriesList.class);
                homeCategoriesListMutableLiveData.setValue(allCategories);
            }

            @Override
            public void setResponseError(String volleyError) {
                HomeCategoriesList homeCategoriesList = new HomeCategoriesList();
                homeCategoriesList.setError("true");
                homeCategoriesList.setErrorMessage(volleyError);
                homeCategoriesListMutableLiveData.setValue(homeCategoriesList);

            }
        });
        return homeCategoriesListMutableLiveData;

    }

    public LiveData<AllCategories> getAllCategoriesList(final InputForAPI input) {

        JSONObject jsonObject;
        final MutableLiveData<AllCategories> allCategoriesMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(input, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AllCategories allCategories = gson.fromJson(response.toString(), AllCategories.class);
                allCategoriesMutableLiveData.setValue(allCategories);
            }

            @Override
            public void setResponseError(String volleyError) {
                AllCategories allCategories = new AllCategories();
                allCategories.setError("true");
                allCategories.setErrorMessage(volleyError);
                allCategoriesMutableLiveData.setValue(allCategories);

            }
        });
        return allCategoriesMutableLiveData;

    }

    public LiveData<AllNewsResponseModel> getAllNewsList(InputForAPI input) {
        JSONObject jsonObject;
        final MutableLiveData<AllNewsResponseModel> homeFragResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(input, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AllNewsResponseModel allCategories = gson.fromJson(response.toString(), AllNewsResponseModel.class);
                homeFragResponseModelMutableLiveData.setValue(allCategories);
            }

            @Override
            public void setResponseError(String volleyError) {
               /* HomeFragResponseModel registerResponseModel = new HomeFragResponseModel();
                registerResponseModel.setError(true);
                registerResponseModel.setErrorMessage(volleyError);
                homeFragResponseModelMutableLiveData.setValue(registerResponseModel);*/

            }
        });
        return homeFragResponseModelMutableLiveData;
    }

    public LiveData<VideosCategoriesList> getAllVideosCategoriesList(InputForAPI inputs) {

        JSONObject jsonObject;
        final MutableLiveData<VideosCategoriesList> videosCategoriesListMutableLiveData = new MutableLiveData<>();
        ApiCalls.GetMethod(inputs, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                VideosCategoriesList videosCategoriesList = gson.fromJson(response.toString(), VideosCategoriesList.class);
                videosCategoriesListMutableLiveData.setValue(videosCategoriesList);
            }

            @Override
            public void setResponseError(String volleyError) {
                VideosCategoriesList videosCategoriesList = new VideosCategoriesList();
                videosCategoriesList.setError("true");
                videosCategoriesList.setErrorMessage(volleyError);
                videosCategoriesListMutableLiveData.setValue(videosCategoriesList);

            }
        });
        return videosCategoriesListMutableLiveData;
    }

    public LiveData<VideosListResponseModel> getVideosList(InputForAPI inputForAPI) {

        final MutableLiveData<VideosListResponseModel> videosCategoriesListMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                VideosListResponseModel videosListResponseModel = gson.fromJson(response.toString(), VideosListResponseModel.class);
                videosCategoriesListMutableLiveData.setValue(videosListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                VideosListResponseModel videosListResponseModel = new VideosListResponseModel();
                videosListResponseModel.setError("true");
                videosListResponseModel.setErrorMessage(volleyError);
                videosCategoriesListMutableLiveData.setValue(videosListResponseModel);

            }
        });
        return videosCategoriesListMutableLiveData;

    }

    public LiveData<ViralCategoriesList> getAllViralCategoriesList(InputForAPI inputs) {

        JSONObject jsonObject;
        final MutableLiveData<ViralCategoriesList> viralCategoriesListMutableLiveData = new MutableLiveData<>();
        ApiCalls.GetMethod(inputs, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ViralCategoriesList viralCategoriesList = gson.fromJson(response.toString(), ViralCategoriesList.class);
                viralCategoriesListMutableLiveData.setValue(viralCategoriesList);
            }

            @Override
            public void setResponseError(String volleyError) {
                ViralCategoriesList viralCategoriesList = new ViralCategoriesList();
                viralCategoriesList.setError("true");
                viralCategoriesList.setErrorMessage(volleyError);
                viralCategoriesListMutableLiveData.setValue(viralCategoriesList);

            }
        });
        return viralCategoriesListMutableLiveData;
    }


    public LiveData<ViralListResponseModel> getViralList(InputForAPI inputForAPI) {

        final MutableLiveData<ViralListResponseModel> videosCategoriesListMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                ViralListResponseModel viralListResponseModel = gson.fromJson(response.toString(), ViralListResponseModel.class);
                videosCategoriesListMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                ViralListResponseModel viralListResponseModel = new ViralListResponseModel();
                viralListResponseModel.setError("true");
                viralListResponseModel.setErrorMessage(volleyError);
                videosCategoriesListMutableLiveData.setValue(viralListResponseModel);

            }
        });
        return videosCategoriesListMutableLiveData;
    }

    public LiveData<CommonSuccessResponseModel> getAddCategoryResponse(InputForAPI inputForAPI) {
        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;


    }

    public LiveData<GetAllRelatedNewsResponseModel> getAllRelatedNewsList(InputForAPI inputForAPI) {
        final MutableLiveData<GetAllRelatedNewsResponseModel> getAllRelatedNewsResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetAllRelatedNewsResponseModel viralListResponseModel = gson.fromJson(response.toString(), GetAllRelatedNewsResponseModel.class);
                getAllRelatedNewsResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetAllRelatedNewsResponseModel getAllRelatedNewsResponseModel = new GetAllRelatedNewsResponseModel();
                getAllRelatedNewsResponseModel.setError("true");
                getAllRelatedNewsResponseModel.setErrorMessage(volleyError);
                getAllRelatedNewsResponseModelMutableLiveData.setValue(getAllRelatedNewsResponseModel);

            }
        });
        return getAllRelatedNewsResponseModelMutableLiveData;

    }

    public LiveData<CommonSuccessResponseModel> getLikeNewsResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;

    }

    public LiveData<CommonSuccessResponseModel> getVideoLikeNewsResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;

    }


    public LiveData<LoginResponseModel> getLoginResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<LoginResponseModel> loginResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                LoginResponseModel loginResponseModel = gson.fromJson(response.toString(), LoginResponseModel.class);
                loginResponseModelMutableLiveData.setValue(loginResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                LoginResponseModel loginResponseModel = new LoginResponseModel();
                loginResponseModel.setError("true");
                loginResponseModel.setErrorMessage(volleyError);
                loginResponseModelMutableLiveData.setValue(loginResponseModel);

            }
        });
        return loginResponseModelMutableLiveData;
    }

    public LiveData<LoginResponseModel> getRegisterResponseData(InputForAPI inputForAPI) {
        final MutableLiveData<LoginResponseModel> loginResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                LoginResponseModel loginResponseModel = gson.fromJson(response.toString(), LoginResponseModel.class);
                loginResponseModelMutableLiveData.setValue(loginResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                LoginResponseModel loginResponseModel = new LoginResponseModel();
                loginResponseModel.setError("true");
                loginResponseModel.setErrorMessage(volleyError);
                loginResponseModelMutableLiveData.setValue(loginResponseModel);

            }
        });
        return loginResponseModelMutableLiveData;


    }

    public LiveData<GetCommentsListlResponseModel> getCommentsListResponseData(InputForAPI inputForAPI) {
        final MutableLiveData<GetCommentsListlResponseModel> getCommentsListlResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetCommentsListlResponseModel loginResponseModel = gson.fromJson(response.toString(), GetCommentsListlResponseModel.class);
                getCommentsListlResponseModelMutableLiveData.setValue(loginResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetCommentsListlResponseModel getCommentsListlResponseModel = new GetCommentsListlResponseModel();
                getCommentsListlResponseModel.setError("true");
                getCommentsListlResponseModel.setErrorMessage(volleyError);
                getCommentsListlResponseModelMutableLiveData.setValue(getCommentsListlResponseModel);

            }
        });
        return getCommentsListlResponseModelMutableLiveData;

    }

    public LiveData<CommonSuccessResponseModel> getSendCommentResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;

    }

    public LiveData<GetNotificationsResponseModel> getNotificationsList(InputForAPI inputForAPI) {

        final MutableLiveData<GetNotificationsResponseModel> getNotificationsResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetNotificationsResponseModel getNotificationsResponseModel = gson.fromJson(response.toString(), GetNotificationsResponseModel.class);
                getNotificationsResponseModelMutableLiveData.setValue(getNotificationsResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetNotificationsResponseModel getNotificationsResponseModel = new GetNotificationsResponseModel();
                getNotificationsResponseModel.setError("true");
                getNotificationsResponseModel.setErrorMessage(volleyError);
                getNotificationsResponseModelMutableLiveData.setValue(getNotificationsResponseModel);

            }
        });
        return getNotificationsResponseModelMutableLiveData;


    }

    public LiveData<CommonSuccessResponseModel> updateNotificationView(InputForAPI inputForAPI) {

        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;
    }

    public LiveData<CommonSuccessResponseModel> getDeviceTokenResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<CommonSuccessResponseModel> commonSuccessResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                CommonSuccessResponseModel viralListResponseModel = gson.fromJson(response.toString(), CommonSuccessResponseModel.class);
                commonSuccessResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                CommonSuccessResponseModel commonSuccessResponseModel = new CommonSuccessResponseModel();
                commonSuccessResponseModel.setError("true");
                commonSuccessResponseModel.setErrorMessage(volleyError);
                commonSuccessResponseModelMutableLiveData.setValue(commonSuccessResponseModel);

            }
        });
        return commonSuccessResponseModelMutableLiveData;


    }

    public LiveData<GetBloggersListResponseModel> getBloggersListResponseData(InputForAPI inputForAPI) {
        final MutableLiveData<GetBloggersListResponseModel> getBloggersListResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetBloggersListResponseModel viralListResponseModel = gson.fromJson(response.toString(), GetBloggersListResponseModel.class);
                getBloggersListResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetBloggersListResponseModel getBloggersListResponseModel = new GetBloggersListResponseModel();
                getBloggersListResponseModel.setError("true");
                getBloggersListResponseModel.setErrorMessage(volleyError);
                getBloggersListResponseModelMutableLiveData.setValue(getBloggersListResponseModel);

            }
        });
        return getBloggersListResponseModelMutableLiveData;


    }

    public LiveData<GetBloggersListResponseModel> getAdddFollowResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<GetBloggersListResponseModel> getBloggersListResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                GetBloggersListResponseModel viralListResponseModel = gson.fromJson(response.toString(), GetBloggersListResponseModel.class);
                getBloggersListResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                GetBloggersListResponseModel getBloggersListResponseModel = new GetBloggersListResponseModel();
                getBloggersListResponseModel.setError("true");
                getBloggersListResponseModel.setErrorMessage(volleyError);
                getBloggersListResponseModelMutableLiveData.setValue(getBloggersListResponseModel);

            }
        });
        return getBloggersListResponseModelMutableLiveData;

    }


    public LiveData<MyBloggersDetailsListResponseModel> getBlogDetailsResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<MyBloggersDetailsListResponseModel> getBloggersListResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                MyBloggersDetailsListResponseModel viralListResponseModel = gson.fromJson(response.toString(), MyBloggersDetailsListResponseModel.class);
                getBloggersListResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                MyBloggersDetailsListResponseModel myBloggersDetailsListResponseModel = new MyBloggersDetailsListResponseModel();
                myBloggersDetailsListResponseModel.setError("true");
                myBloggersDetailsListResponseModel.setErrorMessage(volleyError);
                getBloggersListResponseModelMutableLiveData.setValue(myBloggersDetailsListResponseModel);

            }
        });
        return getBloggersListResponseModelMutableLiveData;

    }


    public LiveData<SingleBlogDetailsListResponseModel> getSingleBlogDetailsResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<SingleBlogDetailsListResponseModel> getBloggersListResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                SingleBlogDetailsListResponseModel viralListResponseModel = gson.fromJson(response.toString(), SingleBlogDetailsListResponseModel.class);
                getBloggersListResponseModelMutableLiveData.setValue(viralListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                SingleBlogDetailsListResponseModel singleBlogDetailsListResponseModel = new SingleBlogDetailsListResponseModel();
                singleBlogDetailsListResponseModel.setError("true");
                singleBlogDetailsListResponseModel.setErrorMessage(volleyError);
                getBloggersListResponseModelMutableLiveData.setValue(singleBlogDetailsListResponseModel);

            }
        });
        return getBloggersListResponseModelMutableLiveData;

    }

    public LiveData<AllNewsResponseModel> getMySearchListResponseData(InputForAPI inputForAPI) {

        final MutableLiveData<AllNewsResponseModel> getBloggersListResponseModelMutableLiveData = new MutableLiveData<>();
        ApiCalls.PostMethods(inputForAPI, new ApiCalls.ResponseHandler() {
            @Override
            public void setDataResponse(JSONObject response) {
                Gson gson = new Gson();
                AllNewsResponseModel mySearchListResponseModel = gson.fromJson(response.toString(), AllNewsResponseModel.class);
                getBloggersListResponseModelMutableLiveData.setValue(mySearchListResponseModel);
            }

            @Override
            public void setResponseError(String volleyError) {
                AllNewsResponseModel allNewsResponseModel = new AllNewsResponseModel();
                allNewsResponseModel.setError("true");
                allNewsResponseModel.setErrorMessage(volleyError);
                getBloggersListResponseModelMutableLiveData.setValue(allNewsResponseModel);

            }
        });
        return getBloggersListResponseModelMutableLiveData;

    }


}
