package com.app.dinasuvadunews.interfaces;

import android.widget.ImageView;


import com.app.dinasuvadunews.Models.VideosListDetailsResponseModel;

import java.util.List;

public interface CommonClickListenerForVideos {

    void onImgClick(int position, List<VideosListDetailsResponseModel> videosListDetailsResponseModels);

    void onLikeClick(int posi, List<VideosListDetailsResponseModel> videosListDetailsResponseModels);

    void onDownloadClick(int pos, List<VideosListDetailsResponseModel> videosListDetailsResponseModels);

    void onShareClick(int pos, List<VideosListDetailsResponseModel> videosListDetailsResponseModels);

    void onShareClickWithImg(int pos, ImageView imageView, List<VideosListDetailsResponseModel> videosListDetailsResponseModels);

}
