package com.app.dinasuvadunews.Interfaces;

import android.widget.ImageView;

public interface CommonClickListenerWithTwo {

    void onImgClick(int position);

    void onLikeClick(int posi);

    void onDownloadClick(int pos);

    void onShareClick(int pos);

    void onShareClickWithImg(int pos, ImageView imageView);

}
