package com.app.dinasuvadunews.interfaces;

import android.widget.ImageView;

import com.app.dinasuvadunews.Models.ViralListDetailsResponseModel;

import java.util.List;

public interface CommonClickListenerForViral {

    void onImgClick(int position, List<ViralListDetailsResponseModel> viralListDetailsResponseModels);

    void onLikeClick(int posi, List<ViralListDetailsResponseModel> viralListDetailsResponseModels);

    void onDownloadClick(int pos, List<ViralListDetailsResponseModel> viralListDetailsResponseModels);

    void onShareClick(int pos, List<ViralListDetailsResponseModel> viralListDetailsResponseModels);

    void onShareClickWithImg(int pos, ImageView imageView, List<ViralListDetailsResponseModel> viralListDetailsResponseModels);

}
