package com.app.dinasuvadunews.Interfaces;

public interface CommonClickListenerForShare {

    void clickListenerShare(String title, String imgUrl, String id, String catgId);
}
