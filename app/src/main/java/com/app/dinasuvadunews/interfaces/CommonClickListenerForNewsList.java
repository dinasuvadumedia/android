package com.app.dinasuvadunews.Interfaces;

public interface CommonClickListenerForNewsList {

    void clickListenerForNewsList(int position, String imgUrl, String content, String newsIs, String catgId);
}
