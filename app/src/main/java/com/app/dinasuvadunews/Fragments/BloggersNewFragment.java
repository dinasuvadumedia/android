package com.app.dinasuvadunews.Fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.dinasuvadunews.Activities.BloggerProfileActivity;
import com.app.dinasuvadunews.Activities.BloggersListActivity;
import com.app.dinasuvadunews.Activities.BloggersPostListActivity;
import com.app.dinasuvadunews.Activities.MainActivity;
import com.app.dinasuvadunews.Activities.NotificationActivity;
import com.app.dinasuvadunews.Adapters.BloggersListAdapter;
import com.app.dinasuvadunews.Adapters.PopularBloggerListAdapterNew;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.BuildConfig;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerWithTwo;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetBloggersListResponseModel;
import com.app.dinasuvadunews.Models.OurBloggersListResponseModel;
import com.app.dinasuvadunews.Models.PopularBloggersListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BloggersNewFragment extends Fragment {


    @BindView(R.id.bloggersListRecyclerView)
    RecyclerView bloggersListRecyclerView;
    @BindView(R.id.allBloggerList)
    TextView allBloggerList;
    @BindView(R.id.popularBloggersListRecyclerView)
    RecyclerView popularBloggersListRecyclerView;

    TextView allTxtView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    BloggersListAdapter bloggersListAdapter;
    PopularBloggerListAdapterNew popularBloggerListAdapter;

    CommonViewModel commonViewModel;
    public static List<OurBloggersListResponseModel> bloggersList;

    private ImageView sharingImgView;
    private String shareTitle = "";

    @BindView(R.id.navigationIcon)
    ImageView navigationIcon;

    @BindView(R.id.notificationLayout)
    RelativeLayout notificationLayout;

    public BloggersNewFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bloggers, container, false);
        ButterKnife.bind(this, view);


        initViews();
        setClickListener();
        getBloggersList();
        swipeRefreshLayoutListener();
        return view;
    }

    private void setClickListener() {
        allBloggerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), BloggersListActivity.class));
            }
        });

        navigationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });

    }

    private void swipeRefreshLayoutListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getBloggersList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void getBloggersList() {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_BLOGS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(getActivity()).getUdid());
            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAllBloggersList(inputForAPI).observe(this, new Observer<GetBloggersListResponseModel>() {
            @Override
            public void onChanged(@Nullable GetBloggersListResponseModel getBloggersListResponseModel) {

                if (getBloggersListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    bloggersList = getBloggersListResponseModel.getBloggersList();
                    setOurBloggersList(getBloggersListResponseModel.getBloggersList());

                    setPopularBloggersList(getBloggersListResponseModel.getPopularBlogs());
                    //setAllBloggersList(getBloggersListResponseModel.getAllBlogger());
                } else {
                    Utilities.showToast(getActivity(), getBloggersListResponseModel.getErrorMessage());
                }
            }

        });

    }


    private void setOurBloggersList(List<OurBloggersListResponseModel> bloggersList) {
        bloggersListAdapter = new BloggersListAdapter(getActivity(), bloggersList);
        bloggersListRecyclerView.setAdapter(bloggersListAdapter);


        bloggersListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                startActivity(new Intent(getActivity(), BloggersPostListActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, bloggersList.get(position).getBloggerId()));

            }
        });
    }

    private void initViews() {


        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);

        bloggersListRecyclerView.addItemDecoration(new SpacesItemDecoration(30, 2));

    }


    private void setPopularBloggersList(List<PopularBloggersListResponseModel> popularBlogs) {


        popularBloggerListAdapter = new PopularBloggerListAdapterNew(getActivity(), popularBlogs);
        popularBloggersListRecyclerView.setAdapter(popularBloggerListAdapter);

        popularBloggerListAdapter.setOnClickListener(new CommonClickListenerWithTwo() {
            @Override
            public void onImgClick(int position) {

                startActivity(new Intent(getActivity(), BloggersPostListActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, popularBlogs.get(position).getBloggerId()));
            }

            @Override
            public void onLikeClick(int pos) {
                int likeCounts = popularBlogs.get(pos).getLikesCount();

                int likeVal = 0;

                if (popularBlogs.get(pos).getIsLiked() == 0) {
                    likeVal = 1;
                    popularBlogs.get(pos).setIsLiked(1);
                    likeCounts++;
                    popularBlogs.get(pos).setLikesCount(likeCounts);
                    Utilities.playLikeSound(getActivity());
                } else {
                    likeVal = 0;
                    likeCounts--;
                    popularBlogs.get(pos).setLikesCount(likeCounts);
                    popularBlogs.get(pos).setIsLiked(0);
                }

                popularBloggerListAdapter.reloadList(popularBlogs);

                sendLikeVal(popularBlogs.get(pos).getBlogId(), likeVal);


            }

            @Override
            public void onDownloadClick(int pos) { // go to blog details page

                startActivity(new Intent(getActivity(), BloggerProfileActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, popularBlogs.get(pos).getBlogId()));


            }

            @Override
            public void onShareClick(int pos) {

                String myDynamicUri = Utilities.generateCustomUriForBlog(Constants.INTENT_VALUES.SHARE_BLOG_TYPE, popularBlogs.get(pos).getBloggerId());
                Utilities.generateDynamicLinkForExternalShare(getActivity(), myDynamicUri, popularBlogs.get(pos).getImages(), popularBlogs.get(pos).getTitle());

            }

            @Override
            public void onShareClickWithImg(int pos, ImageView imageView) {

                // Whatsapp share

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    sharingImgView = imageView;
                    shareTitle = popularBlogs.get(pos).getTitle();
                    getPermissions();
                } else {
                    sharingImgView = imageView;
                    Utilities.sendImageAsMsgToWhatsapp(getActivity(), sharingImgView, popularBlogs.get(pos).getTitle());
                }

            }
        });

    }

    private void sendLikeVal(String blogId, int likeVal) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.BLOG_ID, blogId);
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(getActivity()).getUdid());
            jsonObject.put(Constants.API_PARAMS.IS_LIKED, likeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.BLOG_LIKE);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getLikeNewsResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    // Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                } else
                    Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
            }

        });

    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getPermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.sendImageAsMsgToWhatsapp(getActivity(), sharingImgView, shareTitle);
                } else {
                    showAlertDialog();
                }
                break;
        }

    }


    private void showAlertDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getResources().getString(R.string.requires_per));
        alertDialogBuilder.setMessage(getResources().getString(R.string.need_storage_per));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                        startActivity(i);

                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.cancel();

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
