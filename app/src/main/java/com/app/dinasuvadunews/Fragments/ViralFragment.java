package com.app.dinasuvadunews.Fragments;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.dinasuvadunews.Adapters.ViralMainListAdapter;
import com.app.dinasuvadunews.Adapters.ViralTitlesListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.BuildConfig;
import com.app.dinasuvadunews.DownloadFiles.DownloadFiles;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.ViralCategoriesDetailsList;
import com.app.dinasuvadunews.Models.ViralCategoriesList;
import com.app.dinasuvadunews.Models.ViralListDetailsResponseModel;
import com.app.dinasuvadunews.Models.ViralListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;
import com.app.dinasuvadunews.interfaces.CommonClickListenerForViral;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ViralFragment extends Fragment {


    @BindView(R.id.viralTitlesRecyclerView)
    RecyclerView viralTitlesRecyclerView;

    @BindView(R.id.mainViralsRecyclerView)
    RecyclerView mainViralsRecyclerView;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    ViralTitlesListAdapter viralTitlesListAdapter;
    ViralMainListAdapter viralMainListAdapter;
    AppSettings appSettings;
    CommonViewModel commonViewModel;
    private int currentPageNum = 1;
    private Dialog showViralImagedialog;
    private ImageView closeDialogImgView, viralImgView;
    private String downloadImageUrl = "";
    private boolean isDownload = false;
    private ImageView sharingImgView;
    private String shareTitle;
    private LinearLayoutManager linearLayoutManagerVertical;
    private int totalPages = 0;
    private String currentCategoryId = "";

    public ViralFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_viral, container, false);
        ButterKnife.bind(this, view);


        initViews();
        getViralTitleList();
        swipeRefreshLayoutListener();
        setRecyclerScrollListener();

        return view;
    }

    private void setRecyclerScrollListener() {

        mainViralsRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (viralMainListAdapter != null) {

                        if (totalPages > 1) {
                            if (viralMainListAdapter.getItemCount() > 0 && linearLayoutManagerVertical.findLastVisibleItemPosition() >= viralMainListAdapter.getItemCount() - 1) {
                                try {
                                    if (totalPages == currentPageNum) {
                                    } else {
                                        currentPageNum = currentPageNum + 1;
                                        getViralList(currentCategoryId);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                }
            }
        });

    }

    private void swipeRefreshLayoutListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getViralTitleList();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void getViralTitleList() {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_VIRAL_CATEGORY);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);


        commonViewModel.getAllViralCategories(inputForAPI).observe(this, new Observer<ViralCategoriesList>() {
            @Override
            public void onChanged(@Nullable ViralCategoriesList viralCategoriesList) {

                if (viralCategoriesList.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    if (viralCategoriesList != null) {
                        if (viralCategoriesList.getListCategory().size() > 0) {

                            viralCategoriesList.getListCategory().get(0).setSelected(true);
                            setViralTitlesAdapter(viralCategoriesList.getListCategory());
                            currentCategoryId = viralCategoriesList.getListCategory().get(0).getCategoryId();
                            getViralList(viralCategoriesList.getListCategory().get(0).getCategoryId());
                        }
                    }
                } else {
                    Utilities.showToast(getActivity(), viralCategoriesList.getErrorMessage());
                }
            }

        });

    }

    private void getViralList(String categoryId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.CATEGORY_ID, categoryId);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.PAGE_NUMBER, currentPageNum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_VIRAL);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getViralList(inputForAPI).observe(this, new Observer<ViralListResponseModel>() {
            @Override
            public void onChanged(@Nullable ViralListResponseModel viralListResponseModel) {

                if (viralListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    totalPages = viralListResponseModel.getTotalPage();
                    setViralMainAdapter(viralListResponseModel.getListViral());
//                    hideLoading();
                } else {
                    Utilities.showToast(getActivity(), viralListResponseModel.getErrorMessage());
                }
            }

        });

    }


    private void setViralTitlesAdapter(final List<ViralCategoriesDetailsList> listCategory) {
        viralTitlesListAdapter = new ViralTitlesListAdapter(getActivity(), listCategory);
        viralTitlesRecyclerView.setAdapter(viralTitlesListAdapter);

        viralTitlesListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {
                currentPageNum = 1;

                currentCategoryId = listCategory.get(position).getCategoryId();
                getViralList(listCategory.get(position).getCategoryId());

            }
        });

    }


    private void initViews() {

        linearLayoutManagerVertical = new LinearLayoutManager(getActivity());
        mainViralsRecyclerView.setLayoutManager(linearLayoutManagerVertical);

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        viralTitlesRecyclerView.addItemDecoration(new SpacesItemDecoration(20, 2));


        showViralImagedialog = Utilities.showImageViewDialog(getActivity());
        closeDialogImgView = showViralImagedialog.findViewById(R.id.closeDialogImgView);
        viralImgView = showViralImagedialog.findViewById(R.id.viralImgView);

        appSettings = new AppSettings(getActivity());
    }


    private void setViralMainAdapter(List<ViralListDetailsResponseModel> listViral) {

        if (listViral.size() > 0) {
            if (currentPageNum == 1) {
                viralMainListAdapter = new ViralMainListAdapter(getActivity(), listViral);
                mainViralsRecyclerView.setAdapter(viralMainListAdapter);
                mainViralsRecyclerView.setHasFixedSize(true);
                mainViralsRecyclerView.setItemViewCacheSize(20);
            } else {
                viralMainListAdapter.notifyList(listViral);
            }

            viralMainListAdapter.setOnClickListener(new CommonClickListenerForViral() {
                @Override
                public void onImgClick(int position, List<ViralListDetailsResponseModel> listViral) {

                    new GlideImageLoader(getActivity()).loadSingleImgUrl(getActivity(), listViral.get(position).getUrlToViral(), viralImgView);
                    showViralImagedialog.show();

                    closeDialogImgView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            showViralImagedialog.dismiss();
                        }
                    });

                }

                @Override
                public void onLikeClick(int pos, List<ViralListDetailsResponseModel> listViral) {
                    int likeCounts = listViral.get(pos).getLikeCount();

                    int likeVal = 0;

                    if (listViral.get(pos).getIsLiked() == 0) {
                        likeVal = 1;
                        listViral.get(pos).setIsLiked(1);
                        likeCounts++;
                        listViral.get(pos).setLikeCount(likeCounts);
                        Utilities.playLikeSound(getActivity());
                    } else {
                        likeVal = 0;
                        likeCounts--;
                        listViral.get(pos).setLikeCount(likeCounts);
                        listViral.get(pos).setIsLiked(0);
                    }

                    viralMainListAdapter.reloadList(listViral);

                    sendLikeVal(listViral.get(pos).getId(), likeVal);
                }


                @Override
                public void onDownloadClick(int pos, List<ViralListDetailsResponseModel> listViral) {

                    isDownload = true;
                    downloadImageUrl = listViral.get(pos).getUrlToViral();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        getPermissions();
                    } else {

                        if (downloadImageUrl.equalsIgnoreCase("") || downloadImageUrl.equalsIgnoreCase("null"))
                            Utilities.showToast(getActivity(), getString(R.string.image_not_available));
                        else
                            downloadImage(downloadImageUrl);
                    }

                }

                @Override
                public void onShareClick(int pos, List<ViralListDetailsResponseModel> listViral) {


                    // Utilities.shareInWhatsappDialog(getActivity(), listViral.get(pos).getUrlToViral());

                    String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_VIRAL_TYPE, listViral.get(pos).getUrlToViral(), listViral.get(pos).getId(), listViral.get(pos).getCategoryId());
                    Utilities.generateDynamicLinkForWhatsappShare(getActivity(), myDynamicUri, listViral.get(pos).getUrlToViral(), listViral.get(pos).getTitle());

                }

                @Override
                public void onShareClickWithImg(int pos, ImageView imageView, List<ViralListDetailsResponseModel> listViral) {

                    isDownload = false;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        sharingImgView = imageView;
                        shareTitle = listViral.get(pos).getTitle();
                        getPermissions();
                    } else {
                        Utilities.sendImageAsMsgToWhatsapp(getActivity(), imageView, listViral.get(pos).getTitle());
                    }

                }

            });
        }
    }


    private void getPermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void downloadImage(String downloadImageUrl) {
        new DownloadFiles(getActivity(), downloadImageUrl, 0);
    }

    private void sendLikeVal(String id, int likeVal) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.VIRAL_ID, id);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.IS_LIKED, likeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.VIRAL_LIKE);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getLikeNewsResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    // Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                } else
                    Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
            }

        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    if (isDownload)
                        downloadImage(downloadImageUrl);
                    else
                        Utilities.sendImageAsMsgToWhatsapp(getActivity(), sharingImgView, shareTitle);

                } else {
                    showAlertDialog();
                }
                break;
        }

    }

    private void showAlertDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getResources().getString(R.string.requires_per));
        alertDialogBuilder.setMessage(getResources().getString(R.string.need_storage_per));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                        startActivity(i);

                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.cancel();

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
