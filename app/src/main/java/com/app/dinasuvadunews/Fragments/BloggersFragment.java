/*
package com.app.dinasuvadu.Fragments;


import android.content.Intent;
import android.os.Bundle;


import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.dinasuvadu.Activities.BloggersListActivity;
import com.app.dinasuvadu.Activities.BloggersPostListActivity;
import com.app.dinasuvadu.Adapters.AllBloggerListAdapter;
import com.app.dinasuvadu.Adapters.AllBloggerListAdapterNew;
import com.app.dinasuvadu.Adapters.BloggersListAdapter;
import com.app.dinasuvadu.Adapters.PopularBloggerListAdapter;
import com.app.dinasuvadu.Adapters.PopularBloggerListAdapterNew;
import com.app.dinasuvadu.ApiCalls.InputForAPI;
import com.app.dinasuvadu.ApiCalls.UrlHelpers;
import com.app.dinasuvadu.Helpers.AppSettings;
import com.app.dinasuvadu.Helpers.Constants;
import com.app.dinasuvadu.Helpers.SpacesItemDecoration;
import com.app.dinasuvadu.Helpers.Utilities;
import com.app.dinasuvadu.Interfaces.CommonClickListener;
import com.app.dinasuvadu.Models.AllBloggersListResponseModel;
import com.app.dinasuvadu.Models.GetBloggersListResponseModel;
import com.app.dinasuvadu.Models.OurBloggersListResponseModel;
import com.app.dinasuvadu.Models.PopularBloggersListResponseModel;
import com.app.dinasuvadu.R;
import com.app.dinasuvadu.ViewModel.CommonViewModel;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BloggersFragment extends Fragment {


    @BindView(R.id.bloggersListRecyclerView)
    RecyclerView bloggersListRecyclerView;
    @BindView(R.id.allBloggerList)
    TextView allBloggerList;
    @BindView(R.id.popularBloggersListRecyclerView)
    RecyclerView popularBloggersListRecyclerView;
    @BindView(R.id.allBloggersListRecyclerView)
    RecyclerView allBloggersListRecyclerView;
    @BindView(R.id.popularLayout)
    LinearLayout popularLayout;
    @BindView(R.id.allLayout)
    LinearLayout allLayout;
    @BindView(R.id.popularTxtView)
    TextView popularTxtView;
    @BindView(R.id.allIndicator)
    View allIndicator;
    @BindView(R.id.popularIndicator)
    View popularIndicator;
    @BindView(R.id.allTxtView)
    TextView allTxtView;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    BloggersListAdapter bloggersListAdapter;
    PopularBloggerListAdapterNew popularBloggerListAdapter;
    AllBloggerListAdapterNew allBloggerListAdapter;

    CommonViewModel commonViewModel;
    public static List<OurBloggersListResponseModel> bloggersList;
    private ArrayList<ArrayList<PopularBloggersListResponseModel>> popularListForAdapter = new ArrayList<>();
    private ArrayList<ArrayList<AllBloggersListResponseModel>> allListForAdapter = new ArrayList<>();

    public BloggersFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_bloggers, container, false);
        ButterKnife.bind(this, view);


        initViews();
        setClickListener();
        getBloggersList();
        swipeRefreshLayoutListener();
        return view;
    }

    private void swipeRefreshLayoutListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getBloggersList();
                showSelectedPopular();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }


    private void getBloggersList() {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_BLOGS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(getActivity()).getUdid());
            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAllBloggersList(inputForAPI).observe(this, new Observer<GetBloggersListResponseModel>() {
            @Override
            public void onChanged(@Nullable GetBloggersListResponseModel getBloggersListResponseModel) {

                if (getBloggersListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    bloggersList = getBloggersListResponseModel.getBloggersList();
                    setOurBloggersList(getBloggersListResponseModel.getBloggersList());

                    setPopularBloggersList(getBloggersListResponseModel.getPopularBlogs());
                    setAllBloggersList(getBloggersListResponseModel.getAllBlogger());
                } else {
                    Utilities.showToast(getActivity(), getBloggersListResponseModel.getErrorMessage());
                }
            }

        });

    }

    private void setClickListener() {

        allBloggerList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), BloggersListActivity.class).putExtra("bloggersListData", (Serializable) bloggersList));
            }
        });

        popularLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSelectedPopular();
            }
        });


        allLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                popularIndicator.setVisibility(View.GONE);
                allIndicator.setVisibility(View.VISIBLE);
                allTxtView.setAlpha(1);

                popularTxtView.setAlpha(0.4f);

                popularBloggersListRecyclerView.setVisibility(View.GONE);
                allBloggersListRecyclerView.setVisibility(View.VISIBLE);

            }
        });
    }

    private void showSelectedPopular() {

        popularIndicator.setVisibility(View.VISIBLE);
        allIndicator.setVisibility(View.GONE);
        popularTxtView.setAlpha(1);

        allTxtView.setAlpha(0.4f);

        popularBloggersListRecyclerView.setVisibility(View.VISIBLE);
        allBloggersListRecyclerView.setVisibility(View.GONE);

    }

    private void setOurBloggersList(List<OurBloggersListResponseModel> bloggersList) {
        bloggersListAdapter = new BloggersListAdapter(getActivity(), bloggersList);
        bloggersListRecyclerView.setAdapter(bloggersListAdapter);


        bloggersListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                startActivity(new Intent(getActivity(), BloggersPostListActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, bloggersList.get(position).getBloggerId()));

            }
        });
    }

    private void initViews() {

        bloggersListRecyclerView.setFocusable(false);
        bloggersListRecyclerView.setNestedScrollingEnabled(false);


        popularBloggersListRecyclerView.setFocusable(false);
        popularBloggersListRecyclerView.setNestedScrollingEnabled(false);


        allBloggersListRecyclerView.setFocusable(false);
        allBloggersListRecyclerView.setNestedScrollingEnabled(false);


        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);

        bloggersListRecyclerView.addItemDecoration(new SpacesItemDecoration(30, 2));

    }


    private void setPopularBloggersList(List<PopularBloggersListResponseModel> popularBlogs) {

        ArrayList<PopularBloggersListResponseModel> popularBloggersListResponseModels = new ArrayList<>();
        int i = 0;
        int j = 0;

        while (i < popularBlogs.size()) {

            if (j < 3) {
                popularBloggersListResponseModels.add(popularBlogs.get(i));
                i++;
                j++;
            } else {
                popularListForAdapter.add(popularBloggersListResponseModels);
                popularBloggersListResponseModels = new ArrayList<>();
                j = 0;
            }

        }


        popularBloggerListAdapter = new PopularBloggerListAdapterNew(getActivity(), popularListForAdapter);
        popularBloggersListRecyclerView.setAdapter(popularBloggerListAdapter);

        popularBloggerListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                startActivity(new Intent(getActivity(), BloggersPostListActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, popularBlogs.get(position).getBloggerId()));
            }
        });

    }


    private void setAllBloggersList(List<AllBloggersListResponseModel> allBlogger) {

        ArrayList<AllBloggersListResponseModel> allBloggersListResponseModels = new ArrayList<>();
        int i = 0;
        int j = 0;

        while (i < allBlogger.size()) {

            if (j < 3) {
                allBloggersListResponseModels.add(allBlogger.get(i));
                i++;
                j++;
            } else {
                allListForAdapter.add(allBloggersListResponseModels);
                allBloggersListResponseModels = new ArrayList<>();
                j = 0;
            }

        }


        allBloggerListAdapter = new AllBloggerListAdapterNew(getActivity(), allListForAdapter);
        allBloggersListRecyclerView.setAdapter(allBloggerListAdapter);

        allBloggerListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                startActivity(new Intent(getActivity(), BloggersPostListActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, allBlogger.get(position).getBloggerId()));
            }
        });

    }

}
*/
