package com.app.dinasuvadunews.Fragments;

import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.R;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

import net.alexandroid.utils.exoplayerhelper.ExoPlayerHelper;


public class TrendingNewsListFragment extends Fragment {

    private static final String imageUrlFrag = "imageUrl";
    private static final String TAG = TrendingNewsListFragment.class.getSimpleName();
    public static ExoPlayerHelper mExoPlayerHelper;

    public static TrendingNewsListFragment newInstance(String imgUrl) {
        TrendingNewsListFragment fragment = new TrendingNewsListFragment();
        Bundle args = new Bundle();
        args.putString(imageUrlFrag, imgUrl);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.trending_news_item, container, false);

        ImageView trendingImgView = rootView.findViewById(R.id.trendingImgView);
        SimpleExoPlayerView exoPlayerView = rootView.findViewById(R.id.exoPlayerView);

        String image = getArguments().getString(imageUrlFrag);

        trendingImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExoPlayerHelper != null) {
                    if (mExoPlayerHelper.isPlayerCreated()) {
                        mExoPlayerHelper.playerPause();
                    }
                }

                trendingImgView.setVisibility(View.GONE);
                exoPlayerView.setVisibility(View.VISIBLE);
                mExoPlayerHelper = new ExoPlayerHelper.Builder(getActivity(), exoPlayerView)
                        .addMuteButton(false, false)
                        .setUiControllersVisibility(true)
                        .setAutoPlayOn(true).setRepeatModeOn(false)
                        .setRepeatModeOn(false)
                        .setAutoPlayOn(false)
                        .setVideoUrls("http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4")
                        .enableLiveStreamSupport()
                        .addProgressBarWithColor(getResources().getColor(R.color.colorPrimary))
                        .createAndPrepare();

                mExoPlayerHelper.playerPlay();


            }
        });

        new GlideImageLoader(getActivity()).loadSingleImgUrl(getActivity(), "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerFun.mp4", trendingImgView);

        return rootView;
    }
}

