package com.app.dinasuvadunews.Fragments;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.dinasuvadunews.Activities.CategoriesActivity;
import com.app.dinasuvadunews.Activities.MainActivity;
import com.app.dinasuvadunews.Activities.NotificationActivity;
import com.app.dinasuvadunews.Activities.SearchActivity;
import com.app.dinasuvadunews.Adapters.CategoriesListAdapter;
import com.app.dinasuvadunews.Adapters.NewsListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.HomeCategoriesDetails;
import com.app.dinasuvadunews.Models.HomeCategoriesList;
import com.app.dinasuvadunews.Models.NewsItemList;
import com.app.dinasuvadunews.Models.AllNewsDetailsResponseModel;
import com.app.dinasuvadunews.Models.AllNewsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NewsFragment extends Fragment {

    private static final String TAG = NewsFragment.class.getSimpleName();
    @BindView(R.id.categoriesRecyclerView)
    RecyclerView categoriesRecyclerView;
    @BindView(R.id.newRecyclerView)
    RecyclerView newRecyclerView;
    CategoriesListAdapter categoriesListAdapter;
    NewsListAdapter newsListAdapter;
    @BindView(R.id.addCategoryLayout)
    RelativeLayout addCategoryLayout;
    @BindView(R.id.loadingLayout)
    View loadingLayout;
    @BindView(R.id.searchIcon)
    ImageView searchIcon;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayoutManager linearLayoutManager;
    public static final int ITEMS_PER_AD = 5;
    private static final String AD_UNIT_ID = "ca-app-pub-3940256099942544/6300978111";
    //private JSONArray jsonArray;
    private List<Object> recyclerViewItems;
    CommonViewModel newsFragmentViewModel;
    AppSettings appSettings;
    private int currentPageNum = 1;
    private Integer totalNumOfPages = 1;
    private LinearLayoutManager linearLayoutManagerVertical;
    private String currentCategoryId = "";

    @BindView(R.id.navigationIcon)
    ImageView navigationIcon;

    @BindView(R.id.notificationLayout)
    RelativeLayout notificationLayout;

    public NewsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_news, container, false);
        ButterKnife.bind(this, view);

        initViews();


        setClickListeners();
        getCategoriesApi();

        swipeRefreshLayoutListener();
        updateDeviceToken();
        setRecyclerViewScrollViewListener();
        return view;
    }

    private void setRecyclerViewScrollViewListener() {

        newRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (newsListAdapter != null) {

                        if (totalNumOfPages > 1) {
                            if (newsListAdapter.getItemCount() > 0 && linearLayoutManagerVertical.findLastVisibleItemPosition() >= newsListAdapter.getItemCount() - 1) {
                                try {
                                    if (totalNumOfPages == currentPageNum) {
                                    } else {
                                        currentPageNum = currentPageNum + 1;
                                        getNewsApi(currentCategoryId);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                }
            }
        });

    }

    private void updateDeviceToken() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.DEVICE_TOKEN, appSettings.getAppTokenId());
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.UPDATE_DEV_TOKEN);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getDeviceTokenResponse(inputForAPI).observe(getActivity(), new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                }
            }

        });

    }


    private void swipeRefreshLayoutListener() {

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPageNum = 1;
                totalNumOfPages = 1;
                updateDeviceToken();
                getCategoriesApi();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
    }

    private void getNewsApi(String categoryId) {

        if (currentPageNum == 1)
            showLoading();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.CATEGORY_ID, categoryId);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.PAGE_NUMBER, currentPageNum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.NEWS_API);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getAllNewsAp(inputForAPI).observe(getActivity(), new Observer<AllNewsResponseModel>() {
            @Override
            public void onChanged(@Nullable AllNewsResponseModel allNewsResponseModel) {

                if (allNewsResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    totalNumOfPages = allNewsResponseModel.getTotalPage();


                    if (currentPageNum == 1) {
                        recyclerViewItems.clear();
                        addNewsItemsFromResponse(allNewsResponseModel.getNewsfeed());
                        setAdsList();
                        setNewsAdapter();
                        loadBannerAds();
                    } else {
                        addNewsItemsFromResponse(allNewsResponseModel.getNewsfeed());
                        newsListAdapter.reloadTheList(recyclerViewItems);
                    }
                    hideLoading();
                }
            }

        });

    }

    private void hideLoading() {
        loadingLayout.setVisibility(View.GONE);
        newRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        loadingLayout.setVisibility(View.VISIBLE);
        newRecyclerView.setVisibility(View.GONE);
    }

    private void getCategoriesApi() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.DEVICE_ID, appSettings.getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.HOME_CATEGORY);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getHomeCategoriesList(inputForAPI).observe(this, new Observer<HomeCategoriesList>() {
            @Override
            public void onChanged(@Nullable HomeCategoriesList homeCategoriesList) {

                if (homeCategoriesList != null) {

                    if (homeCategoriesList.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                        if (homeCategoriesList.getCategories().size() > 0) {
                            setCategoriesAdapter(homeCategoriesList.getCategories());

                            currentCategoryId = homeCategoriesList.getCategories().get(0).getCategoryId();
                            getNewsApi(currentCategoryId);
                        }
                    } else {
                        Utilities.showToast(getActivity(), homeCategoriesList.getErrorMessage());
                    }
                }
            }

        });

    }

    private void setClickListeners() {

        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(getActivity(), SearchActivity.class));
            }
        });

        addCategoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                CategoriesActivity.reloadTheApi(new CommonClickListener() {
                    @Override
                    public void clickListener(int position) {

                        getCategoriesApi();
                    }
                });

                startActivity(new Intent(getActivity(), CategoriesActivity.class));
            }
        });


        navigationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });


    }


    private void setAdsList() {

        // Loop through the items array and place a new banner ad in every ith position in
        // the items List.

        for (int i = ITEMS_PER_AD; i <= recyclerViewItems.size(); i += ITEMS_PER_AD) {
            final AdView adView = new AdView(getActivity());
            adView.setAdSize(AdSize.LARGE_BANNER);
            adView.setAdUnitId(AD_UNIT_ID);
            recyclerViewItems.add(i, adView);
        }

    }


    private void setNewsAdapter() {

        Log.e(TAG, "setNewsAdapter:" + recyclerViewItems.size());

        newsListAdapter = new NewsListAdapter(getActivity(), recyclerViewItems);
        newRecyclerView.setAdapter(newsListAdapter);
        newRecyclerView.setHasFixedSize(true);
        newRecyclerView.setItemViewCacheSize(20);

        newsListAdapter.setOnClickListener(new CommonClickListenerForNewsList() {
            @Override
            public void clickListenerForNewsList(int position, String imgUrl, String title, String newsId, String catgId) {


                Dialog dialog = Utilities.showShareExteralDialog(getActivity());
                LinearLayout shareLayout = dialog.findViewById(R.id.shareLayout);

                shareLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        //   Utilities.openExternalShare(getActivity(), value);

                        String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, imgUrl, newsId, catgId);
                        Utilities.generateDynamicLinkForExternalShare(getActivity(), myDynamicUri, imgUrl, title);
                    }
                });

                dialog.show();

            }
        });
    }


    private void initViews() {

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        categoriesRecyclerView.setLayoutManager(linearLayoutManager);

        linearLayoutManagerVertical = new LinearLayoutManager(getActivity());
        newRecyclerView.setLayoutManager(linearLayoutManagerVertical);

        recyclerViewItems = new ArrayList<>();
        appSettings = new AppSettings(getActivity());

        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);

    }


    private void setCategoriesAdapter(final List<HomeCategoriesDetails> categories) {

        //select the 0th category

        if (categories.size() > 0)
            categories.get(0).setIsSelected(1);

        categoriesListAdapter = new CategoriesListAdapter(getActivity(), categories);
        categoriesRecyclerView.setAdapter(categoriesListAdapter);
        categoriesRecyclerView.setHasFixedSize(true);
        categoriesListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int pos) {

                currentPageNum = 1;
                currentCategoryId = categories.get(pos).getCategoryId();
                getNewsApi(currentCategoryId);
            }
        });
    }


    @Override
    public void onResume() {
        for (Object item : recyclerViewItems) {
            if (item instanceof AdView) {
                AdView adView = (AdView) item;
                adView.resume();
            }
        }
        super.onResume();
    }

    @Override
    public void onPause() {
        for (Object item : recyclerViewItems) {
            if (item instanceof AdView) {
                AdView adView = (AdView) item;
                adView.pause();
            }
        }
        super.onPause();
    }

    @Override
    public void onDestroy() {
        for (Object item : recyclerViewItems) {
            if (item instanceof AdView) {
                AdView adView = (AdView) item;
                adView.destroy();
            }
        }
        super.onDestroy();
    }

    private void loadBannerAds() {
        loadBannerAd(ITEMS_PER_AD);
    }

    /**
     * Loads the banner ads in the items list.
     */
    private void loadBannerAd(final int index) {

        if (recyclerViewItems.size() == 0) {
            return;
        }

        if (index >= recyclerViewItems.size()) {
            return;
        }

        Object item = recyclerViewItems.get(index);
        if (!(item instanceof AdView)) {
            throw new ClassCastException("Expected item at index " + index + " to be a banner ad"
                    + " ad.");
        }

        final AdView adView = (AdView) item;

        // Set an AdListener on the AdView to wait for the previous banner ad
        // to finish loading before loading the next ad in the items list.
        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                // The previous banner ad loaded successfully, call this method again to
                // load the next ad in the items list.
                loadBannerAd(index + ITEMS_PER_AD);
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // The previous banner ad failed to load. Call this method again to load
                // the next ad in the items list.
                Log.e("MainActivity", "The previous banner ad failed to load. Attempting to"
                        + " load the next banner ad in the items list.");
                loadBannerAd(index + ITEMS_PER_AD);
            }
        });

        // Load the banner ad.
        adView.loadAd(new AdRequest.Builder().build());
    }

    private void addNewsItemsFromResponse(List<AllNewsDetailsResponseModel> newsList) {
        try {

            for (int i = 0; i < newsList.size(); ++i) {

                String title = newsList.get(i).getTitle();
                String imageUrl = newsList.get(i).getUrlToImage();
                String postedAtTime = newsList.get(i).getPublishedAt();
                String description = newsList.get(i).getDescription();
                String categoryId = newsList.get(i).getCategoryId();
                String newsId = newsList.get(i).getId();
                String catgName = newsList.get(i).getCategoryName();
                String mintsRead = newsList.get(i).getMintesRead();

                NewsItemList menuItem = new NewsItemList(title, imageUrl, postedAtTime, description, newsId, categoryId, catgName, mintsRead);
                recyclerViewItems.add(menuItem);
            }
        } catch (Exception exception) {
            Log.e(MainActivity.class.getName(), "Unable to parse JSON file.", exception);
        }
    }


}
