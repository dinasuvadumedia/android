package com.app.dinasuvadunews.Fragments;

import android.app.Dialog;

import android.content.Intent;
import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.CommentsActivity;
import com.app.dinasuvadunews.Activities.LoginActivity;
import com.app.dinasuvadunews.Adapters.SuggestedNewsListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerForShare;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsDetailResponseModel;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsDetailResponseTwoModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;


public class NewsSwipeListFragment extends Fragment {

    private static final String currentPosition = "currentPos";
    private static final String newsDetailList = "newsDetails";
    private static final String suggestedNewsList = "suggestedNews";
    private static final String comingFromPushVal = "comingFromPushVal";
    private static final String TAG = NewsSwipeListFragment.class.getSimpleName();
    private static CommonClickListenerForShare mcommonClickListener;

    List<GetAllRelatedNewsDetailResponseModel> listNewsResponse;
    List<GetAllRelatedNewsDetailResponseTwoModel> suggestedNewsResponse;
    GlideImageLoader glideImageLoader;
    private int position = 0;
    AppSettings appSettings;
    CommonViewModel newsFragmentViewModel;
    private boolean comingFromPushBoolVal = false;

    public static NewsSwipeListFragment newInstance(int pos, List<GetAllRelatedNewsDetailResponseModel> listNews, List<GetAllRelatedNewsDetailResponseTwoModel> suggestedNews, boolean comingFromPush) {
        NewsSwipeListFragment fragment = new NewsSwipeListFragment();
        Bundle args = new Bundle();
        args.putInt(currentPosition, pos);
        args.putBoolean(comingFromPushVal, comingFromPush);
        args.putSerializable(newsDetailList, (Serializable) listNews);
        args.putSerializable(suggestedNewsList, (Serializable) suggestedNews);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.swipe_news_item, container, false);

        appSettings = new AppSettings(getActivity());

        ImageView newsImage = rootView.findViewById(R.id.newsImage);
        ImageView backIconImgView = rootView.findViewById(R.id.backIconImgView);
        TextView newTitleTxtView = rootView.findViewById(R.id.newTitleTxtView);
        TextView newDescriptionTxtView = rootView.findViewById(R.id.newDescriptionTxtView);
        TextView newsCatgTxtView = rootView.findViewById(R.id.newsCatgTxtView);
        LinearLayout likeLayout = rootView.findViewById(R.id.likeLayout);
        LinearLayout whatsappShareLayout = rootView.findViewById(R.id.whatsappShareLayout);
        LinearLayout commentsLayout = rootView.findViewById(R.id.commentsLayout);
        TextView likeTxtView = rootView.findViewById(R.id.likeTxtView);
        ImageView moreImgView = rootView.findViewById(R.id.moreImgView);
        TextView timeTxtView = rootView.findViewById(R.id.timeTxtView);
        TextView catgLetterTxtView = rootView.findViewById(R.id.catgLetterTxtView);
        ImageView likeImgView = rootView.findViewById(R.id.likeImgView);
        RecyclerView suggestedRecyclerView = rootView.findViewById(R.id.suggestedRecyclerView);
        glideImageLoader = new GlideImageLoader(getActivity());

        position = getArguments().getInt(currentPosition);
        comingFromPushBoolVal = getArguments().getBoolean(comingFromPushVal);
        listNewsResponse = (List<GetAllRelatedNewsDetailResponseModel>) getArguments().getSerializable(newsDetailList);
        suggestedNewsResponse = (List<GetAllRelatedNewsDetailResponseTwoModel>) getArguments().getSerializable(suggestedNewsList);


        newTitleTxtView.setText(listNewsResponse.get(position).getTitle());
        catgLetterTxtView.setText(listNewsResponse.get(position).getCategoryName());
        newsCatgTxtView.setText(listNewsResponse.get(position).getCategoryName());
        newDescriptionTxtView.setText(listNewsResponse.get(position).getLongDescription());
        timeTxtView.setText(listNewsResponse.get(position).getMintesRead());
        glideImageLoader.loadSingleImgUrl(getActivity(), listNewsResponse.get(position).getUrlToImage(), newsImage);

        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        SuggestedNewsListAdapter suggestedNewsListAdapter = new SuggestedNewsListAdapter(getActivity(), suggestedNewsResponse);
        suggestedRecyclerView.setAdapter(suggestedNewsListAdapter);
        suggestedNewsListAdapter.setOnClickListener(new CommonClickListenerForNewsList() {
            @Override
            public void clickListenerForNewsList(int position, String imgUrl, String content, String newsId, String catgId) {


                Dialog dialog = Utilities.showShareExteralDialog(getActivity());
                LinearLayout shareLayout = dialog.findViewById(R.id.shareLayout);

                shareLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, imgUrl, newsId, catgId);
                        Utilities.generateDynamicLinkForExternalShare(getActivity(), myDynamicUri, imgUrl, content);
                    }
                });

                dialog.show();
            }
        });


        if (listNewsResponse.get(position).getIsLiked() == 1) {
            likeImgView.setImageDrawable(getResources().getDrawable(R.drawable.sel_like));
        } else {
            likeImgView.setImageDrawable(getResources().getDrawable(R.drawable.like));
        }


        if (listNewsResponse.get(position).getLikeCount() == 1) {
            likeTxtView.setText("1" + " Like");
        } else {
            likeTxtView.setText(listNewsResponse.get(position).getLikeCount() + " Likes");
        }

/*
        backIconImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (comingFromPushBoolVal) {
                    startActivity(new Intent(getActivity(), MainActivity.class));
                    getActivity().finish();
                } else {
                    getActivity().finish();
                }

            }
        });
*/


/*
        moreImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = Utilities.showShareExteralDialog(getActivity());
                LinearLayout shareLayout = dialog.findViewById(R.id.shareLayout);

                shareLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        // Utilities.openExternalShare(getActivity(), listNewsResponse.get(position).getUrlToImage());

                        String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, listNewsResponse.get(position).getUrlToImage(), listNewsResponse.get(position).getId(), listNewsResponse.get(position).getCategoryId());
                        Utilities.generateDynamicLinkForExternalShare(getActivity(), myDynamicUri, listNewsResponse.get(position).getUrlToImage(), listNewsResponse.get(position).getTitle());
                    }
                });

                dialog.show();
            }
        });
*/


        likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int likeCounts = listNewsResponse.get(position).getLikeCount();

                if (new AppSettings(getActivity()).getIsLoggedIn().equalsIgnoreCase(getResources().getString(R.string.true_val))) {

                    int likeVal = 0;

                    if (listNewsResponse.get(position).getIsLiked() == 0) {
                        likeVal = 1;
                        likeImgView.setImageDrawable(getResources().getDrawable(R.drawable.sel_like));
                        Utilities.playLikeSound(getActivity());

                        likeCounts++;

                        if (likeCounts <= 1) {
                            likeTxtView.setText("1" + " Like");
                        } else {
                            likeTxtView.setText(likeCounts + " Likes");
                        }

                        listNewsResponse.get(position).setLikeCount(likeCounts);

                    } else {
                        likeVal = 0;
                        likeImgView.setImageDrawable(getResources().getDrawable(R.drawable.like));


                        likeCounts--;

                        if (likeCounts <= 1) {
                            likeTxtView.setText("1" + " Like");
                        } else {
                            likeTxtView.setText(likeCounts + " Likes");
                        }

                        listNewsResponse.get(position).setLikeCount(likeCounts);


                    }

                    updateLikeValue(listNewsResponse.get(position).getId(), likeVal);
                    listNewsResponse.get(position).setIsLiked(likeVal);

                } else {
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                }

            }
        });

        whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Utilities.shareInWhatsappDialog(getActivity(), listNewsResponse.get(position).getUrlToImage());

                String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, listNewsResponse.get(position).getUrlToImage(), listNewsResponse.get(position).getId(), listNewsResponse.get(position).getCategoryId());
                Utilities.generateDynamicLinkForWhatsappShare(getActivity(), myDynamicUri, listNewsResponse.get(position).getUrlToImage(), listNewsResponse.get(position).getTitle());

            }
        });


        commentsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (new AppSettings(getActivity()).getIsLoggedIn().equalsIgnoreCase(getResources().getString(R.string.true_val))) {
                    startActivity(new Intent(getActivity(), CommentsActivity.class).putExtra(Constants.INTENT_VALUES.NEWS_ID, listNewsResponse.get(position).getId()));
                } else {
                    startActivity(new Intent(getActivity(), LoginActivity.class));
                }

            }
        });

        return rootView;
    }

    private void updateLikeValue(String id, Integer isLiked) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.NEWS_ID, id);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.IS_LIKED, isLiked);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.NEWS_LIKE);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getLikeNewsResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    // Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                } else
                    Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
            }

        });

    }


}
