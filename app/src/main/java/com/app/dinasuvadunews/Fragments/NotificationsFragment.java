package com.app.dinasuvadunews.Fragments;


import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Adapters.NotificationsListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.DetailNotificationsResponseModel;
import com.app.dinasuvadunews.Models.GetNotificationsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class NotificationsFragment extends Fragment {


    @BindView(R.id.notificationsReclerView)
    RecyclerView notificationsReclerView;
    @BindView(R.id.errorTxtView)
    TextView errorTxtView;
    CommonViewModel commonViewModel;
    AppSettings appSettings;
    NotificationsListAdapter notificationsListAdapter;

    public NotificationsFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.notification_frag, container, false);
        ButterKnife.bind(this, view);


        initViews();
        getNotificationsList();
        return view;
    }


    private void getNotificationsList() {
        try {
            InputForAPI inputForAPI = new InputForAPI(getActivity());
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            inputForAPI.setJsonObject(jsonObject);


            inputForAPI.setUrl(UrlHelpers.GET_NOTIFICATIONS);
            HashMap<String, String> header = new HashMap<>();
            header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
            header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
            inputForAPI.setHeaders(header);

            commonViewModel.getNotificationsList(inputForAPI).observe(this, new Observer<GetNotificationsResponseModel>() {
                @Override
                public void onChanged(@Nullable GetNotificationsResponseModel getNotificationsResponseModel) {

                    if (getNotificationsResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                        if (getNotificationsResponseModel.getListNotification().size() > 0) {
                            setNotificationsListAdapter(getNotificationsResponseModel.getListNotification());
                            errorTxtView.setVisibility(View.GONE);
                        } else {
                            errorTxtView.setVisibility(View.VISIBLE);
                        }

                    } else {
                        Utilities.showToast(getActivity(), getNotificationsResponseModel.getErrorMessage());
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setNotificationsListAdapter(List<DetailNotificationsResponseModel> listNotification) {
        notificationsListAdapter = new NotificationsListAdapter(getActivity(), listNotification);
        notificationsReclerView.setAdapter(notificationsListAdapter);


        notificationsListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                updateSeenNotification(listNotification.get(position).getNewsId());
            }
        });
    }

    private void updateSeenNotification(String newsId) {
        try {
            InputForAPI inputForAPI = new InputForAPI(getActivity());
            JSONObject jsonObject = new JSONObject();

            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.NEWS_ID, newsId);
            inputForAPI.setJsonObject(jsonObject);


            inputForAPI.setUrl(UrlHelpers.UPDATE_NOTI_VIEW);
            HashMap<String, String> header = new HashMap<>();
            header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
            header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
            inputForAPI.setHeaders(header);


            commonViewModel.updateNotificationView(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
                @Override
                public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                    if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        getNotificationsList();
                    } else {
                        Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                    }
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void initViews() {
        appSettings = new AppSettings(getActivity());
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        notificationsReclerView.addItemDecoration(new SpacesItemDecoration(40, 1));
    }
}