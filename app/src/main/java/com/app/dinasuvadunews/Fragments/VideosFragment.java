package com.app.dinasuvadunews.Fragments;


import android.Manifest;
import android.app.AlertDialog;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.MainActivity;
import com.app.dinasuvadunews.Activities.NotificationActivity;
import com.app.dinasuvadunews.Activities.VideoPlayerActivity;
import com.app.dinasuvadunews.Adapters.CustomViewpagerAdapter;
import com.app.dinasuvadunews.Adapters.TrendingVideosListAdapter;
import com.app.dinasuvadunews.Adapters.VideosMainListAdapter;
import com.app.dinasuvadunews.Adapters.VideosTitlesListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.BuildConfig;
import com.app.dinasuvadunews.DownloadFiles.DownloadFiles;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.RecyclerViewPager;
import com.app.dinasuvadunews.Helpers.DotsIndicatorDecoration;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.TrendingVideosList;
import com.app.dinasuvadunews.Models.VideosCategoriesDetailsList;
import com.app.dinasuvadunews.Models.VideosCategoriesList;
import com.app.dinasuvadunews.Models.VideosListDetailsResponseModel;
import com.app.dinasuvadunews.Models.VideosListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.interfaces.CommonClickListenerForVideos;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class VideosFragment extends Fragment {

    @BindView(R.id.videoCategoriesRecyclerview)
    RecyclerView videoCategoriesRecyclerview;
    @BindView(R.id.videosListRecyclerview)
    RecyclerView videosListRecyclerview;
    VideosTitlesListAdapter videosTitlesListAdapter;
    VideosMainListAdapter videosMainListAdapter;
    CustomViewpagerAdapter trendingNewsViewpagerAdapter;
    //    @BindView(R.id.swipeRefreshLayout)
//    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.trendingVideosRecyclerView)
    RecyclerViewPager trendingVideosRecyclerView;
    @BindView(R.id.prevVideoImgView)
    ImageView prevVideoImgView;
    @BindView(R.id.nextVideoImgView)
    ImageView nextVideoImgView;
    AppSettings appSettings;
    CommonViewModel commonViewModel;
    private int currentPageNum = 1;
    private String downloadVideoUrl = "";
    TrendingVideosListAdapter trendingVideosListAdapter;
    private int currentVideoPos = 0;
    private int totalTrendingVideos = 0;

    @BindView(R.id.navigationIcon)
    ImageView navigationIcon;

    @BindView(R.id.notificationLayout)
    RelativeLayout notificationLayout;

    private LinearLayoutManager linearLayoutManagerVertical;
    private int totalPages = 0;
    private String currentCategoryId = "";


    public VideosFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_videos, container, false);
        ButterKnife.bind(this, view);


        initViews();
        // setViewPager();
        setClickListener();
        getVideosCategoriesList();
        swipeRefreshLayoutListener();
        setScrollListener();

        return view;
    }

    private void setScrollListener() {

        videosListRecyclerview.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (videosMainListAdapter != null) {

                        if (totalPages > 1) {
                            if (videosMainListAdapter.getItemCount() > 0 && linearLayoutManagerVertical.findLastVisibleItemPosition() >= videosMainListAdapter.getItemCount() - 1) {
                                try {
                                    if (totalPages == currentPageNum) {
                                    } else {
                                        currentPageNum = currentPageNum + 1;
                                        getCategoryVideosList(currentCategoryId);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                }
            }
        });

    }

    private void setClickListener() {

        nextVideoImgView.setOnClickListener(v -> {

            currentVideoPos++;
            trendingVideosRecyclerView.smoothScrollToPosition(currentVideoPos);
        });


        prevVideoImgView.setOnClickListener(v -> {

            currentVideoPos--;
            trendingVideosRecyclerView.smoothScrollToPosition(currentVideoPos);

        });


        navigationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.drawerLayout.openDrawer(GravityCompat.START);
            }
        });


        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), NotificationActivity.class));
            }
        });


    }

    private void trendingVideosRecyclerViewPager(List<TrendingVideosList> listvideos) {
        totalTrendingVideos = listvideos.size();
        trendingVideosListAdapter = new TrendingVideosListAdapter(getActivity(), listvideos, trendingVideosRecyclerView);
        trendingVideosRecyclerView.setAdapter(trendingVideosListAdapter);


        final int radius = getResources().getDimensionPixelSize(R.dimen.radius);
        final int dotsHeight = getResources().getDimensionPixelSize(R.dimen.height);
        final int color = ContextCompat.getColor(getContext(), R.color.pink);
        final int inactive_color = ContextCompat.getColor(getContext(), R.color.grey);
        trendingVideosRecyclerView.addItemDecoration(new DotsIndicatorDecoration(radius, radius, dotsHeight, inactive_color, color));

    }

    private void swipeRefreshLayoutListener() {

/*
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getVideosCategoriesList();
                swipeRefreshLayout.setRefreshing(false);

            }
        });
*/

    }

    private void getVideosCategoriesList() {
        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_VIDEO_CATEGORY);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAllVideosCategories(inputForAPI).observe(this, new Observer<VideosCategoriesList>() {
            @Override
            public void onChanged(@Nullable VideosCategoriesList videosCategoriesList) {

                if (videosCategoriesList.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    if (videosCategoriesList != null) {
                        if (videosCategoriesList.getListCategory().size() > 0) {

                            videosCategoriesList.getListCategory().get(0).setSelected(true);


                            trendingVideosRecyclerViewPager(videosCategoriesList.getListvideos());
                            setVideosCategoryAdapter(videosCategoriesList.getListCategory());
                            currentCategoryId = videosCategoriesList.getListCategory().get(0).getCategoryId();
                            getCategoryVideosList(currentCategoryId);
                        }
                    }
                } else {
                    Utilities.showToast(getActivity(), videosCategoriesList.getErrorMessage());
                }
            }

        });

    }

    private void getCategoryVideosList(String categoryId) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.CATEGORY_ID, categoryId);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.PAGE_NUMBER, currentPageNum);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.LIST_VIDEOS);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getVideosList(inputForAPI).observe(this, new Observer<VideosListResponseModel>() {
            @Override
            public void onChanged(@Nullable VideosListResponseModel videosListResponseModel) {


                if (videosListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    currentPageNum = videosListResponseModel.getTotalPage();
                    setVideosMainAdapter(videosListResponseModel.getListVideos());
                } else {
                    Utilities.showToast(getActivity(), videosListResponseModel.getErrorMessage());
                }
            }

        });

    }

    private void setViewPager() {

/*
        for (int i = 0; i < 10; i++) {
            String imageUrl = "";
            trendingNewsViewpagerAdapter.addFragment(TrendingNewsListFragment.newInstance(imageUrl));
        }

        trendingNewsViewPager.setAdapter(trendingNewsViewpagerAdapter);
        indicator.setViewPager(trendingNewsViewPager);
        trendingNewsViewPager.setOffscreenPageLimit(10);
*/

    }

    private void setVideosCategoryAdapter(final List<VideosCategoriesDetailsList> listCategory) {
        videosTitlesListAdapter = new VideosTitlesListAdapter(getActivity(), listCategory);
        videoCategoriesRecyclerview.setAdapter(videosTitlesListAdapter);

        videosTitlesListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                currentPageNum = 1;
                currentCategoryId = listCategory.get(position).getCategoryId();
                getCategoryVideosList(listCategory.get(position).getCategoryId());

            }
        });

    }


    private void initViews() {


        linearLayoutManagerVertical = new LinearLayoutManager(getActivity());
        videosListRecyclerview.setLayoutManager(linearLayoutManagerVertical);


        trendingVideosRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        trendingNewsViewpagerAdapter = new CustomViewpagerAdapter(getChildFragmentManager());
        appSettings = new AppSettings(getActivity());
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        videoCategoriesRecyclerview.addItemDecoration(new SpacesItemDecoration(20, 2));


    }

    private void setVideosMainAdapter(List<VideosListDetailsResponseModel> listVideos) {

        if (listVideos.size() > 0) {

            if (currentPageNum == 1) {
                videosMainListAdapter = new VideosMainListAdapter(getActivity(), listVideos);
                videosListRecyclerview.setAdapter(videosMainListAdapter);
                videosListRecyclerview.setHasFixedSize(true);
                videosListRecyclerview.setItemViewCacheSize(20);
            } else {

                videosMainListAdapter.notifyList(listVideos);
            }

            videosMainListAdapter.setOnClickListener(new CommonClickListenerForVideos() {
                @Override
                public void onImgClick(int position, List<VideosListDetailsResponseModel> listVideos) {

                    startActivity(new Intent(getActivity(), VideoPlayerActivity.class).putExtra(Constants.INTENT_VALUES.VIDEO_URL, listVideos.get(position).getUrlToVideo()));
                }

                @Override
                public void onLikeClick(int pos, List<VideosListDetailsResponseModel> listVideos) {
                    int likeCounts = listVideos.get(pos).getLikeCount();

                    int likeVal = 0;

                    if (listVideos.get(pos).getIsLiked() == 0) {
                        likeVal = 1;
                        listVideos.get(pos).setIsLiked(1);
                        likeCounts++;
                        listVideos.get(pos).setLikeCount(likeCounts);
                        Utilities.playLikeSound(getActivity());
                    } else {
                        likeVal = 0;
                        likeCounts--;
                        listVideos.get(pos).setLikeCount(likeCounts);
                        listVideos.get(pos).setIsLiked(0);
                    }

                    videosMainListAdapter.reloadList(listVideos);

                    sendLikeVal(listVideos.get(pos).getId(), likeVal);
                }

                @Override
                public void onDownloadClick(int pos, List<VideosListDetailsResponseModel> listVideos) {

                    downloadVideoUrl = listVideos.get(pos).getUrlToVideo();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        getPermissions();
                    } else {
                        downloadVideo(downloadVideoUrl);
                    }

                }

                @Override
                public void onShareClick(int pos, List<VideosListDetailsResponseModel> listVideos) {
                    //    Utilities.shareInWhatsappDialog(getActivity(), listVideos.get(pos).getUrlToVideo());
                    String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_VIDEO_TYPE, listVideos.get(pos).getUrlToVideo(), listVideos.get(pos).getId(), listVideos.get(pos).getCategoryId());
                    Utilities.generateDynamicLinkForWhatsappShare(getActivity(), myDynamicUri, listVideos.get(pos).getUrlToVideo(), listVideos.get(pos).getTitle());


                }

                @Override
                public void onShareClickWithImg(int pos, ImageView imageView, List<VideosListDetailsResponseModel> listVideos) {

                }
            });
        }
    }

    private void getPermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }

    private void downloadVideo(String downloadVideoUrl) {
        new DownloadFiles(getActivity(), downloadVideoUrl, 1);
    }


    private void sendLikeVal(String id, int likeVal) {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.VIDEO_ID, id);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.IS_LIKED, likeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(getActivity());
        inputForAPI.setUrl(UrlHelpers.VIDEO_LIKE);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getVideoLikeNewsResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    // Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                } else
                    Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
            }

        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    downloadVideo(downloadVideoUrl);
                } else {
                    showAlertDialog();
                }
                break;
        }

    }

    private void showAlertDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle(getResources().getString(R.string.requires_per));
        alertDialogBuilder.setMessage(getResources().getString(R.string.need_storage_per));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                        startActivity(i);

                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.cancel();

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


    @Override
    public void onStop() {
        super.onStop();

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if (player != null) {
            player.onPause();
        }*/

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (player != null) {
            player.onResume();
        }*/

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if (player != null) {
            player.onDestroy();
        }*/

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityDestroy();
    }
}
