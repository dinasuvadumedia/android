package com.app.dinasuvadunews.Helpers;

import android.content.Context;

public class AppSettings {
    Context context;

    public AppSettings(Context context) {
        this.context = context;
    }

    public String getAppTokenId() {
        appTokenId = SharedPreference.getKey(context, "appTokenId");
        return appTokenId;
    }

    public void setAppTokenId(String appTokenId) {
        SharedPreference.putKey(context, "appTokenId", appTokenId);
        this.appTokenId = appTokenId;
    }

    String appTokenId;


    public String getUdid() {
        udid = SharedPreference.getKey(context, "udid");
        return udid;
    }

    public void setUdid(String udid) {
        SharedPreference.putKey(context, "udid", udid);
        this.udid = udid;
    }

    String udid;

    public String getIsLoggedIn() {
        isLoggedIn = SharedPreference.getKey(context, "isLoggedIn");
        return isLoggedIn;
    }

    public void setIsLoggedIn(String isLoggedIn) {
        SharedPreference.putKey(context, "isLoggedIn", isLoggedIn);
        this.isLoggedIn = isLoggedIn;
    }

    String isLoggedIn = "";

    public String getUserId() {
        userId = SharedPreference.getKey(context, "userId");
        return userId;
    }

    public void setUserId(String userId) {
        SharedPreference.putKey(context, "userId", userId);
        this.userId = userId;
    }

    String userId = "";

    public String getUserName() {
        userName = SharedPreference.getKey(context, "userName");
        return userName;
    }

    public void setUserName(String userName) {
        SharedPreference.putKey(context, "userName", userName);
        this.userName = userName;
    }

    String userName = "";



    public String getOpenVideoTab() {
        openVideoTab = SharedPreference.getKey(context, "openVideoTab");
        return openVideoTab;
    }

    public void setOpenVideoTab(String openVideoTab) {
        SharedPreference.putKey(context, "openVideoTab", openVideoTab);
        this.openVideoTab = openVideoTab;
    }

    String openVideoTab = "";

    public String getOpenViralTab() {
        openViralTab = SharedPreference.getKey(context, "openViralTab");
        return openViralTab;
    }

    public void setOpenViralTab(String openViralTab) {
        SharedPreference.putKey(context, "openViralTab", openViralTab);
        this.openViralTab = openViralTab;
    }

    String openViralTab = "";
}
