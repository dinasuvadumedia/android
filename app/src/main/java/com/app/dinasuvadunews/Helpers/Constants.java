package com.app.dinasuvadunews.Helpers;

public class Constants {

    public static class HEADERS {

        public static final String Content_Type = "Content-Type";
        public static final String CONTENT_NAME = "application/x-www-form-urlencoded";
        public static final String CONTENT_JSON_NAME = "application/json";

    }


    public static class API_PARAMS {
        public static final String CATEGORY_ID = "categoryId";
        public static final String PAGE_NUMBER = "pageNumber";
        public static final String CONTENT_NAME = "application/x-www-form-urlencoded";
        public static final String DEVICE_ID = "deviceId";
        public static final String NAME = "name";
        public static final String IS_SELECTED = "isSelected";
        public static final String NEWS_ID = "newsId";
        public static final String USER_ID = "userId";
        public static final String UDID = "udId";
        public static final String IS_LIKED = "isLiked";
        public static final String EMAIL = "email";
        public static final String USER_NAME = "userName";
        public static final String PASSWORD = "password";
        public static final String COMMENT = "comment";
        public static final String VIRAL_ID = "viralId";
        public static final String VIDEO_ID = "videoId";
        public static final String CATEGORYS = "categorys";
        public static final String DEVICE_TOKEN = "devicetoken";
        public static final String BLOGGER_ID = "bloggerId";
        public static final String STATUS = "status";
        public static final String BLOG_ID = "blogId";
        public static final String SEARCH_TEXT = "searchText";
    }


    public static class INTENT_VALUES {
        public static final String NEWS_TITLE = "newsTitle";
        public static final String NEWS_DESC = "newsDescription";
        public static final String NEWS_IMG = "newsImg";
        public static final String VIDEO_URL = "videoUrl";
        public static final String CATEGORY_ID = "categoryId";
        public static final String NEWS_ID = "newsId";
        public static final String APP_FOLDER = "Dinasuvadu";
        public static final String SHARE_NEWS_TYPE = "shareNews";
        public static final String SHARE_VIDEO_TYPE = "shareVideos";
        public static final String TITLE = "title";
        public static final String COMING_FROM_PUSH = "comingFromPush";
        public static final String SHARE_VIRAL_TYPE = "shareViral";
        public static final String BLOGER_ID = "bloggerId";
        public static final String SHARE_BLOG_TYPE = "blogType";
    }
}
