package com.app.dinasuvadunews.Helpers.CustomViews;

import android.content.Context;
import android.graphics.Bitmap;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.palette.graphics.Palette;

import android.widget.ImageView;
import android.widget.RelativeLayout;


import com.app.dinasuvadunews.Helpers.FastBlur;
import com.app.dinasuvadunews.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class GlideImageLoader {
    public Context context;

    public GlideImageLoader(Context contextvalue) {
        context = contextvalue;
    }

    RequestOptions options = new RequestOptions()
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE);

    public void loadImgUrl(String url, final ImageView imageView, final RelativeLayout backgroundImgLayout) {

        Glide.with(context)
                .asBitmap()
                .load(url)
                .dontAnimate()
                .apply(options.skipMemoryCache(true))
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                        if (resource != null) {
                            Palette p = Palette.from(resource).generate();
                            // Use generated instance
                            int color = p.getLightMutedColor(ContextCompat.getColor(context, R.color.grey));

                            backgroundImgLayout.setBackgroundColor(color);
                        }
                        return false;
                    }
                })
                .into(imageView);

    }

    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }


    public void loadSingleImgUrl(Context context, String newsImgUrl, ImageView newsImage) {

        Glide.with(context).load(newsImgUrl).dontAnimate().into(newsImage);
    }


    public void loadSingleImgBlurUrl(Context context, String newsImgUrl, ImageView newsImage, ImageView blurImgView) {

        Glide.with(context)
                .asBitmap()
                .load(newsImgUrl)
                .dontAnimate()
                .apply(options.skipMemoryCache(true))
                .listener(new RequestListener<Bitmap>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {

                        if (resource != null) {
                            Bitmap bitmap = Bitmap.createScaledBitmap(
                                    resource, 50, 50, false);
                            blurImgView.setImageBitmap(FastBlur.doBlur(bitmap, 5, true));


                        }
                        return false;
                    }
                })
                .into(newsImage);
    }
}
