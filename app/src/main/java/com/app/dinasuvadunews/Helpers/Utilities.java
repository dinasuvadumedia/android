package com.app.dinasuvadunews.Helpers;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.app.dinasuvadunews.BuildConfig;
import com.app.dinasuvadunews.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class Utilities {

    public static void setStatusBarColor(Activity activity, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(color);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static final void setSystemBarTheme(final Activity pActivity, final boolean pIsDark) {
        // Fetch the current flags.
        final int lFlags = pActivity.getWindow().getDecorView().getSystemUiVisibility();
        // Update the SystemUiVisibility dependening on whether we want a Light or Dark theme.
        pActivity.getWindow().getDecorView().setSystemUiVisibility(pIsDark ? (lFlags & ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR) : (lFlags | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR));
    }

    public static void transperentStatusBar(Activity activity) {
        activity.getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        activity.getWindow().setStatusBarColor(Color.TRANSPARENT);

    }

    public static void setViewsColor(View view, int color) {
        view.setBackgroundColor(color);

    }

    public static void showToast(Activity activity, String msg) {

        Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
    }

    public static void setTextColor(TextView txtView, int color) {
        txtView.setTextColor(color);
    }

    public static void setImgTintColor(ImageView imgView, int color) {
        imgView.setColorFilter(ContextCompat.getColor(imgView.getContext(), color), android.graphics.PorterDuff.Mode.MULTIPLY);
    }

    public static void showView(View searchIcon) {
        searchIcon.setVisibility(View.VISIBLE);
    }

    public static void hideView(View searchIcon) {

        searchIcon.setVisibility(View.GONE);
    }

    public static void log(String tag, String params) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, "Values:==>" + params);
        }
    }

    public static boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm != null) {
            return cm.getActiveNetworkInfo() != null;
        } else {
            return false;
        }
    }

    public static String getNewsTime(String postedTime) {

        String date = "";
        DateTime postDateTime = new DateTime(postedTime);

        DateTime currentDateTime = new DateTime();

        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ");
            String dates = postDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");
            String currentDates = currentDateTime.toString("yyyy-MM-dd'T'HH:mm:ss.sssZ");

            Date date1 = simpleDateFormat.parse(dates);
            Date date2 = simpleDateFormat.parse(currentDates);
            if (date1.getTime() - date2.getTime() == 0) {
                date = " Just now";
            } else {
                Period period = null;
                try {
                    Interval interval = new Interval(date1.getTime(), date2.getTime());
                    period = interval.toPeriod(PeriodType.dayTime());
                    if (period.getYears() != 0) {
                        date = period.getYears() + " years ago";
                    } else if (period.getYears() == 1) {
                        date = period.getYears() + " year ago";
                    } else if (period.getMonths() != 0) {
                        date = period.getMonths() + " months ago";
                    } else if (period.getMonths() == 1) {
                        date = period.getMonths() + " month ago";
                    } else if (period.getDays() != 0) {
                        date = period.getDays() + " day ago";
                    } else if (period.getHours() > 0 && period.getHours() == 1) {
                        date = period.getHours() + " hour ago";
                    } else if (period.getHours() > 1) {
                        date = period.getHours() + " hours ago";
                    } else if (period.getMinutes() == 1) {
                        date = period.getMinutes() + " minute ago";
                    } else if (period.getMinutes() != 0) {
                        date = period.getMinutes() + " minutes ago";
                    } else if (period.getSeconds() > 0 && period.getSeconds() == 1) {
                        date = period.getSeconds() + " second ago";
                    } else if (period.getSeconds() > 1) {
                        date = period.getSeconds() + " seconds ago";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    date = " Just now";
                }

            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Dialog showImageViewDialog(Context context) {
        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_image_viewer);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static void shareInWhatsappDialog(Activity context, String imageUrl) {

        Intent shareIntent = new Intent();
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        shareIntent.setAction(Intent.ACTION_SEND);
        //without the below line intent will show error
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, imageUrl);
        // Target whatsapp:
        shareIntent.setPackage("com.whatsapp");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        try {
            context.startActivity(shareIntent);
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(context,
                    "Whatsapp have not been installed.",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isValidEmail(String email) {


        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();

    }

    public static void playLikeSound(Activity activity) {

        MediaPlayer mPlayer2;
        mPlayer2 = MediaPlayer.create(activity, R.raw.pop);
        mPlayer2.start();
    }


    public static void playChatSound(Activity activity) {

        MediaPlayer mPlayer2;
        mPlayer2 = MediaPlayer.create(activity, R.raw.chat);
        mPlayer2.start();
    }

    public static void openExternalShare(Activity context, String imageUrl) {

        try {
            Intent txtIntent = new Intent(android.content.Intent.ACTION_SEND);
            txtIntent.setType("text/plane");
            txtIntent.putExtra(android.content.Intent.EXTRA_TEXT, imageUrl);
            context.startActivity(Intent.createChooser(txtIntent, "Share"));
        } catch (Exception e) {

        }

    }

    public static Dialog showShareExteralDialog(Activity activity) {
        Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.show_share_dialog);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.BOTTOM);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;


    }

    private static String buildDynamicLink(String uriLink, String imageUrl, String title) {

        String path = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setDomainUriPrefix("https://api.dinasuvadu.com")
                .setLink(Uri.parse(uriLink))
                .setAndroidParameters(new DynamicLink.AndroidParameters.Builder().build())
                .setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
                        .setTitle(title)
                        .setImageUrl(Uri.parse(imageUrl))
                        .setDescription("dinasuvadu.com")
                        .build())
                .setGoogleAnalyticsParameters(new DynamicLink.GoogleAnalyticsParameters.Builder().setSource("Android").build())
                .buildDynamicLink().getUri().toString();
        return path;
    }


    public static void generateDynamicLinkForExternalShare(Activity context, String dynamicUri, String imageUrl, String title) {

        Task<ShortDynamicLink> createLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(buildDynamicLink(dynamicUri, imageUrl, title)))
                .buildShortDynamicLink()
                .addOnCompleteListener(context, new OnCompleteListener<ShortDynamicLink>() {
                    public String TAG;

                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink(); //flowchart link is a debugging URL

                            Log.d(TAG, shortLink.toString());
                            Log.d(TAG, flowchartLink.toString());
                            Intent intent = new Intent();
                            String msg = title + " " + shortLink.toString() + "\n" + "\n" + " by Dinasuvadu";
                            intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, msg);
                            intent.setType("text/plain");
                            context.startActivity(intent);

                        } else {
                            // Error
                            Log.d(TAG, task.toString());

                        }
                    }
                });
    }


    public static void generateDynamicLinkForWhatsappShare(Activity context, String dynamicUri, String imageUrl, String title) {

        Task<ShortDynamicLink> createLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLongLink(Uri.parse(buildDynamicLink(dynamicUri, imageUrl, title)))
                .buildShortDynamicLink()
                .addOnCompleteListener(context, new OnCompleteListener<ShortDynamicLink>() {
                    public String TAG;

                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {

                        //  Log.e("sdsdsds", task.getResult().toString() + "==>" + task.isSuccessful());

                        if (task.isSuccessful()) {
                            // Short link created
                            Uri shortLink = task.getResult().getShortLink();
                            Uri flowchartLink = task.getResult().getPreviewLink(); //flowchart link is a debugging URL

                            Log.d(TAG, shortLink.toString());
                            Log.d(TAG, flowchartLink.toString());
                            String msg = title + " " + shortLink.toString() + "\n" + "\n" + " by Dinasuvadu";
                           /* intent.setAction(Intent.ACTION_SEND);
                            intent.putExtra(Intent.EXTRA_TEXT, msg);
                            intent.setType("text/plain");
                            context.startActivity(intent);*/


                            Intent shareIntent = new Intent();
                            shareIntent.setType("image/*");
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            shareIntent.setAction(Intent.ACTION_SEND);
                            //without the below line intent will show error
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_TEXT, msg);
                            // Target whatsapp:
                            shareIntent.setPackage("com.whatsapp");
                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                            try {
                                context.startActivity(shareIntent);
                            } catch (android.content.ActivityNotFoundException ex) {
                                Toast.makeText(context,
                                        "Whatsapp have not been installed.",
                                        Toast.LENGTH_SHORT).show();
                            }


                        } else {
                            // Error
                            Log.d(TAG, task.toString());

                        }
                    }
                });
    }


    public static String generateCustomUri(String comingFrom, String imgUrl, String newsId, String categoryId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("api.dinasuvadu.com")
                .appendPath("module")
                .appendPath(comingFrom)
                .appendQueryParameter("newsId", newsId)
                .appendQueryParameter("categoryId", categoryId);

        String url = builder.build().toString();
        return url;
    }


    public static String generateCustomUriForBlog(String comingFrom, String blogId) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https")
                .authority("api.dinasuvadu.com")
                .appendPath("module")
                .appendPath(comingFrom)
                .appendQueryParameter("blogId", blogId);

        String url = builder.build().toString();
        return url;
    }


    public static String replacebracket(String str) {
        str = str.replaceAll("\\[|\\]", "");
        return str;
    }


    public static void sendImageAsMsgToWhatsapp(Context context, ImageView imageView, String title) {

        // Bitmap adv = BitmapFactory.decodeResource(context.getResources(), R.drawable.add_catg_plus);
        Bitmap bitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("image/jpeg");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File f = new File(Environment.getExternalStorageDirectory()
                + File.separator + "temporary_file.jpg");
        try {
            f.createNewFile();
            new FileOutputStream(f).write(bytes.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        share.putExtra(Intent.EXTRA_STREAM,
                Uri.parse(Environment.getExternalStorageDirectory() + File.separator + "temporary_file.jpg"));
        share.putExtra(Intent.EXTRA_TEXT, title);

        try {
            share.setPackage("com.whatsapp");
            context.startActivity(Intent.createChooser(share, "Share Image"));
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, "Please Install Whatsapp", Toast.LENGTH_LONG).show();
        }

    }

}
