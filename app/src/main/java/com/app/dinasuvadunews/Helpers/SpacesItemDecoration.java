package com.app.dinasuvadunews.Helpers;

import android.graphics.Rect;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space, whichSide;

    public SpacesItemDecoration(int space, int whichSide) {
        this.space = space;
        this.whichSide = whichSide;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if (whichSide == 0) {

            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.left = space;
            }
        } else if (whichSide == 1) {

            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.top = space;
            }
        }else if (whichSide == 2) {

            if (parent.getChildAdapterPosition(view) == 0) {
                outRect.left = space;
            }
        }
    }
}
