package com.app.dinasuvadunews.FCM;

import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.google.firebase.messaging.FirebaseMessagingService;


public class MyFirebaseInstanceIdService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseInstanceIdService.class.getSimpleName();

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        Utilities.log(TAG, "onNewToken:" + s);
        new AppSettings(this).setAppTokenId(s);
    }
}
