package com.app.dinasuvadunews.FCM;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.app.dinasuvadunews.Activities.DetailNewsActivity;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.FutureTarget;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class FCMMsgService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        new AppSettings(getApplicationContext()).setAppTokenId(s);
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        Log.e("hai", "onMessageReceived:" + remoteMessage.getData());
        //   Log.e("hai", "onMessageReceived123:" + remoteMessage.getData().get("payload"));


        if (remoteMessage.getData() != null) {

            Intent intent = new Intent(this, DetailNewsActivity.class);
            intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, remoteMessage.getData().get("categoryId"));
            intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, remoteMessage.getData().get("newsId"));
            intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, true);


            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0/* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);

            try {
                FutureTarget<Bitmap> img = Glide.with(this).asBitmap().load(remoteMessage.getData().get("image")).submit();

                Bitmap bitmap = img.get();
                sendNotification(remoteMessage, pendingIntent, bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void sendNotification(RemoteMessage remoteMessage, PendingIntent pendingIntent, Bitmap bitmap) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("0", "Dinasuvadu", importance);
            channel.enableLights(true);
            channel.setLightColor(getColor(R.color.colorPrimary));
            //  NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }

        }
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_logo)
                .setLargeIcon(bitmap)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .setBigContentTitle(remoteMessage.getData().get("body"))
                        .bigPicture(bitmap)
                        .bigLargeIcon(null))
                .setChannelId("0")
                .setDefaults(NotificationCompat.DEFAULT_VIBRATE)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVibrate(new long[]{Notification.DEFAULT_VIBRATE})
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        notificationManager.notify(0, mBuilder.build());

/*
        try {
            JSONObject jsonObject = new JSONObject(remoteMessage.getData().get(ConstantKeys.INTENT_VALUES.PAYLOAD));
            String tvMediaId = "";
            if (jsonObject.has(ConstantKeys.tvMediaId)) {
                tvMediaId = jsonObject.optString(ConstantKeys.tvMediaId);
            }
            if (remoteMessage.getData().get("notifyType").equalsIgnoreCase(ChatConstant.SINGLECHAT)) {
                if (!new AppSettings(getApplicationContext()).getIsChatToUser().equalsIgnoreCase(remoteMessage.getData().get(ConstantKeys.INTENT_VALUES.USER_ID)))
                    notificationManager.notify(0, mBuilder.build());
            } else if (remoteMessage.getData().get("notifyType").equalsIgnoreCase(ConstantKeys.NotificationTypes.TVMEDIACOMMENT) || remoteMessage.getData().get("notifyType").equalsIgnoreCase(ConstantKeys.NotificationTypes.TVMEDIASTAR) || remoteMessage.getData().get("notifyType").equalsIgnoreCase(ConstantKeys.NotificationTypes.TVMEDIACOMMENTSTAR) || remoteMessage.getData().get("notifyType").equalsIgnoreCase(ConstantKeys.NotificationTypes.TVMEDIAREPLYCOMMENT)) {
                if (!new AppSettings(getApplicationContext()).getTvVideoId().equalsIgnoreCase(tvMediaId))
                    notificationManager.notify(0, mBuilder.build());
            } else {
                notificationManager.notify(0, mBuilder.build());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
*/
    }


}
