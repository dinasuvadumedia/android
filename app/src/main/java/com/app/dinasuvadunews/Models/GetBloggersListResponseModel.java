package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetBloggersListResponseModel implements Serializable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("allBlogs")
    @Expose
    private List<PopularBloggersListResponseModel> popularBlogs = null;
    @SerializedName("bloggersList")
    @Expose
    private List<OurBloggersListResponseModel> bloggersList = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<PopularBloggersListResponseModel> getPopularBlogs() {
        return popularBlogs;
    }

    public void setPopularBlogs(List<PopularBloggersListResponseModel> popularBlogs) {
        this.popularBlogs = popularBlogs;
    }

    public List<OurBloggersListResponseModel> getBloggersList() {
        return bloggersList;
    }

    public void setBloggersList(List<OurBloggersListResponseModel> bloggersList) {
        this.bloggersList = bloggersList;
    }


}
