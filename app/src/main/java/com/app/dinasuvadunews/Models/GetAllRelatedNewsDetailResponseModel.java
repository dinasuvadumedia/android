package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class GetAllRelatedNewsDetailResponseModel implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("mintesRead")
    @Expose
    private String mintesRead;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("categoryName")
    @Expose
    private String categoryName;
    @SerializedName("shortDescription")
    @Expose
    private String shortDescription;
    @SerializedName("longDescription")
    @Expose
    private String longDescription;
    @SerializedName("urlToImage")
    @Expose
    private String urlToImage;
    @SerializedName("isLiked")
    @Expose
    private Integer isLiked = 0;
    @SerializedName("likeCount")
    @Expose
    private Integer likeCount = 0;

    @SerializedName("suggestedNews")
    @Expose
    private List<GetAllRelatedNewsDetailResponseTwoModel> suggestedNews = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMintesRead() {
        return mintesRead;
    }

    public void setMintesRead(String mintesRead) {
        this.mintesRead = mintesRead;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public List<GetAllRelatedNewsDetailResponseTwoModel> getSuggestedNews() {
        return suggestedNews;
    }

    public void setSuggestedNews(List<GetAllRelatedNewsDetailResponseTwoModel> suggestedNews) {
        this.suggestedNews = suggestedNews;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public Integer getLikeCount() {
        return likeCount;
    }

    public void setLikeCount(Integer likeCount) {
        this.likeCount = likeCount;
    }
}