package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class PopularBloggersListResponseModel implements Serializable {
    @SerializedName("blogId")
    @Expose
    private String blogId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("isLiked")
    @Expose
    private Integer isLiked;
    @SerializedName("likesCount")
    @Expose
    private Integer likesCount;
    @SerializedName("bloggerImage")
    @Expose
    private String bloggerImage;
    @SerializedName("bloggerName")
    @Expose
    private String bloggerName;
    @SerializedName("bloggerId")
    @Expose
    private String bloggerId;
    @SerializedName("isFollow")
    @Expose
    private Integer isFollow;
    @SerializedName("followers")
    @Expose
    private Integer followers;

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public String getBloggerImage() {
        return bloggerImage;
    }

    public void setBloggerImage(String bloggerImage) {
        this.bloggerImage = bloggerImage;
    }

    public String getBloggerName() {
        return bloggerName;
    }

    public void setBloggerName(String bloggerName) {
        this.bloggerName = bloggerName;
    }

    public String getBloggerId() {
        return bloggerId;
    }

    public void setBloggerId(String bloggerId) {
        this.bloggerId = bloggerId;
    }

    public Integer getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(Integer isFollow) {
        this.isFollow = isFollow;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

}