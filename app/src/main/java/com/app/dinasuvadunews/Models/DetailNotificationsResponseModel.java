package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DetailNotificationsResponseModel implements Serializable {

    @SerializedName("newsId")
    @Expose
    private String newsId;
    @SerializedName("categoryId")
    @Expose
    private String categoryId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("urlToImage")
    @Expose
    private String urlToImage;
    @SerializedName("mintesRead")
    @Expose
    private String mintesRead;
    @SerializedName("isSeen")
    @Expose
    private Integer isSeen;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;

    public String getNewsId() {
        return newsId;
    }

    public void setNewsId(String newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    public String getMintesRead() {
        return mintesRead;
    }

    public void setMintesRead(String mintesRead) {
        this.mintesRead = mintesRead;
    }

    public Integer getIsSeen() {
        return isSeen;
    }

    public void setIsSeen(Integer isSeen) {
        this.isSeen = isSeen;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }
}
