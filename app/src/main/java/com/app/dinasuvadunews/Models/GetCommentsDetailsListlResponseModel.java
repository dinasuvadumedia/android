package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class GetCommentsDetailsListlResponseModel implements Serializable {
    @SerializedName("udId")
    @Expose
    private String udId;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("comment")
    @Expose
    private String comment;

    public String getUdId() {
        return udId;
    }

    public void setUdId(String udId) {
        this.udId = udId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}