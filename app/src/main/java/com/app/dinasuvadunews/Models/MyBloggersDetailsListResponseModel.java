package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class MyBloggersDetailsListResponseModel implements Serializable {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("bloggerDetails")
    @Expose
    private BloggersDetailsListResponseModel bloggerDetails;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public BloggersDetailsListResponseModel getBloggerDetails() {
        return bloggerDetails;
    }

    public void setBloggerDetails(BloggersDetailsListResponseModel bloggerDetails) {
        this.bloggerDetails = bloggerDetails;
    }

}