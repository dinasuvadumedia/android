package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class BloggersDetailTwoListResponseModel implements Serializable {
    @SerializedName("blogId")
    @Expose
    private String blogId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("mintesRead")
    @Expose
    private String mintesRead;
    @SerializedName("isLiked")
    @Expose
    private Integer isLiked;
    @SerializedName("likesCount")
    @Expose
    private Integer likesCount;

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public Integer getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Integer isLiked) {
        this.isLiked = isLiked;
    }

    public Integer getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(Integer likesCount) {
        this.likesCount = likesCount;
    }

    public String getMintesRead() {
        return mintesRead;
    }

    public void setMintesRead(String mintesRead) {
        this.mintesRead = mintesRead;
    }

}