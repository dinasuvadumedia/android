package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class AllCategories implements Serializable {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("categoryList")
    @Expose
    private List<CategoriesDetails> categories = null;
    private final static long serialVersionUID = -7044024180309921241L;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<CategoriesDetails> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoriesDetails> categories) {
        this.categories = categories;
    }

}