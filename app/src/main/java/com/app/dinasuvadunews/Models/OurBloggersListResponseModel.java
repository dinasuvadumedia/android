package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class OurBloggersListResponseModel implements Serializable {
    @SerializedName("bloggerId")
    @Expose
    private String bloggerId;
    @SerializedName("bloggerImage")
    @Expose
    private String bloggerImage;
    @SerializedName("bloggername")
    @Expose
    private String bloggername;
    @SerializedName("isFollow")
    @Expose
    private Integer isFollow;
    @SerializedName("followers")
    @Expose
    private Integer followers;

    public String getBloggerId() {
        return bloggerId;
    }

    public void setBloggerId(String bloggerId) {
        this.bloggerId = bloggerId;
    }

    public String getBloggerImage() {
        return bloggerImage;
    }

    public void setBloggerImage(String bloggerImage) {
        this.bloggerImage = bloggerImage;
    }

    public String getBloggername() {
        return bloggername;
    }

    public void setBloggername(String bloggername) {
        this.bloggername = bloggername;
    }

    public Integer getIsFollow() {
        return isFollow;
    }

    public void setIsFollow(Integer isFollow) {
        this.isFollow = isFollow;
    }

    public Integer getFollowers() {
        return followers;
    }

    public void setFollowers(Integer followers) {
        this.followers = followers;
    }

}