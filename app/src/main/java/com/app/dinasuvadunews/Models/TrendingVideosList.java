package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class TrendingVideosList implements Serializable {
    @SerializedName("videoId")
    @Expose
    private String videoId;
    @SerializedName("urlToVideo")
    @Expose
    private String urlToVideo;

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public String getUrlToVideo() {
        return urlToVideo;
    }

    public void setUrlToVideo(String urlToVideo) {
        this.urlToVideo = urlToVideo;
    }

}