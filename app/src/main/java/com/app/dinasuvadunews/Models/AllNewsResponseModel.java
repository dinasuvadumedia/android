package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AllNewsResponseModel implements Serializable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("totalPage")
    @Expose
    private Integer totalPage;
    @SerializedName("listNews")
    @Expose
    private List<AllNewsDetailsResponseModel> newsfeed = null;
    private final static long serialVersionUID = -5687257278259173792L;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<AllNewsDetailsResponseModel> getNewsfeed() {
        return newsfeed;
    }

    public void setNewsfeed(List<AllNewsDetailsResponseModel> newsfeed) {
        this.newsfeed = newsfeed;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }


}
