package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class VideosListResponseModel implements Serializable {


    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("totalPage")
    @Expose
    private Integer totalPage;
    @SerializedName("listVideos")
    @Expose
    private List<VideosListDetailsResponseModel> listVideos = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public List<VideosListDetailsResponseModel> getListVideos() {
        return listVideos;
    }

    public void setListVideos(List<VideosListDetailsResponseModel> listVideos) {
        this.listVideos = listVideos;
    }


}
