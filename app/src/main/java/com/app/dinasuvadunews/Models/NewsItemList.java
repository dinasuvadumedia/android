package com.app.dinasuvadunews.Models;


public class NewsItemList {

    private final String title;
    private final String imageUrl;
    private final String postedTime;
    private final String newsId;
    private final String categoryId;

    public String getMintsRead() {
        return mintsRead;
    }

    private final String mintsRead;

    public String getCategoryName() {
        return categoryName;
    }

    private final String categoryName;

    public String getDescription() {
        return description;
    }

    private final String description;


    public NewsItemList(String title, String imageUrl, String postedTime, String description, String newsId, String categoryId, String categoryName, String mintsRead) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.newsId = newsId;
        this.categoryId = categoryId;
        this.postedTime = postedTime;
        this.categoryName = categoryName;
        this.mintsRead = mintsRead;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public String getNewsId() {
        return newsId;
    }

    public String getCategoryId() {
        return categoryId;
    }


}
