package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class GetCommentsListlResponseModel implements Serializable {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listComments")
    @Expose
    private List<GetCommentsDetailsListlResponseModel> listComments = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<GetCommentsDetailsListlResponseModel> getListComments() {
        return listComments;
    }

    public void setListComments(List<GetCommentsDetailsListlResponseModel> listComments) {
        this.listComments = listComments;
    }
}