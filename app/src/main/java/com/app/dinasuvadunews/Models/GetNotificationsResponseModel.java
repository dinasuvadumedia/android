package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetNotificationsResponseModel implements Serializable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listNotification")
    @Expose
    private List<DetailNotificationsResponseModel> listNotification = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<DetailNotificationsResponseModel> getListNotification() {
        return listNotification;
    }

    public void setListNotification(List<DetailNotificationsResponseModel> listNotification) {
        this.listNotification = listNotification;
    }


}
