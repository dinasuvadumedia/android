package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class GetAllRelatedNewsResponseModel implements Serializable {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listNews")
    @Expose
    private List<GetAllRelatedNewsDetailResponseModel> listNews = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<GetAllRelatedNewsDetailResponseModel> getListNews() {
        return listNews;
    }

    public void setListNews(List<GetAllRelatedNewsDetailResponseModel> listNews) {
        this.listNews = listNews;
    }


}