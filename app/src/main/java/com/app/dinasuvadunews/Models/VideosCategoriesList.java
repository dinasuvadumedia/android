package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class VideosCategoriesList implements Serializable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("listCategory")
    @Expose
    private List<VideosCategoriesDetailsList> listCategory = null;
    @SerializedName("listvideos")
    @Expose
    private List<TrendingVideosList> listvideos = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public List<VideosCategoriesDetailsList> getListCategory() {
        return listCategory;
    }

    public void setListCategory(List<VideosCategoriesDetailsList> listCategory) {
        this.listCategory = listCategory;
    }

    public List<TrendingVideosList> getListvideos() {
        return listvideos;
    }

    public void setListvideos(List<TrendingVideosList> listvideos) {
        this.listvideos = listvideos;
    }

}