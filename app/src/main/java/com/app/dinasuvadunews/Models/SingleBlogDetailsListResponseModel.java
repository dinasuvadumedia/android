package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class SingleBlogDetailsListResponseModel implements Serializable {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errorMessage")
    @Expose
    private String errorMessage;
    @SerializedName("blogDetails")
    @Expose
    private SingleBlogDetailsListTwoResponseModel blogDetails;

    @SerializedName("bloggerDetails")
    @Expose
    private BlogersInfoListResponseModel blogerInfoDetails;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public SingleBlogDetailsListTwoResponseModel getBlogDetails() {
        return blogDetails;
    }

    public void setBlogDetails(SingleBlogDetailsListTwoResponseModel blogDetails) {
        this.blogDetails = blogDetails;
    }


    public BlogersInfoListResponseModel getBlogerInfoDetails() {
        return blogerInfoDetails;
    }

    public void setBlogDetails(BlogersInfoListResponseModel mblogerInfoDetails) {
        this.blogerInfoDetails = mblogerInfoDetails;
    }


}