package com.app.dinasuvadunews.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class AllBloggersListResponseModel implements Serializable {
    @SerializedName("blogId")
    @Expose
    private String blogId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("images")
    @Expose
    private String images;
    @SerializedName("bloggerImage")
    @Expose
    private String bloggerImage;
    @SerializedName("bloggerName")
    @Expose
    private String bloggerName;
    @SerializedName("bloggerId")
    @Expose
    private String bloggerId;

    public String getBlogId() {
        return blogId;
    }

    public void setBlogId(String blogId) {
        this.blogId = blogId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public String getBloggerImage() {
        return bloggerImage;
    }

    public void setBloggerImage(String bloggerImage) {
        this.bloggerImage = bloggerImage;
    }

    public String getBloggerName() {
        return bloggerName;
    }

    public void setBloggerName(String bloggerName) {
        this.bloggerName = bloggerName;
    }

    public String getBloggerId() {
        return bloggerId;
    }

    public void setBloggerId(String bloggerId) {
        this.bloggerId = bloggerId;
    }

}