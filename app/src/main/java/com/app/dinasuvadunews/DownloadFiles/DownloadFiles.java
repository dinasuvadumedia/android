package com.app.dinasuvadunews.DownloadFiles;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;

import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadFiles {

    private static final String TAG = "Download Task";
    private int fileType = 0;
    private Activity context;

    private String downloadUrl = "", downloadFileName = "";

    static CustomDialog customDialog;
    private static boolean isShowing;

    public static void showProgress(Activity context) {
        if (!context.isFinishing()) {
            if (!isShowing) {
                isShowing = true;
                customDialog = new CustomDialog(context);
                customDialog.setCancelable(false);
                customDialog.setCanceledOnTouchOutside(false);
                customDialog.setContentView(R.layout.loading_view);
                customDialog.show();

            }
        }

    }

    public static void dismiss(Activity activity) {
        if (!activity.isFinishing()) {
            try {
                if (isShowing) {
                    isShowing = false;
                    customDialog.dismiss();
                }
            } catch (Exception e) {
                if (isShowing) {
                    isShowing = false;

                } else {
                    isShowing = true;

                }
            }
        } else {
            isShowing = !isShowing;
        }
    }


    public DownloadFiles(Activity context, String downloadUrl, int fileType) {
        this.context = context;
        this.downloadUrl = downloadUrl;
        this.fileType = fileType;

        if (fileType == 0)
            downloadFileName = System.currentTimeMillis() + ".jpg";
        else
            downloadFileName = System.currentTimeMillis() + ".mp4";


        new DownloadingTask().execute();
    }

    private class DownloadingTask extends AsyncTask<Void, Void, Void> {

        File mainFolder = null;
        File outputFile = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgress(context);
        }

        @Override
        protected void onPostExecute(Void result) {

            dismiss(context);

            try {
                if (outputFile != null) {
                    Utilities.showToast(context, context.getString(R.string.downloaded_to_storage));
                } else {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            dismiss(context);
                            Utilities.showToast(context, context.getString(R.string.unable_to_download));
                        }
                    }, 3000);

                }
            } catch (Exception e) {
                e.printStackTrace();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismiss(context);
                        Utilities.showToast(context, context.getString(R.string.unable_to_download));
                    }
                }, 3000);

            }


            super.onPostExecute(result);
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            try {
                URL url = new URL(downloadUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();


                if (c.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    Utilities.log(TAG, "Server returned HTTP " + c.getResponseCode()
                            + " " + c.getResponseMessage());

                }

                if (new CheckForSDCard().isSDCardPresent()) {

                    mainFolder = new File(
                            Environment.getExternalStorageDirectory() + "/"
                                    + Constants.INTENT_VALUES.APP_FOLDER);
                } else
                    Utilities.showToast(context, "Oops!! There is no SD Card.");


                if (!mainFolder.exists()) {
                    mainFolder.mkdir();

                }

                File subFolder;
                if (fileType == 0)
                    subFolder = new File(mainFolder, context.getResources().getString(R.string.image)).getAbsoluteFile();
                else
                    subFolder = new File(mainFolder, context.getResources().getString(R.string.video)).getAbsoluteFile();

                if (!subFolder.exists()) {
                    subFolder.mkdir();
                }

                outputFile = new File(subFolder, downloadFileName);

                //Create New File if not present
                if (!outputFile.exists()) {
                    outputFile.createNewFile();

                }

                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;//init length
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);//Write new file
                }

                fos.close();
                is.close();

            } catch (Exception e) {
                e.printStackTrace();
                outputFile = null;
            }

            return null;
        }
    }
}
