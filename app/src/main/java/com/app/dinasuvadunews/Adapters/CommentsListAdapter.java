package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Models.GetCommentsDetailsListlResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<GetCommentsDetailsListlResponseModel> mcommentsDetailsListlResponseModels;

    public CommentsListAdapter(Activity context, List<GetCommentsDetailsListlResponseModel> commentsDetailsListlResponseModels) {
        this.context = context;
        mcommentsDetailsListlResponseModels = commentsDetailsListlResponseModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.comments_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CommentsListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final CommentsListViewHolder commentsListViewHolder = (CommentsListViewHolder) viewHolder;

        commentsListViewHolder.firstLetterTxtView.setText(String.valueOf(mcommentsDetailsListlResponseModels.get(position).getUserName().charAt(0)));
        commentsListViewHolder.commentTxtViw.setText(mcommentsDetailsListlResponseModels.get(position).getComment());
        commentsListViewHolder.userNameTxtview.setText(mcommentsDetailsListlResponseModels.get(position).getUserName());

        Random rnd = new Random();
        int color = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        commentsListViewHolder.imageLayout.setCardBackgroundColor(color);
    }

    @Override
    public int getItemCount() {

        return mcommentsDetailsListlResponseModels.size();

    }

    public void reloadData(RecyclerView commentsRecylerView, List<GetCommentsDetailsListlResponseModel> commentsList) {

        mcommentsDetailsListlResponseModels.addAll(commentsList);
        notifyDataSetChanged();

        if (mcommentsDetailsListlResponseModels.size() >= 3)
            commentsRecylerView.scrollToPosition(mcommentsDetailsListlResponseModels.size() - 1);
    }


    class CommentsListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.commentTxtViw)
        TextView commentTxtViw;
        @BindView(R.id.firstLetterTxtView)
        TextView firstLetterTxtView;
        @BindView(R.id.imageLayout)
        CardView imageLayout;

        CommentsListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}