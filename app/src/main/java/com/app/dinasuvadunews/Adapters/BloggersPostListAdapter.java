package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.LoginActivity;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerWithTwo;
import com.app.dinasuvadunews.Models.BloggersDetailTwoListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class BloggersPostListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private String mname, mimage;
    private List<BloggersDetailTwoListResponseModel> mbloggersDetailTwoListResponseModels;
    private CommonClickListenerWithTwo mcommonClickListener;

    public BloggersPostListAdapter(Activity context, List<BloggersDetailTwoListResponseModel> bloggersDetailTwoListResponseModels, String name, String image) {
        this.context = context;
        mbloggersDetailTwoListResponseModels = bloggersDetailTwoListResponseModels;
        mname = name;
        mimage = image;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.bloggers_post_list_itemview, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new BloggersPostListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final BloggersPostListViewHolder bloggersPostListViewHolder = (BloggersPostListViewHolder) viewHolder;
        bloggersPostListViewHolder.userNameTxtview.setText(mname);
        new GlideImageLoader(context).loadSingleImgUrl(context, mimage, bloggersPostListViewHolder.bloggersImgView);


        bloggersPostListViewHolder.descriptionTxtView.setText(mbloggersDetailTwoListResponseModels.get(position).getTitle());
        bloggersPostListViewHolder.mintsReadTxtView.setText(mbloggersDetailTwoListResponseModels.get(position).getMintesRead());
        new GlideImageLoader(context).loadSingleImgBlurUrl(context, mbloggersDetailTwoListResponseModels.get(position).getImages(), bloggersPostListViewHolder.blogImgView, bloggersPostListViewHolder.blurImgView);


        if (mbloggersDetailTwoListResponseModels.get(position).getLikesCount() == 1) {
            bloggersPostListViewHolder.likesCountTxtView.setText("1" + " Like");
        } else {
            bloggersPostListViewHolder.likesCountTxtView.setText(mbloggersDetailTwoListResponseModels.get(position).getLikesCount() + " Likes");
        }

        if (mbloggersDetailTwoListResponseModels.get(position).getIsLiked() == 0) {
            bloggersPostListViewHolder.likeImgView.setImageDrawable(context.getDrawable(R.drawable.heart));
        } else
            bloggersPostListViewHolder.likeImgView.setImageDrawable(context.getDrawable(R.drawable.sel_like_heart));


        bloggersPostListViewHolder.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new AppSettings(context).getIsLoggedIn().equalsIgnoreCase(context.getString(R.string.true_val)))
                    mcommonClickListener.onLikeClick(position);
                else
                    context.startActivity(new Intent(context, LoginActivity.class));

            }
        });


        bloggersPostListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onImgClick(position);
            }
        });


        bloggersPostListViewHolder.whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onShareClickWithImg(position, bloggersPostListViewHolder.blogImgView);
            }
        });

        bloggersPostListViewHolder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mcommonClickListener.onDownloadClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {

        return mbloggersDetailTwoListResponseModels.size();

    }

    public void setOnClickListener(CommonClickListenerWithTwo commonClickListener) {

        mcommonClickListener = commonClickListener;

    }

    public void reloadList(List<BloggersDetailTwoListResponseModel> bloggerDetails) {
        this.mbloggersDetailTwoListResponseModels = bloggerDetails;
        notifyDataSetChanged();

    }


    class BloggersPostListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.bloggersImgView)
        CircleImageView bloggersImgView;
        @BindView(R.id.blogImgView)
        ImageView blogImgView;
        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;
        @BindView(R.id.likeLayout)
        LinearLayout likeLayout;
        @BindView(R.id.likeImgView)
        ImageView likeImgView;
        @BindView(R.id.likesCountTxtView)
        TextView likesCountTxtView;
        @BindView(R.id.blurImgView)
        ImageView blurImgView;
        @BindView(R.id.whatsappShareLayout)
        LinearLayout whatsappShareLayout;
        @BindView(R.id.shareLayout)
        LinearLayout shareLayout;
        @BindView(R.id.mintsReadTxtView)
        TextView mintsReadTxtView;

        BloggersPostListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}