package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.CustomViews.RecyclerViewPager;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerWithTwo;
import com.app.dinasuvadunews.Models.TrendingVideosList;
import com.app.dinasuvadunews.Models.VideosListDetailsResponseModel;
import com.app.dinasuvadunews.R;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

import net.alexandroid.utils.exoplayerhelper.ExoPlayerHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TrendingVideosListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final RecyclerViewPager mtrendingVideosRecyclerView;
    private List<TrendingVideosList> mlistvideos;
    private Activity context;
    GlideImageLoader glideImageLoader;
    private CommonClickListenerWithTwo mcommonClickListener;
    public static ExoPlayerHelper mExoPlayerHelper;

    public TrendingVideosListAdapter(Activity context, List<TrendingVideosList> listvideos, RecyclerViewPager trendingVideosRecyclerView) {
        this.context = context;
        mlistvideos = listvideos;
        glideImageLoader = new GlideImageLoader(context);
        mtrendingVideosRecyclerView = trendingVideosRecyclerView;


        mtrendingVideosRecyclerView.addOnPageChangedListener(new RecyclerViewPager.OnPageChangedListener() {
            @Override
            public void OnPageChanged(int oldPosition, int newPosition) {

                if (VideosMainListAdapter.mExoPlayerHelper != null)
                    VideosMainListAdapter.mExoPlayerHelper.playerPause();


                if (mExoPlayerHelper != null) {
                    if (mExoPlayerHelper.isPlayerCreated()) {
                        mExoPlayerHelper.playerPause();
                    }
                }
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.trending_news_item, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new VideosMainListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final VideosMainListViewHolder videosMainListViewHolder = (VideosMainListViewHolder) viewHolder;

        glideImageLoader.loadSingleImgUrl(context, mlistvideos.get(position).getUrlToVideo(), videosMainListViewHolder.trendingImgView);


        videosMainListViewHolder.trendingImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  mcommonClickListener.onImgClick(position);


                videosMainListViewHolder.trendingImgView.setVisibility(View.GONE);


                if (VideosMainListAdapter.mExoPlayerHelper != null)
                    VideosMainListAdapter.mExoPlayerHelper.playerPause();


                if (mExoPlayerHelper != null) {
                    if (mExoPlayerHelper.isPlayerCreated()) {
                        mExoPlayerHelper.playerPause();
                    }
                }

                videosMainListViewHolder.exoPlayerView.setVisibility(View.VISIBLE);
                mExoPlayerHelper = new ExoPlayerHelper.Builder(context, videosMainListViewHolder.exoPlayerView)
                        .addMuteButton(false, false)
                        .setUiControllersVisibility(true)
                        .setAutoPlayOn(true).setRepeatModeOn(false)
                        .setRepeatModeOn(false)
                        .setAutoPlayOn(false)
                        .setVideoUrls(mlistvideos.get(position).getUrlToVideo())
                        .enableLiveStreamSupport()
                        .addProgressBarWithColor(context.getResources().getColor(R.color.colorPrimary))
                        .createAndPrepare();

                mExoPlayerHelper.playerPlay();

            }
        });


    }

    @Override
    public int getItemCount() {

        return mlistvideos.size();

    }

    public void setOnClickListener(CommonClickListenerWithTwo commonClickListener) {

        mcommonClickListener = commonClickListener;

    }

    public void reloadList(List<VideosListDetailsResponseModel> listViral) {
//        this.mvideosListDetailsResponseModels = listViral;
//        notifyDataSetChanged();

    }

    class VideosMainListViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.exoPlayerView)
        SimpleExoPlayerView exoPlayerView;
        @BindView(R.id.trendingImgView)
        ImageView trendingImgView;


        VideosMainListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}