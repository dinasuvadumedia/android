package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.CategoriesDetails;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.ButterKnife;

public class SwipeNewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<CategoriesDetails> mCategoriesList;
    private CommonClickListener mcommonClickListener;

    public SwipeNewsListAdapter(Activity context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.detail_news_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CategoriesListViewholder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoriesListViewholder categoriesListViewholder = (CategoriesListViewholder) viewHolder;


       /* categoriesListViewholder.categoriesNameTxtView.setText(mCategoriesList.get(position).getName());

        if (mCategoriesList.get(position).isSelected()) {
            categoriesListViewholder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.catg_color_selected));
        } else
            categoriesListViewholder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.catg_color_unselected));


        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcommonClickListener.clickListener(position);
                reselectCategory(position);
            }
        });*/
    }


    @Override
    public int getItemCount() {

        return 10;

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class CategoriesListViewholder extends RecyclerView.ViewHolder {

        /*@BindView(R.id.categoriesNameTxtView)
        TextView categoriesNameTxtView;*/

        CategoriesListViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}