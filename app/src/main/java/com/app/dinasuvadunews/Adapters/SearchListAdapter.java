package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.DetailNewsActivity;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.AllNewsDetailsResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<AllNewsDetailsResponseModel> mSearchList;
    private com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList myCommonClickListenerForNewsList;

    public SearchListAdapter(Activity context, List<AllNewsDetailsResponseModel> searchList) {
        this.context = context;
        mSearchList = searchList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.search_list_item_view, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new SearchListViwHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SearchListViwHolder searchListViwHolder = (SearchListViwHolder) viewHolder;


        searchListViwHolder.newsContentTxtView.setText(mSearchList.get(position).getTitle());
        searchListViwHolder.timeTxtView.setText(mSearchList.get(position).getMintesRead());
        searchListViwHolder.newsCatNameTxtView.setText(mSearchList.get(position).getCategoryName());
        searchListViwHolder.newTitleTxtView.setText(mSearchList.get(position).getCategoryName());
        new GlideImageLoader(context).loadImgUrl(mSearchList.get(position).getUrlToImage(), searchListViwHolder.imgView, searchListViwHolder.backgroundImgLayout);


        searchListViwHolder.itemLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailNewsActivity.class);
                intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, mSearchList.get(position).getCategoryId());
                intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, false);
                intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, mSearchList.get(position).getId());
                context.startActivity(intent);
            }
        });

        searchListViwHolder.moreImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                myCommonClickListenerForNewsList.clickListenerForNewsList(position, mSearchList.get(position).getUrlToImage(), mSearchList.get(position).getTitle(), mSearchList.get(position).getId(), mSearchList.get(position).getCategoryId());


            }
        });


        searchListViwHolder.whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, mSearchList.get(position).getUrlToImage(), mSearchList.get(position).getId(), mSearchList.get(position).getCategoryId());
                Utilities.generateDynamicLinkForWhatsappShare(context, myDynamicUri, mSearchList.get(position).getUrlToImage(), mSearchList.get(position).getTitle());

            }
        });


    }

    @Override
    public int getItemCount() {

        return mSearchList.size();

    }

    public void setOnClickListener(com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList commonClickListenerForNewsList) {

        myCommonClickListenerForNewsList = commonClickListenerForNewsList;
    }

    public void reloadList(List<AllNewsDetailsResponseModel> mySearchListResponseModel) {

        mSearchList.addAll(mySearchListResponseModel);
        notifyDataSetChanged();
    }


    class SearchListViwHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.newTitleTxtView)
        TextView newTitleTxtView;
        @BindView(R.id.newsContentTxtView)
        TextView newsContentTxtView;
        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.backgroundImgLayout)
        RelativeLayout backgroundImgLayout;
        @BindView(R.id.itemLayout)
        LinearLayout itemLayout;
        @BindView(R.id.timeTxtView)
        TextView timeTxtView;
        @BindView(R.id.whatsappShareLayout)
        LinearLayout whatsappShareLayout;
        @BindView(R.id.moreImgView)
        ImageView moreImgView;
        @BindView(R.id.newsCatNameTxtView)
        TextView newsCatNameTxtView;

        SearchListViwHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}