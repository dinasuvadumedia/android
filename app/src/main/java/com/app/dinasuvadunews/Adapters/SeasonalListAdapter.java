package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.R;

import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeasonalListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private JSONArray mCategoriesList;

    public SeasonalListAdapter(Activity context, JSONArray categoriesList) {
        this.context = context;
        mCategoriesList = categoriesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.seasonal_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new SeasonalListViwHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SeasonalListViwHolder seasonalListViwHolder = (SeasonalListViwHolder) viewHolder;


    }

    @Override
    public int getItemCount() {

        return 1;

    }


    class SeasonalListViwHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.seasonalNameTxtView)
        TextView seasonalNameTxtView;

        SeasonalListViwHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}