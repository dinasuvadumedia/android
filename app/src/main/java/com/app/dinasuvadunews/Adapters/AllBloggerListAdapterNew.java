package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.AllBloggersListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class AllBloggerListAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<AllBloggersListResponseModel> mallBloggersListResponseModels;
    private CommonClickListener mcommonClickListener;

    public AllBloggerListAdapterNew(Activity context, List<AllBloggersListResponseModel> allBloggersListResponseModels) {
        this.context = context;
        mallBloggersListResponseModels = allBloggersListResponseModels;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AllBloggersListListViewHolder(LayoutInflater.from(context).inflate(R.layout.blogger_item_view_new, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final AllBloggersListListViewHolder allBloggersListListViewHolder = (AllBloggersListListViewHolder) viewHolder;


        allBloggersListListViewHolder.descriptionTxtView.setText(mallBloggersListResponseModels.get(position).getTitle());
        allBloggersListListViewHolder.titleTxtView.setText(mallBloggersListResponseModels.get(position).getBloggerName());

        new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).getBloggerImage(), allBloggersListListViewHolder.profileImgView);
        new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).getImages(), allBloggersListListViewHolder.userCoverImg);


        allBloggersListListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position);
            }
        });

       /* for (int i = 0; i < mallBloggersListResponseModels.get(position).size(); i++) {
            if (i == 0) {

                allBloggersListListViewHolder.descriptionTxtView.setText(mallBloggersListResponseModels.get(position).get(i).getTitle());
                allBloggersListListViewHolder.userNameTxtview.setText(mallBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getBloggerImage(), allBloggersListListViewHolder.profileImgView1);
                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getImages(), allBloggersListListViewHolder.typeOneImgView);
            } else if (i == 1) {
                allBloggersListListViewHolder.descriptionTxtView2.setText(mallBloggersListResponseModels.get(position).get(i).getTitle());
                allBloggersListListViewHolder.userNameTxtview2.setText(mallBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getBloggerImage(), allBloggersListListViewHolder.profileImgView2);
                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getImages(), allBloggersListListViewHolder.typeTwoImgView);
            } else if (i == 2) {
                allBloggersListListViewHolder.descriptionTxtView3.setText(mallBloggersListResponseModels.get(position).get(i).getTitle());
                allBloggersListListViewHolder.userNameTxtview3.setText(mallBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getBloggerImage(), allBloggersListListViewHolder.profileImgView3);
                new GlideImageLoader(context).loadSingleImgUrl(context, mallBloggersListResponseModels.get(position).get(i).getImages(), allBloggersListListViewHolder.typeThreeImgView);
            }
        }

        allBloggersListListViewHolder.typeOneImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 1);
            }
        });


        allBloggersListListViewHolder.typeTwoImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 2);
            }
        });


        allBloggersListListViewHolder.typeThreeImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 3);
            }
        });*/

    }

    private void getTheClickedPosition(int position) {
//        int myClickedPosition = position * 3 + imageTypePosition;
        mcommonClickListener.clickListener(position);
    }

    @Override
    public int getItemCount() {

        return mallBloggersListResponseModels.size();

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position % 3 == 0) {
            return 1;
        } else {
            return 2;
        }
    }


    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class AllBloggersListListViewHolder extends RecyclerView.ViewHolder {

        /*@BindView(R.id.typeOneImgView)
        ImageView typeOneImgView;

        @BindView(R.id.typeTwoImgView)
        ImageView typeTwoImgView;

        @BindView(R.id.typeThreeImgView)
        ImageView typeThreeImgView;


        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.userNameTxtview2)
        TextView userNameTxtview2;
        @BindView(R.id.descriptionTxtView2)
        TextView descriptionTxtView2;


        @BindView(R.id.userNameTxtview3)
        TextView userNameTxtview3;
        @BindView(R.id.descriptionTxtView3)
        TextView descriptionTxtView3;


        @BindView(R.id.profileImgView1)
        CircleImageView profileImgView1;

        @BindView(R.id.profileImgView2)
        CircleImageView profileImgView2;


        @BindView(R.id.profileImgView3)
        CircleImageView profileImgView3;*/

        @BindView(R.id.titleTxtView)
        TextView titleTxtView;

        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.profileImgView)
        CircleImageView profileImgView;

        @BindView(R.id.userCoverImg)
        ImageView userCoverImg;


        AllBloggersListListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}