package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Models.HomeCategoriesDetails;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<HomeCategoriesDetails> mCategoriesList;
    private CommonClickListener mcommonClickListener;

    public CategoriesListAdapter(Activity context, List<HomeCategoriesDetails> categoriesList) {
        this.context = context;
        mCategoriesList = categoriesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.categories_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CategoriesListViewholder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoriesListViewholder categoriesListViewholder = (CategoriesListViewholder) viewHolder;


        categoriesListViewholder.categoriesNameTxtView.setText(mCategoriesList.get(position).getName());


        if (mCategoriesList.get(position).getIsSelected() == 0) {
            categoriesListViewholder.categoriesNameTxtView.setBackground(null);
            categoriesListViewholder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            categoriesListViewholder.categoriesNameTxtView.setBackground(context.getResources().getDrawable(R.drawable.selected_news_bg));
            categoriesListViewholder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.blue));
        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcommonClickListener.clickListener(position);
                reselectCategory(position);
            }
        });
    }

    private void reselectCategory(int position) {


        for (HomeCategoriesDetails categoriesDetails : mCategoriesList) {
            categoriesDetails.setIsSelected(0);
        }

        mCategoriesList.get(position).setIsSelected(1);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {

        return mCategoriesList.size();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class CategoriesListViewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoriesNameTxtView)
        TextView categoriesNameTxtView;

        CategoriesListViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}