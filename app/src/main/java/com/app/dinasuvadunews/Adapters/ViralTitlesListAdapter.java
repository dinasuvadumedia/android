package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.ViralCategoriesDetailsList;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViralTitlesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<ViralCategoriesDetailsList> mCategoriesList;
    GlideImageLoader glideImageLoader;
    private CommonClickListener mcommonClickListener;

    public ViralTitlesListAdapter(Activity context, List<ViralCategoriesDetailsList> categoriesList) {
        this.context = context;
        mCategoriesList = categoriesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.viral_titles_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViralTitlesListViewHolder(itemView);
        glideImageLoader = new GlideImageLoader(context);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ViralTitlesListViewHolder viralTitlesListViewHolder = (ViralTitlesListViewHolder) viewHolder;


//        glideImageLoader.loadSingleImgUrl(context, mCategoriesList.get(position).getCategoryImage(), viralTitlesListViewHolder.titleImgView);
        viralTitlesListViewHolder.titleTxtView.setText(mCategoriesList.get(position).getCategoryName());

        viralTitlesListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                reloadTheList(position);

                mcommonClickListener.clickListener(position);
            }
        });


/*        if (mCategoriesList.get(position).getSelected()) {
            viralTitlesListViewHolder.transBgLayout.setVisibility(View.VISIBLE);
        } else {
            viralTitlesListViewHolder.transBgLayout.setVisibility(View.GONE);
        }*/


        if (!mCategoriesList.get(position).getSelected()) {
            viralTitlesListViewHolder.titleTxtView.setBackground(null);
            viralTitlesListViewHolder.titleTxtView.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            viralTitlesListViewHolder.titleTxtView.setBackground(context.getResources().getDrawable(R.drawable.follow_rect_shape));
            viralTitlesListViewHolder.titleTxtView.setTextColor(context.getResources().getColor(R.color.white));
        }

    }

    private void reloadTheList(int position) {


        for (int i = 0; i < mCategoriesList.size(); i++) {

            if (i == position)
                mCategoriesList.get(i).setSelected(true);
            else
                mCategoriesList.get(i).setSelected(false);

        }


        notifyDataSetChanged();


    }

    @Override
    public int getItemCount() {

        return mCategoriesList.size();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;


    }


    class ViralTitlesListViewHolder extends RecyclerView.ViewHolder {

        //        @BindView(R.id.titleImgView)
//        ImageView titleImgView;
        @BindView(R.id.titleTxtView)
        TextView titleTxtView;
//        @BindView(R.id.transBgLayout)
//        RelativeLayout transBgLayout;

        ViralTitlesListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}