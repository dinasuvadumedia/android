package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.OurBloggersListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class BloggersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<OurBloggersListResponseModel> mbloggersListResponseModels;
    private CommonClickListener mcommonClickListener;

    public BloggersListAdapter(Activity context, List<OurBloggersListResponseModel> bloggersListResponseModels) {
        this.context = context;
        mbloggersListResponseModels = bloggersListResponseModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.bloggers_list_itemview_new, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new BloggersListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final BloggersListViewHolder bloggersListViewHolder = (BloggersListViewHolder) viewHolder;


        bloggersListViewHolder.userNameTxtView.setText(mbloggersListResponseModels.get(position).getBloggername());

        new GlideImageLoader(context).loadSingleImgUrl(context, mbloggersListResponseModels.get(position).getBloggerImage(), bloggersListViewHolder.titleImgView);


        bloggersListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.clickListener(position);

            }
        });
    }

    @Override
    public int getItemCount() {

        return mbloggersListResponseModels.size();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;

    }


    class BloggersListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.titleImgView)
        CircleImageView titleImgView;
        @BindView(R.id.userNameTxtView)
        TextView userNameTxtView;

        BloggersListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}