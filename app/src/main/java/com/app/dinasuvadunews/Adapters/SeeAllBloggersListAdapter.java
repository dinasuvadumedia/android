package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.OurBloggersListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SeeAllBloggersListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<OurBloggersListResponseModel> mourBloggersListResponseModels;
    private CommonClickListener mcommonClickListener;

    public SeeAllBloggersListAdapter(Activity context, List<OurBloggersListResponseModel> ourBloggersListResponseModels) {
        this.context = context;
        mourBloggersListResponseModels = ourBloggersListResponseModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.seeall_bloggers_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new SeeAllBloggersListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final SeeAllBloggersListViewHolder seeAllBloggersListViewHolder = (SeeAllBloggersListViewHolder) viewHolder;


        seeAllBloggersListViewHolder.userNameTxtview.setText(mourBloggersListResponseModels.get(position).getBloggername());

        if (mourBloggersListResponseModels.get(position).getIsFollow() == 1) {
            seeAllBloggersListViewHolder.btnBackgroundView.setBackground(context.getDrawable(R.drawable.follow_rect_shape));
            seeAllBloggersListViewHolder.followTxtView.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            seeAllBloggersListViewHolder.btnBackgroundView.setBackground(context.getDrawable(R.drawable.unfollow_rect_shape));
            seeAllBloggersListViewHolder.followTxtView.setTextColor(context.getResources().getColor(R.color.black));
        }


        seeAllBloggersListViewHolder.followersCountTxtView.setText(mourBloggersListResponseModels.get(position).getFollowers() + " " + "Followers");

        seeAllBloggersListViewHolder.btnBackgroundView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcommonClickListener.clickListener(position);

            }
        });


        new GlideImageLoader(context).loadSingleImgUrl(context, mourBloggersListResponseModels.get(position).getBloggerImage(), seeAllBloggersListViewHolder.followerImgView);
    }

    @Override
    public int getItemCount() {

        return mourBloggersListResponseModels.size();

    }

    public void setClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;

    }


    public void reloadTheVal(List<OurBloggersListResponseModel> bloggersList) {
        this.mourBloggersListResponseModels = bloggersList;
        notifyDataSetChanged();
    }


    class SeeAllBloggersListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.followersCountTxtView)
        TextView followersCountTxtView;
        @BindView(R.id.btnBackgroundView)
        RelativeLayout btnBackgroundView;
        @BindView(R.id.followTxtView)
        TextView followTxtView;
        @BindView(R.id.followerImgView)
        ImageView followerImgView;

        SeeAllBloggersListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}