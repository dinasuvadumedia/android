package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Models.VideosCategoriesDetailsList;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideosTitlesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<VideosCategoriesDetailsList> mCategoriesList;
    private CommonClickListener mcommonClickListener;

    public VideosTitlesListAdapter(Activity context, List<VideosCategoriesDetailsList> categoriesList) {
        this.context = context;
        mCategoriesList = categoriesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.videos_titles_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new VideosTitleListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final VideosTitleListViewHolder videosTitleListViewHolder = (VideosTitleListViewHolder) viewHolder;


        if (!mCategoriesList.get(position).getSelected()) {
            videosTitleListViewHolder.categoriesNameTxtView.setBackground(null);
            videosTitleListViewHolder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.catg_color_unselected));
        } else {
            videosTitleListViewHolder.categoriesNameTxtView.setBackground(context.getResources().getDrawable(R.drawable.follow_rect_shape));
            videosTitleListViewHolder.categoriesNameTxtView.setTextColor(context.getResources().getColor(R.color.white));
        }


        videosTitleListViewHolder.categoriesNameTxtView.setText(mCategoriesList.get(position).getCategoryName());

        videosTitleListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                reloadTheList(position);

                mcommonClickListener.clickListener(position);
            }
        });

    }

    private void reloadTheList(int position) {

        for (int i = 0; i < mCategoriesList.size(); i++) {

            if (i == position)
                mCategoriesList.get(i).setSelected(true);
            else
                mCategoriesList.get(i).setSelected(false);

        }

        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {

        return mCategoriesList.size();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;

    }


    class VideosTitleListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoriesNameTxtView)
        TextView categoriesNameTxtView;

        VideosTitleListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}