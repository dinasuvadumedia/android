package com.app.dinasuvadunews.Adapters;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.R;

import org.json.JSONArray;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseCategoriesListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private JSONArray mCategoriesList;
    private CommonClickListener mcommonClickListener;

    public ChooseCategoriesListAdapter(Activity context, JSONArray categoriesList) {
        this.context = context;
        mCategoriesList = categoriesList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.choose_categories_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new CategoriesListViewholder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final CategoriesListViewholder categoriesListViewholder = (CategoriesListViewholder) viewHolder;


        categoriesListViewholder.categoriesNameTxtView.setText(mCategoriesList.optJSONObject(position).optString(Constants.API_PARAMS.NAME));


        if (mCategoriesList.optJSONObject(position).optInt(Constants.API_PARAMS.IS_SELECTED) == 1) {

            if (position == 0)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color1);
            else if (position == 1)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color2);
            else if (position == 2)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color3);
            else if (position == 3)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color4);
            else if (position == 4)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color5);
            else if (position == 5)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color6);
            else if (position == 6)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color7);
            else if (position == 7)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color8);
            else if (position == 8)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color9);
            else if (position == 9)
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color10);
            else
                Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.color11);


            Utilities.setImgTintColor(categoriesListViewholder.catIconImgView, R.color.white);

        } else {
            Utilities.setImgTintColor(categoriesListViewholder.imgLayout, R.color.cat_bg_color);
            Utilities.setImgTintColor(categoriesListViewholder.catIconImgView, R.color.grey);

        }

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mcommonClickListener.clickListener(position);


            }
        });
    }


    @Override
    public int getItemCount() {

        return mCategoriesList.length();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {
        mcommonClickListener = commonClickListener;

    }

    public void reloadTheList(JSONArray getJsonFormat) {
        this.mCategoriesList = getJsonFormat;
        notifyDataSetChanged();
    }


    class CategoriesListViewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.categoriesNameTxtView)
        TextView categoriesNameTxtView;
        @BindView(R.id.imgLayout)
        ImageView imgLayout;
        @BindView(R.id.catIconImgView)
        ImageView catIconImgView;

        CategoriesListViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}