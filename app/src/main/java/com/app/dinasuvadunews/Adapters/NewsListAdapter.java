package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.DetailNewsActivity;
import com.app.dinasuvadunews.Fragments.NewsFragment;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList;
import com.app.dinasuvadunews.Models.NewsItemList;
import com.app.dinasuvadunews.R;
import com.google.android.gms.ads.AdView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade;

public class NewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = NewsListAdapter.class.getSimpleName();
    private Activity context;
    // private JSONArray mNewsList;
    private static final int MENU_ITEM_VIEW_TYPE = 0;
    // The banner ad view type.
    private static final int BANNER_AD_VIEW_TYPE = 1;

    private List<Object> recyclerViewItems;
    private CommonClickListenerForNewsList mcommonClickListener;

    public NewsListAdapter(Activity context, List<Object> recyclerViewItems) {
        this.context = context;
        this.recyclerViewItems = recyclerViewItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                View newsListLayout = LayoutInflater.from(context).inflate(
                        R.layout.news_list_itemview, parent, false);
                return new NewsListViewholder(newsListLayout);
            case BANNER_AD_VIEW_TYPE:
                // fall through
            default:
                View bannerLayoutView = LayoutInflater.from(
                        context).inflate(R.layout.banner_ad_container,
                        parent, false);
                return new AdViewHolder(bannerLayoutView);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        int viewType = getItemViewType(position);
        switch (viewType) {
            case MENU_ITEM_VIEW_TYPE:
                final NewsListViewholder newsListViewholder = (NewsListViewholder) viewHolder;
                final NewsItemList menuItem = (NewsItemList) recyclerViewItems.get(position);

                newsListViewholder.newsContentTxtView.setText(menuItem.getTitle());
                newsListViewholder.timeTxtView.setText(menuItem.getMintsRead());
                newsListViewholder.newsCatNameTxtView.setText(menuItem.getCategoryName());
                newsListViewholder.newTitleTxtView.setText(menuItem.getCategoryName());
                new GlideImageLoader(context).loadImgUrl(menuItem.getImageUrl(), newsListViewholder.imgView, newsListViewholder.backgroundImgLayout);


                newsListViewholder.itemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context, DetailNewsActivity.class);
                        intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, menuItem.getCategoryId());
                        intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, false);
                        intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, menuItem.getNewsId());
                        context.startActivity(intent);
                    }
                });


                newsListViewholder.whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Utilities.shareInWhatsappDialog(context, menuItem.getImageUrl());

                        String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, menuItem.getImageUrl(), menuItem.getNewsId(), menuItem.getCategoryId());
                        Utilities.generateDynamicLinkForWhatsappShare(context, myDynamicUri, menuItem.getImageUrl(), menuItem.getTitle());

                    }
                });


                newsListViewholder.moreImgView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        mcommonClickListener.clickListenerForNewsList(position, menuItem.getImageUrl(), menuItem.getTitle(), menuItem.getNewsId(), menuItem.getCategoryId());


                    }
                });

                break;
            case BANNER_AD_VIEW_TYPE:
                // fall through
            default:
                AdViewHolder bannerHolder = (AdViewHolder) viewHolder;
                AdView adView = (AdView) recyclerViewItems.get(position);
                ViewGroup adCardView = (ViewGroup) bannerHolder.itemView;

                if (adCardView.getChildCount() > 0) {
                    adCardView.removeAllViews();
                }
                if (adView.getParent() != null) {
                    ((ViewGroup) adView.getParent()).removeView(adView);
                }

                adCardView.addView(adView);
        }


    }

    @Override
    public int getItemCount() {

        return recyclerViewItems.size();

    }

    @Override
    public int getItemViewType(int position) {

        if (position == 0) {
            return MENU_ITEM_VIEW_TYPE;
        } else {
            return (position % NewsFragment.ITEMS_PER_AD == 0) ? BANNER_AD_VIEW_TYPE
                    : MENU_ITEM_VIEW_TYPE;
        }
    }

    public void setOnClickListener(CommonClickListenerForNewsList commonClickListener) {

        mcommonClickListener = commonClickListener;
    }

    public void reloadTheList(List<Object> newRecyclerViewItems) {

        this.recyclerViewItems = recyclerViewItems;
        notifyDataSetChanged();

    }

    class NewsListViewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.newTitleTxtView)
        TextView newTitleTxtView;
        @BindView(R.id.newsContentTxtView)
        TextView newsContentTxtView;
        @BindView(R.id.imgView)
        ImageView imgView;
        @BindView(R.id.backgroundImgLayout)
        RelativeLayout backgroundImgLayout;
        @BindView(R.id.itemLayout)
        LinearLayout itemLayout;
        @BindView(R.id.timeTxtView)
        TextView timeTxtView;
        @BindView(R.id.whatsappShareLayout)
        LinearLayout whatsappShareLayout;
        @BindView(R.id.moreImgView)
        ImageView moreImgView;
        @BindView(R.id.newsCatNameTxtView)
        TextView newsCatNameTxtView;

        NewsListViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class AdViewHolder extends RecyclerView.ViewHolder {

        AdViewHolder(View view) {
            super(view);
        }
    }

}