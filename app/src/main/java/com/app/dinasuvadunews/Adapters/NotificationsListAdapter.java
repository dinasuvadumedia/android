package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.DetailNewsActivity;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.DetailNotificationsResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<DetailNotificationsResponseModel> mnotificationsResponseModels;
    private CommonClickListener mcommonClickListener;

    public NotificationsListAdapter(Activity context, List<DetailNotificationsResponseModel> notificationsResponseModels) {
        this.context = context;
        mnotificationsResponseModels = notificationsResponseModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.notifications_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new NotificationsListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final NotificationsListViewHolder notificationsListViewHolder = (NotificationsListViewHolder) viewHolder;


        if (mnotificationsResponseModels.get(position).getIsSeen() == 0) {
            notificationsListViewHolder.notificationContentTxtView.setTextColor(context.getResources().getColor(R.color.black));
        } else {
            notificationsListViewHolder.notificationContentTxtView.setTextColor(context.getResources().getColor(R.color.grey));
        }

        notificationsListViewHolder.notificationContentTxtView.setText(mnotificationsResponseModels.get(position).getTitle());

        new GlideImageLoader(context).loadSingleImgUrl(context, mnotificationsResponseModels.get(position).getUrlToImage(), notificationsListViewHolder.notificationImgView);


        notificationsListViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, DetailNewsActivity.class);
                intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, mnotificationsResponseModels.get(position).getCategoryId());
                intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, mnotificationsResponseModels.get(position).getNewsId());
                intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, false);
                context.startActivity(intent);

                if (mnotificationsResponseModels.get(position).getIsSeen() == 0) {
                    mcommonClickListener.clickListener(position);
                }


            }
        });
    }

    @Override
    public int getItemCount() {

        return mnotificationsResponseModels.size();

    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class NotificationsListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notificationContentTxtView)
        TextView notificationContentTxtView;
        @BindView(R.id.notificationImgView)
        CircleImageView notificationImgView;

        NotificationsListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}