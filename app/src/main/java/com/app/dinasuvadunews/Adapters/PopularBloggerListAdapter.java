package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.PopularBloggersListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PopularBloggerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private ArrayList<ArrayList<PopularBloggersListResponseModel>> mpopularBloggersListResponseModels;
    private CommonClickListener mcommonClickListener;

    public PopularBloggerListAdapter(Activity context, ArrayList<ArrayList<PopularBloggersListResponseModel>> popularBloggersListResponseModels) {
        this.context = context;
        mpopularBloggersListResponseModels = popularBloggersListResponseModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 2) {
            return new PopularBloggersListViewHolder(LayoutInflater.from(context).inflate(R.layout.type_two_blog_itemview, parent, false));
        } else {
            return new PopularBloggersListViewHolder(LayoutInflater.from(context).inflate(R.layout.type_one_blog_itemview, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final PopularBloggersListViewHolder popularBloggersListViewHolder = (PopularBloggersListViewHolder) viewHolder;

        // popularBloggersListViewHolder.bloggerDescTxtView.setText(mpopularBloggersListResponseModels.get(position).getTitle());
        //popularBloggersListViewHolder.bloggerNameTxtView.setText(mpopularBloggersListResponseModels.get(position).getBloggerName());


        for (int i = 0; i < mpopularBloggersListResponseModels.get(position).size(); i++) {
            if (i == 0) {

                popularBloggersListViewHolder.descriptionTxtView.setText(mpopularBloggersListResponseModels.get(position).get(i).getTitle());
                popularBloggersListViewHolder.userNameTxtview.setText(mpopularBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getBloggerImage(), popularBloggersListViewHolder.profileImgView1);
                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getImages(), popularBloggersListViewHolder.typeOneImgView);
            } else if (i == 1) {
                popularBloggersListViewHolder.descriptionTxtView2.setText(mpopularBloggersListResponseModels.get(position).get(i).getTitle());
                popularBloggersListViewHolder.userNameTxtview2.setText(mpopularBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getBloggerImage(), popularBloggersListViewHolder.profileImgView2);
                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getImages(), popularBloggersListViewHolder.typeTwoImgView);
            } else if (i == 2) {
                popularBloggersListViewHolder.descriptionTxtView3.setText(mpopularBloggersListResponseModels.get(position).get(i).getTitle());
                popularBloggersListViewHolder.userNameTxtview3.setText(mpopularBloggersListResponseModels.get(position).get(i).getBloggerName());

                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getBloggerImage(), popularBloggersListViewHolder.profileImgView3);
                new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).get(i).getImages(), popularBloggersListViewHolder.typeThreeImgView);
            }
        }

        popularBloggersListViewHolder.typeOneImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 1);
            }
        });


        popularBloggersListViewHolder.typeTwoImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 2);
            }
        });


        popularBloggersListViewHolder.typeThreeImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getTheClickedPosition(position, 3);
            }
        });

    }

    private void getTheClickedPosition(int position, int imageTypePosition) {
        int myClickedPosition = position * 3 + imageTypePosition;
        mcommonClickListener.clickListener(myClickedPosition);
    }

    @Override
    public int getItemCount() {

        return mpopularBloggersListResponseModels.size();

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 || position % 3 == 0) {
            return 1;
        } else {
            return 2;
        }
    }

    public void setOnClickListener(CommonClickListener commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class PopularBloggersListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.typeOneImgView)
        ImageView typeOneImgView;

        @BindView(R.id.typeTwoImgView)
        ImageView typeTwoImgView;

        @BindView(R.id.typeThreeImgView)
        ImageView typeThreeImgView;


        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.userNameTxtview2)
        TextView userNameTxtview2;
        @BindView(R.id.descriptionTxtView2)
        TextView descriptionTxtView2;


        @BindView(R.id.userNameTxtview3)
        TextView userNameTxtview3;
        @BindView(R.id.descriptionTxtView3)
        TextView descriptionTxtView3;


        @BindView(R.id.profileImgView1)
        CircleImageView profileImgView1;

        @BindView(R.id.profileImgView2)
        CircleImageView profileImgView2;


        @BindView(R.id.profileImgView3)
        CircleImageView profileImgView3;
        PopularBloggersListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}