package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.LoginActivity;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Models.VideosListDetailsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.interfaces.CommonClickListenerForVideos;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

import net.alexandroid.utils.exoplayerhelper.ExoPlayerHelper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideosMainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<VideosListDetailsResponseModel> mvideosListDetailsResponseModels;
    private Activity context;
    GlideImageLoader glideImageLoader;
    private CommonClickListenerForVideos mcommonClickListener;
    public static ExoPlayerHelper mExoPlayerHelper;

    public VideosMainListAdapter(Activity context, List<VideosListDetailsResponseModel> videosListDetailsResponseModels) {
        this.context = context;
        mvideosListDetailsResponseModels = videosListDetailsResponseModels;
        glideImageLoader = new GlideImageLoader(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.videos_main_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new VideosMainListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final VideosMainListViewHolder videosMainListViewHolder = (VideosMainListViewHolder) viewHolder;

        videosMainListViewHolder.descriptionTxtView.setText(mvideosListDetailsResponseModels.get(position).getTitle());

        glideImageLoader.loadSingleImgUrl(context, mvideosListDetailsResponseModels.get(position).getUrlToVideo(), videosMainListViewHolder.titleImgView);


        if (mvideosListDetailsResponseModels.get(position).getLikeCount() == 1) {
            videosMainListViewHolder.likeTxtView.setText("1" + " Like");
        } else {
            videosMainListViewHolder.likeTxtView.setText(mvideosListDetailsResponseModels.get(position).getLikeCount() + " Likes");
        }

        videosMainListViewHolder.playVideoImgViw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //  mcommonClickListener.onImgClick(position);

                if (mExoPlayerHelper != null) {
                    if (mExoPlayerHelper.isPlayerCreated()) {
                        mExoPlayerHelper.playerPause();
                    }
                }

                if (TrendingVideosListAdapter.mExoPlayerHelper != null)
                    TrendingVideosListAdapter.mExoPlayerHelper.playerPause();


                videosMainListViewHolder.exoPlayerView.setVisibility(View.VISIBLE);
                mExoPlayerHelper = new ExoPlayerHelper.Builder(context, videosMainListViewHolder.exoPlayerView)
                        .addMuteButton(false, false)
                        .setUiControllersVisibility(true)
                        .setAutoPlayOn(true).setRepeatModeOn(false)
                        .setRepeatModeOn(false)
                        .setAutoPlayOn(false)
                        .setVideoUrls(mvideosListDetailsResponseModels.get(position).getUrlToVideo())
                        .enableLiveStreamSupport()
                        .addProgressBarWithColor(context.getResources().getColor(R.color.colorPrimary))
                        .createAndPrepare();

                mExoPlayerHelper.playerPlay();

            }
        });

        videosMainListViewHolder.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (new AppSettings(context).getIsLoggedIn().equalsIgnoreCase(context.getString(R.string.true_val))) {
                    mcommonClickListener.onLikeClick(position,mvideosListDetailsResponseModels);
                } else
                    context.startActivity(new Intent(context, LoginActivity.class));

            }
        });

        videosMainListViewHolder.downloadLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onDownloadClick(position,mvideosListDetailsResponseModels);

            }
        });


        videosMainListViewHolder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onShareClick(position,mvideosListDetailsResponseModels);
            }
        });

        if (mvideosListDetailsResponseModels.get(position).getIsLiked() == 0) {
            videosMainListViewHolder.likeImgView.setImageDrawable(context.getDrawable(R.drawable.heart));
        } else
            videosMainListViewHolder.likeImgView.setImageDrawable(context.getDrawable(R.drawable.sel_like_heart));


    }

    @Override
    public int getItemCount() {

        return mvideosListDetailsResponseModels.size();

    }

    public void setOnClickListener(CommonClickListenerForVideos commonClickListener) {

        mcommonClickListener = commonClickListener;

    }

    public void reloadList(List<VideosListDetailsResponseModel> listViral) {
        this.mvideosListDetailsResponseModels = listViral;
        notifyDataSetChanged();

    }


    public void notifyList(List<VideosListDetailsResponseModel> listViral) {
        mvideosListDetailsResponseModels.addAll(listViral);
        notifyDataSetChanged();
    }


    class VideosMainListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.titleImgView)
        ImageView titleImgView;
        @BindView(R.id.exoPlayerView)
        SimpleExoPlayerView exoPlayerView;

        @BindView(R.id.playVideoImgViw)
        ImageView playVideoImgViw;
        @BindView(R.id.likeLayout)
        LinearLayout likeLayout;
        @BindView(R.id.likeImgView)
        ImageView likeImgView;
        @BindView(R.id.downloadLayout)
        LinearLayout downloadLayout;
        @BindView(R.id.shareLayout)
        LinearLayout shareLayout;
        @BindView(R.id.likeTxtView)
        TextView likeTxtView;


        VideosMainListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}