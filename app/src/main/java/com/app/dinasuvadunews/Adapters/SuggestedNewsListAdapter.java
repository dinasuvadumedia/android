package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.DetailNewsActivity;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsDetailResponseTwoModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SuggestedNewsListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<GetAllRelatedNewsDetailResponseTwoModel> mgetAllRelatedNewsDetailResponseTwoModels;
    private CommonClickListenerForNewsList mcommonClickListenerForNewsList;

    public SuggestedNewsListAdapter(Activity context, List<GetAllRelatedNewsDetailResponseTwoModel> getAllRelatedNewsDetailResponseTwoModels) {
        this.context = context;
        mgetAllRelatedNewsDetailResponseTwoModels = getAllRelatedNewsDetailResponseTwoModels;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.suggested_news_list_itemview, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new NewsListViewholder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final NewsListViewholder newsListViewholder = (NewsListViewholder) viewHolder;


        new GlideImageLoader(context).loadSingleImgUrl(context, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getUrlToImage(), newsListViewholder.newsImgView);


        newsListViewholder.newsContentTxtView.setText(mgetAllRelatedNewsDetailResponseTwoModels.get(position).getTitle());

        newsListViewholder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailNewsActivity.class);
                intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getCategoryId());
                intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getId());
                intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, false);
                context.startActivity(intent);

            }
        });


        newsListViewholder.whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getUrlToImage(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getId(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getCategoryId());
                Utilities.generateDynamicLinkForWhatsappShare(context, myDynamicUri, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getUrlToImage(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getTitle());

            }
        });


        newsListViewholder.moreImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListenerForNewsList.clickListenerForNewsList(position, mgetAllRelatedNewsDetailResponseTwoModels.get(position).getUrlToImage(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getTitle(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getId(), mgetAllRelatedNewsDetailResponseTwoModels.get(position).getCategoryId());


            }
        });

    }

    @Override
    public int getItemCount() {

        return mgetAllRelatedNewsDetailResponseTwoModels.size();

    }

    public void setOnClickListener(CommonClickListenerForNewsList commonClickListenerForNewsList) {

        mcommonClickListenerForNewsList = commonClickListenerForNewsList;
    }


    class NewsListViewholder extends RecyclerView.ViewHolder {

        @BindView(R.id.newsContentTxtView)
        TextView newsContentTxtView;
        @BindView(R.id.newsImgView)
        ImageView newsImgView;
        @BindView(R.id.moreImgView)
        ImageView moreImgView;
        @BindView(R.id.whatsappShareLayout)
        LinearLayout whatsappShareLayout;

        NewsListViewholder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}