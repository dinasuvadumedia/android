package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.content.Intent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Activities.LoginActivity;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Models.ViralListDetailsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.interfaces.CommonClickListenerForViral;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ViralMainListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<ViralListDetailsResponseModel> mviralListDetailsResponseModels;
    private GlideImageLoader glideImageLoader;
    private CommonClickListenerForViral mcommonClickListener;

    public ViralMainListAdapter(Activity context, List<ViralListDetailsResponseModel> viralListDetailsResponseModels) {
        this.context = context;
        mviralListDetailsResponseModels = viralListDetailsResponseModels;
        glideImageLoader = new GlideImageLoader(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.viral_main_list, parent, false);
        RecyclerView.ViewHolder viewHolder;
        viewHolder = new ViralMainListViewHolder(itemView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ViralMainListViewHolder viralMainListViewHolder = (ViralMainListViewHolder) viewHolder;


        viralMainListViewHolder.descriptionTxtView.setText(mviralListDetailsResponseModels.get(position).getTitle());
        glideImageLoader.loadSingleImgUrl(context, mviralListDetailsResponseModels.get(position).getUrlToViral(), viralMainListViewHolder.titleImgView);


        if (mviralListDetailsResponseModels.get(position).getLikeCount() == 1) {
            viralMainListViewHolder.likesCountTxtView.setText("1" + " Like");
        } else {
            viralMainListViewHolder.likesCountTxtView.setText(mviralListDetailsResponseModels.get(position).getLikeCount() + " Likes");
        }

        viralMainListViewHolder.titleImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onImgClick(position, mviralListDetailsResponseModels);
            }
        });


        viralMainListViewHolder.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new AppSettings(context).getIsLoggedIn().equalsIgnoreCase(context.getString(R.string.true_val)))
                    mcommonClickListener.onLikeClick(position, mviralListDetailsResponseModels);
                else
                    context.startActivity(new Intent(context, LoginActivity.class));

            }
        });

        viralMainListViewHolder.downloadLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onDownloadClick(position, mviralListDetailsResponseModels);

            }
        });


        viralMainListViewHolder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onShareClickWithImg(position, viralMainListViewHolder.titleImgView, mviralListDetailsResponseModels);

            }
        });


        if (mviralListDetailsResponseModels.get(position).getIsLiked() == 0) {
            viralMainListViewHolder.heartIconImgView.setImageDrawable(context.getDrawable(R.drawable.heart));
        } else
            viralMainListViewHolder.heartIconImgView.setImageDrawable(context.getDrawable(R.drawable.sel_like_heart));
    }

    @Override
    public int getItemCount() {

        return mviralListDetailsResponseModels.size();

    }

    public void setOnClickListener(CommonClickListenerForViral commonClickListener) {

        mcommonClickListener = commonClickListener;
    }

    public void reloadList(List<ViralListDetailsResponseModel> listViral) {
        this.mviralListDetailsResponseModels = listViral;
        notifyDataSetChanged();
    }


    public void notifyList(List<ViralListDetailsResponseModel> listViral) {
        mviralListDetailsResponseModels.addAll(listViral);
        notifyDataSetChanged();
    }



    class ViralMainListViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;
        @BindView(R.id.titleImgView)
        ImageView titleImgView;
        @BindView(R.id.likeLayout)
        LinearLayout likeLayout;
        @BindView(R.id.downloadLayout)
        LinearLayout downloadLayout;
        @BindView(R.id.heartIconImgView)
        ImageView heartIconImgView;
        @BindView(R.id.shareLayout)
        LinearLayout shareLayout;
        @BindView(R.id.likesCountTxtView)
        TextView likesCountTxtView;


        ViralMainListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}