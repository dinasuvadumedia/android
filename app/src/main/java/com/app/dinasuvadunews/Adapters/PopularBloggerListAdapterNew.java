package com.app.dinasuvadunews.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerWithTwo;
import com.app.dinasuvadunews.Models.PopularBloggersListResponseModel;
import com.app.dinasuvadunews.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PopularBloggerListAdapterNew extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity context;
    private List<PopularBloggersListResponseModel> mpopularBloggersListResponseModels;
    private CommonClickListenerWithTwo mcommonClickListener;

    public PopularBloggerListAdapterNew(Activity context, List<PopularBloggersListResponseModel> popularBloggersListResponseModels) {
        this.context = context;
        mpopularBloggersListResponseModels = popularBloggersListResponseModels;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        return new PopularBloggersListViewHolder(LayoutInflater.from(context).inflate(R.layout.blogger_item_view_new, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final PopularBloggersListViewHolder popularBloggersListViewHolder = (PopularBloggersListViewHolder) viewHolder;

        popularBloggersListViewHolder.descriptionTxtView.setText(mpopularBloggersListResponseModels.get(position).getTitle());
        popularBloggersListViewHolder.titleTxtView.setText(mpopularBloggersListResponseModels.get(position).getBloggerName());

        new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).getBloggerImage(), popularBloggersListViewHolder.profileImgView);
        new GlideImageLoader(context).loadSingleImgUrl(context, mpopularBloggersListResponseModels.get(position).getImages(), popularBloggersListViewHolder.userCoverImg);


        popularBloggersListViewHolder.profileImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mcommonClickListener.onDownloadClick(position); // go to single blog details page
            }
        });


        popularBloggersListViewHolder.titleTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onDownloadClick(position); // go to single blog details page
            }
        });

        popularBloggersListViewHolder.userCoverImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onImgClick(position);
            }
        });


        popularBloggersListViewHolder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                mcommonClickListener.onShareClick(position);
            }
        });


        popularBloggersListViewHolder.whatsappShareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Whatsapp share
                mcommonClickListener.onShareClickWithImg(position, popularBloggersListViewHolder.userCoverImg);
            }
        });

        popularBloggersListViewHolder.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mcommonClickListener.onLikeClick(position);
            }
        });


        if (mpopularBloggersListResponseModels.get(position).getLikesCount() == 1) {
            popularBloggersListViewHolder.likesCountTxtView.setText("1" + " Like");
        } else {
            popularBloggersListViewHolder.likesCountTxtView.setText(mpopularBloggersListResponseModels.get(position).getLikesCount() + " Likes");
        }


        if (mpopularBloggersListResponseModels.get(position).getIsLiked() == 0) {
            popularBloggersListViewHolder.heartIconImgView.setImageDrawable(context.getDrawable(R.drawable.heart));
        } else
            popularBloggersListViewHolder.heartIconImgView.setImageDrawable(context.getDrawable(R.drawable.sel_like_heart));

        if (mpopularBloggersListResponseModels.get(position).getIsFollow() == 1) {
            popularBloggersListViewHolder.followTxtView.setText(context.getString(R.string.un_follow));
        } else {
            popularBloggersListViewHolder.followTxtView.setText(context.getString(R.string.follow));
        }

    }

    public void reloadList(List<PopularBloggersListResponseModel> mpopularBloggersListResponseModels) {
        this.mpopularBloggersListResponseModels = mpopularBloggersListResponseModels;
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {

        return mpopularBloggersListResponseModels.size();

    }


    public void setOnClickListener(CommonClickListenerWithTwo commonClickListener) {

        mcommonClickListener = commonClickListener;
    }


    class PopularBloggersListViewHolder extends RecyclerView.ViewHolder {

      /*  @BindView(R.id.typeOneImgView)
        ImageView typeOneImgView;

        @BindView(R.id.typeTwoImgView)
        ImageView typeTwoImgView;

        @BindView(R.id.typeThreeImgView)
        ImageView typeThreeImgView;


        @BindView(R.id.userNameTxtview)
        TextView userNameTxtview;
        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.userNameTxtview2)
        TextView userNameTxtview2;
        @BindView(R.id.descriptionTxtView2)
        TextView descriptionTxtView2;


        @BindView(R.id.userNameTxtview3)
        TextView userNameTxtview3;
        @BindView(R.id.descriptionTxtView3)
        TextView descriptionTxtView3;


        @BindView(R.id.profileImgView1)
        CircleImageView profileImgView1;

        @BindView(R.id.profileImgView2)
        CircleImageView profileImgView2;


        @BindView(R.id.profileImgView3)
        CircleImageView profileImgView3;*/


        @BindView(R.id.titleTxtView)
        TextView titleTxtView;
        @BindView(R.id.descriptionTxtView)
        TextView descriptionTxtView;

        @BindView(R.id.profileImgView)
        CircleImageView profileImgView;

        @BindView(R.id.followTxtView)
        TextView followTxtView;

        @BindView(R.id.userCoverImg)
        ImageView userCoverImg;

        @BindView(R.id.whatsappShareLayout)
        LinearLayout whatsappShareLayout;

        @BindView(R.id.shareLayout)
        LinearLayout shareLayout;

        @BindView(R.id.likeLayout)
        LinearLayout likeLayout;

        @BindView(R.id.likesCountTxtView)
        TextView likesCountTxtView;

        @BindView(R.id.heartIconImgView)
        ImageView heartIconImgView;

        PopularBloggersListViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}