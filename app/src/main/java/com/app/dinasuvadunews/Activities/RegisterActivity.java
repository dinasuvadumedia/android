package com.app.dinasuvadunews.Activities;


import android.content.Intent;
import android.os.Build;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.LoginResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends BaseWhiteActivity {
    private static final int RC_SIGN_IN = 10;

    @BindView(R.id.emailIdEdtTxt)
    EditText emailIdEdtTxt;

    @BindView(R.id.userNameEdtTxt)
    EditText userNameEdtTxt;
    @BindView(R.id.passwordEdtTxt)
    EditText passwordEdtTxt;
    @BindView(R.id.confirmPasswordEdtTxt)
    EditText confirmPasswordEdtTxt;

    @BindView(R.id.registerButton)
    Button registerButton;

    CommonViewModel newsFragmentViewModel;
    AppSettings appSettings;

    @BindView(R.id.goToLoginTxtView)
    TextView goToLoginTxtView;

    private GoogleSignInClient mGoogleSignInClient;
    private boolean isGoogleLoginClicked = false;
    private String userEmail = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        initViews();
        setClickListener();
    }

    private void setClickListener() {

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isGoogleLoginClicked = false;

                if (userNameEdtTxt.getText().toString().length() == 0) {
                    Utilities.showToast(RegisterActivity.this, getString(R.string.enter_username));
                } else if (!Utilities.isValidEmail(emailIdEdtTxt.getText().toString().trim())) {
                    Utilities.showToast(RegisterActivity.this, getString(R.string.enter_valid_email));
                } else if (passwordEdtTxt.getText().toString().trim().length() <= 5) {
                    Utilities.showToast(RegisterActivity.this, getString(R.string.enter_valid_pass));
                } else if (confirmPasswordEdtTxt.getText().toString().trim().length() <= 5) {
                    Utilities.showToast(RegisterActivity.this, getString(R.string.enter_valid_confirm_pass));
                } else if (!passwordEdtTxt.getText().toString().trim().equalsIgnoreCase(confirmPasswordEdtTxt.getText().toString().toString())) {
                    Utilities.showToast(RegisterActivity.this, getString(R.string.password_mismatch));
                } else {
                    registerNow(userNameEdtTxt.getText().toString().trim(), emailIdEdtTxt.getText().toString().trim(), passwordEdtTxt.getText().toString().trim());
                }
            }
        });

        goToLoginTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToLoginClass();

            }
        });


    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }


    private void goToLoginClass() {
        startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
        finish();
    }

    private void registerNow(String name, String email, String password) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.USER_NAME, name);
            jsonObject.put(Constants.API_PARAMS.EMAIL, email);
            jsonObject.put(Constants.API_PARAMS.PASSWORD, password);
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(RegisterActivity.this).getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(RegisterActivity.this);
        inputForAPI.setUrl(UrlHelpers.SIGNUP);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getRegisterResponse(inputForAPI).observe(this, new Observer<LoginResponseModel>() {
            @Override
            public void onChanged(@Nullable LoginResponseModel loginResponseModel) {

                if (loginResponseModel != null) {
                    if (loginResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        new AppSettings(RegisterActivity.this).setIsLoggedIn("true");
                        new AppSettings(RegisterActivity.this).setUserId(loginResponseModel.getUsers().getId());
                        new AppSettings(RegisterActivity.this).setUserName(loginResponseModel.getUsers().getUserName());
                        finish();
                    } else {

                        if (isGoogleLoginClicked) {

                            if (loginResponseModel.getErrorMessage().equalsIgnoreCase("Email Address already exists")) {
                                loginNow(userEmail, "123456");
                            } else
                                Utilities.showToast(RegisterActivity.this, loginResponseModel.getErrorMessage());
                        } else
                            Utilities.showToast(RegisterActivity.this, loginResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }


    private void loginNow(String email, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.EMAIL, email);
            jsonObject.put(Constants.API_PARAMS.PASSWORD, pass);
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(RegisterActivity.this).getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(RegisterActivity.this);
        inputForAPI.setUrl(UrlHelpers.LOGIN);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getLoginResponse(inputForAPI).observe(this, new Observer<LoginResponseModel>() {
            @Override
            public void onChanged(@Nullable LoginResponseModel loginResponseModel) {

                if (loginResponseModel != null) {
                    if (loginResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        new AppSettings(RegisterActivity.this).setIsLoggedIn("true");
                        new AppSettings(RegisterActivity.this).setUserId(loginResponseModel.getUsers().getId());
                        new AppSettings(RegisterActivity.this).setUserName(loginResponseModel.getUsers().getUserName());
                        finish();
                    } else {
                        Utilities.showToast(RegisterActivity.this, loginResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }


    private void initViews() {
        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        appSettings = new AppSettings(RegisterActivity.this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // SignInButton signInButton = findViewById(R.id.sign_in_button);
        //signInButton.setSize(SignInButton.SIZE_WIDE);

    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            if (account.getEmail().equalsIgnoreCase("")) {
                Utilities.showToast(RegisterActivity.this, getString(R.string.unable_to_fetch_email));
            } else {
                userEmail = account.getEmail();
                registerNow(account.getEmail(), account.getDisplayName(), "123456");
            }

        } catch (ApiException e) {
        }
    }

}
