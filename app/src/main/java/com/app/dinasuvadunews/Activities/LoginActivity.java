package com.app.dinasuvadunews.Activities;


import android.content.Intent;
import android.os.Build;


import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.LoginResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseWhiteActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final int RC_SIGN_IN = 10;
    @BindView(R.id.emailIdEdtTxt)
    EditText emailIdEdtTxt;
    @BindView(R.id.passwordEdtTxt)
    EditText passwordEdtTxt;

//    @BindView(R.id.loginButton)
//    Button googleLoginButton;

    CommonViewModel newsFragmentViewModel;
    AppSettings appSettings;

    @BindView(R.id.goToSignupTxtView)
    TextView goToSignupTxtView;
    private GoogleSignInClient mGoogleSignInClient;
    private String userEmail = "";
    private CallbackManager callbackManager;
    private static final String EMAIL = "email";

    @BindView(R.id.gmailLoginBtn)
    Button gmailLoginBtn;

    @BindView(R.id.fbLoginBtn)
    Button fbLoginBtn;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        initViews();
        setClickListener();
        faceBookListener();
    }

    private void faceBookListener() {

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());
                                        try {
                                            // Application code
                                            userEmail = response.getJSONObject().getString("email");
                                            String name = response.getJSONObject().getString("name");
                                            //  txtStatus.setText("Login Success \n" + email);

                                            registerNow(userEmail, name);

                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            ;
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                        // App code
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

    }

    private void setClickListener() {

/*
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (!Utilities.isValidEmail(emailIdEdtTxt.getText().toString().trim())) {
                    Utilities.showToast(LoginActivity.this, getString(R.string.enter_valid_email));
                } else if (passwordEdtTxt.getText().toString().trim().length() <= 5) {
                    Utilities.showToast(LoginActivity.this, getString(R.string.enter_valid_pass));
                } else {
                    loginNow(emailIdEdtTxt.getText().toString().trim(), passwordEdtTxt.getText().toString().trim());
                }
            }
        });
*/


        goToSignupTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToSignupClass();
            }
        });


        findViewById(R.id.gmailLoginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mGoogleSignInClient.signOut();
                signIn();
            }
        });


        findViewById(R.id.fbLoginBtn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList(EMAIL));
            }
        });

    }

    private void goToSignupClass() {

        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
        finish();
    }

    private void loginNow(String email, String pass) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.EMAIL, email);
            jsonObject.put(Constants.API_PARAMS.PASSWORD, "123456");
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(LoginActivity.this).getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(LoginActivity.this);
        inputForAPI.setUrl(UrlHelpers.LOGIN);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getLoginResponse(inputForAPI).observe(this, new Observer<LoginResponseModel>() {
            @Override
            public void onChanged(@Nullable LoginResponseModel loginResponseModel) {

                if (loginResponseModel != null) {
                    if (loginResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        new AppSettings(LoginActivity.this).setIsLoggedIn("true");
                        new AppSettings(LoginActivity.this).setUserId(loginResponseModel.getUsers().getId());
                        new AppSettings(LoginActivity.this).setUserName(loginResponseModel.getUsers().getUserName());
                        finish();
                    } else {
                        Utilities.showToast(LoginActivity.this, loginResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }


    private void registerNow(String email, String displayName) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.USER_NAME, displayName);
            jsonObject.put(Constants.API_PARAMS.EMAIL, email);
            jsonObject.put(Constants.API_PARAMS.PASSWORD, "123456");
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(LoginActivity.this).getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(LoginActivity.this);
        inputForAPI.setUrl(UrlHelpers.SIGNUP);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getRegisterResponse(inputForAPI).observe(this, new Observer<LoginResponseModel>() {
            @Override
            public void onChanged(@Nullable LoginResponseModel loginResponseModel) {

                if (loginResponseModel != null) {
                    if (loginResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        new AppSettings(LoginActivity.this).setIsLoggedIn("true");
                        new AppSettings(LoginActivity.this).setUserId(loginResponseModel.getUsers().getId());
                        new AppSettings(LoginActivity.this).setUserName(loginResponseModel.getUsers().getUserName());
                        finish();
                    } else {
                        if (loginResponseModel.getErrorMessage().equalsIgnoreCase("Email Address already exists")) {
                            loginNow(userEmail, "123456");
                        } else
                            Utilities.showToast(LoginActivity.this, loginResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }


    private void initViews() {
        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        appSettings = new AppSettings(LoginActivity.this);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);


        // Facebook
        callbackManager = CallbackManager.Factory.create();


    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }


    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);


            if (account.getEmail().equalsIgnoreCase("")) {
                Utilities.showToast(LoginActivity.this, getString(R.string.unable_to_fetch_email));
            } else {
                userEmail = account.getEmail();
                registerNow(account.getEmail(), account.getDisplayName());
            }

        } catch (ApiException e) {
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

