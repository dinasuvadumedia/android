package com.app.dinasuvadunews.Activities;

import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.app.dinasuvadunews.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashScreenOneActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        initViews();
        printHashKey(this);
    }

    private void initViews() {

        Intent intent = new Intent(SplashScreenOneActivity.this, SplashActivity.class);
        startActivity(intent);
        finish();

    }


    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.e("dsdadad", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (NoSuchAlgorithmException e) {
            Log.e("dsdsd", "printHashKey()", e);
        } catch (Exception e) {
            Log.e("dsdsa", "printHashKey()", e);
        }
    }
}
