package com.app.dinasuvadunews.Activities;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Adapters.BloggersPostListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.BuildConfig;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListenerWithTwo;
import com.app.dinasuvadunews.Models.BloggersDetailTwoListResponseModel;
import com.app.dinasuvadunews.Models.BloggersDetailsListResponseModel;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetBloggersListResponseModel;
import com.app.dinasuvadunews.Models.MyBloggersDetailsListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BloggersPostListActivity extends BaseActivity {


    @BindView(R.id.bloggersPostRecyclerView)
    RecyclerView bloggersPostRecyclerView;


    @BindView(R.id.bloggersNameTxtView)
    TextView bloggersNameTxtView;
    @BindView(R.id.followersCountTxtView)
    TextView followersCountTxtView;
    @BindView(R.id.followingCountTxtView)
    TextView followingCountTxtView;
    @BindView(R.id.postsCountTxtView)
    TextView postsCountTxtView;
    @BindView(R.id.genderBtn)
    Button genderBtn;
    @BindView(R.id.profileImgView)
    ImageView profileImgView;
    @BindView(R.id.followBtn)
    Button followBtn;
    @BindView(R.id.coverPictureImgView)
    ImageView coverPictureImgView;
    CommonViewModel commonViewModel;
    BloggersPostListAdapter bloggersPostListAdapter;
    private String bloggerId = "";
    AppSettings appSettings;
    private int isFollowValue = 0;
    private ImageView sharingImgView;
    private String shareTitle = "";
    @BindView(R.id.errorTxtView)
    TextView errorTxtView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bloggers_post_list);
        Utilities.transperentStatusBar(BloggersPostListActivity.this);

        ButterKnife.bind(this);


        initViews();
        setClickListener();
        getBloggersDetailsList();
    }

    private void setClickListener() {

        followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isFollowValue == 0) {
                    updateFollowStatus(bloggerId, 1);
                } else
                    updateFollowStatus(bloggerId, 0);

            }
        });
    }

    private void updateFollowStatus(String bloggerId, int followVal) {

        InputForAPI inputForAPI = new InputForAPI(BloggersPostListActivity.this);
        inputForAPI.setUrl(UrlHelpers.ADD_FOLLOWERS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.BLOGGER_ID, bloggerId);
            jsonObject.put(Constants.API_PARAMS.STATUS, followVal);
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(this).getUdid());

            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAddFollowStatus(inputForAPI).observe(this, new Observer<GetBloggersListResponseModel>() {
            @Override
            public void onChanged(@Nullable GetBloggersListResponseModel getBloggersListResponseModel) {

                if (getBloggersListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    getBloggersDetailsList();
                } else {
                    Utilities.showToast(BloggersPostListActivity.this, getBloggersListResponseModel.getErrorMessage());
                }
            }

        });


    }

    private void setBlogDetailsList(String name, String image, List<BloggersDetailTwoListResponseModel> bloggerDetails) {

        if (bloggerDetails.size() > 0) {
            errorTxtView.setVisibility(View.GONE);
            bloggersPostListAdapter = new BloggersPostListAdapter(BloggersPostListActivity.this, bloggerDetails, name, image);
            bloggersPostRecyclerView.setAdapter(bloggersPostListAdapter);


            bloggersPostListAdapter.setOnClickListener(new CommonClickListenerWithTwo() {
                @Override
                public void onImgClick(int position) {

                    startActivity(new Intent(BloggersPostListActivity.this, BloggerProfileActivity.class).putExtra(Constants.INTENT_VALUES.BLOGER_ID, bloggerDetails.get(position).getBlogId()));
                }

                @Override
                public void onLikeClick(int pos) {

                    int likeCounts = bloggerDetails.get(pos).getLikesCount();

                    int likeVal = 0;

                    if (bloggerDetails.get(pos).getIsLiked() == 0) {
                        likeVal = 1;
                        bloggerDetails.get(pos).setIsLiked(1);
                        likeCounts++;
                        bloggerDetails.get(pos).setLikesCount(likeCounts);
                        Utilities.playLikeSound(BloggersPostListActivity.this);
                    } else {
                        likeVal = 0;
                        likeCounts--;
                        bloggerDetails.get(pos).setLikesCount(likeCounts);
                        bloggerDetails.get(pos).setIsLiked(0);
                    }

                    bloggersPostListAdapter.reloadList(bloggerDetails);

                    sendLikeVal(bloggerDetails.get(pos).getBlogId(), likeVal);

                }

                @Override
                public void onDownloadClick(int pos) {

                    String myDynamicUri = Utilities.generateCustomUriForBlog(Constants.INTENT_VALUES.SHARE_BLOG_TYPE, bloggerId);
                    Utilities.generateDynamicLinkForExternalShare(BloggersPostListActivity.this, myDynamicUri, bloggerDetails.get(pos).getImages(), bloggerDetails.get(pos).getTitle());
                }

                @Override
                public void onShareClick(int pos) {


                }

                @Override
                public void onShareClickWithImg(int pos, ImageView imageView) {

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        sharingImgView = imageView;
                        shareTitle = bloggerDetails.get(pos).getTitle();
                        getPermissions();
                    } else {
                        sharingImgView = imageView;
                        Utilities.sendImageAsMsgToWhatsapp(BloggersPostListActivity.this, sharingImgView, bloggerDetails.get(pos).getTitle());
                    }

                }

            });
        } else {
            errorTxtView.setVisibility(View.VISIBLE);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void getPermissions() {
        requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
    }


    private void sendLikeVal(String blogId, int likeVal) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.BLOG_ID, blogId);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.IS_LIKED, likeVal);
        } catch (Exception e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(BloggersPostListActivity.this);
        inputForAPI.setUrl(UrlHelpers.BLOG_LIKE);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        commonViewModel.getLikeNewsResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    // Utilities.showToast(getActivity(), commonSuccessResponseModel.getErrorMessage());
                } else
                    Utilities.showToast(BloggersPostListActivity.this, commonSuccessResponseModel.getErrorMessage());
            }

        });

    }

    private void initViews() {
        bloggersPostRecyclerView.setFocusable(false);
        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        appSettings = new AppSettings(BloggersPostListActivity.this);

        if (getIntent() != null) {
            bloggerId = getIntent().getStringExtra(Constants.INTENT_VALUES.BLOGER_ID);
        }

        bloggersPostRecyclerView.addItemDecoration(new SpacesItemDecoration(60, 1));
    }

    private void getBloggersDetailsList() {
        InputForAPI inputForAPI = new InputForAPI(BloggersPostListActivity.this);
        inputForAPI.setUrl(UrlHelpers.GET_INDIVIDUAL_BLOG);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(BloggersPostListActivity.this).getUdid());
            jsonObject.put(Constants.API_PARAMS.BLOGGER_ID, bloggerId);

            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAllBloggerDetailsList(inputForAPI).observe(this, new Observer<MyBloggersDetailsListResponseModel>() {
            @Override
            public void onChanged(@Nullable MyBloggersDetailsListResponseModel myBloggersDetailsListResponseModel) {

                if (myBloggersDetailsListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    setBasicDetails(myBloggersDetailsListResponseModel.getBloggerDetails());
                    setBlogDetailsList(myBloggersDetailsListResponseModel.getBloggerDetails().getName(), myBloggersDetailsListResponseModel.getBloggerDetails().getImages(), myBloggersDetailsListResponseModel.getBloggerDetails().getBlogs());
                } else {
                    Utilities.showToast(BloggersPostListActivity.this, myBloggersDetailsListResponseModel.getErrorMessage());
                }
            }

        });

    }

    private void setBasicDetails(BloggersDetailsListResponseModel bloggerDetails) {

        bloggersNameTxtView.setText(bloggerDetails.getName());
        genderBtn.setText(bloggerDetails.getGender());

        new GlideImageLoader(BloggersPostListActivity.this).loadSingleImgUrl(this, bloggerDetails.getImages(), profileImgView);
        new GlideImageLoader(BloggersPostListActivity.this).loadSingleImgUrl(this, bloggerDetails.getCoverpicture(), coverPictureImgView);

        Log.e("ddsdl", "jfiwejfwkefmwkfmew:" + bloggerDetails.getCoverpicture());

        followersCountTxtView.setText("" + bloggerDetails.getFollowers());
        followingCountTxtView.setText("" + bloggerDetails.getFollowing());
        postsCountTxtView.setText("" + bloggerDetails.getBlogcount());

        if (bloggerDetails.getIsFollow() == 1) {
            isFollowValue = 1;
            followBtn.setBackground(getResources().getDrawable(R.drawable.follow_rect_shape));
        } else {
            isFollowValue = 0;
            followBtn.setBackground(getResources().getDrawable(R.drawable.grey_rect_shape));
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case 1:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    Utilities.sendImageAsMsgToWhatsapp(BloggersPostListActivity.this, sharingImgView, shareTitle);
                } else {
                    showAlertDialog();
                }
                break;
        }

    }


    private void showAlertDialog() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BloggersPostListActivity.this);
        alertDialogBuilder.setTitle(getResources().getString(R.string.requires_per));
        alertDialogBuilder.setMessage(getResources().getString(R.string.need_storage_per));
        alertDialogBuilder.setPositiveButton(getResources().getString(R.string.yes),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        arg0.cancel();
                        Intent i = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + BuildConfig.APPLICATION_ID));
                        startActivity(i);

                    }
                });

        alertDialogBuilder.setNegativeButton(getResources().getString(R.string.no),
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        arg0.cancel();

                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
