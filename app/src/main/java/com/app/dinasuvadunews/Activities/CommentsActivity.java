package com.app.dinasuvadunews.Activities;


import android.os.Build;


import android.os.Bundle;

import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Adapters.CommentsListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetCommentsDetailsListlResponseModel;
import com.app.dinasuvadunews.Models.GetCommentsListlResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentsActivity extends BaseWhiteActivity {


    @BindView(R.id.commentsReclerView)
    RecyclerView commentsRecylerView;
    @BindView(R.id.backIconImgView)
    ImageView backIconImgView;
    @BindView(R.id.sendCommentImgView)
    ImageView sendCommentImgView;
    @BindView(R.id.commentEdtTxt)
    EditText commentEdtTxt;
    @BindView(R.id.errorTxtView)
    TextView errorTxtView;
    CommentsListAdapter commentsListAdapter;
    CommonViewModel newsFragmentViewModel;
    AppSettings appSettings;
    List<GetCommentsDetailsListlResponseModel> commentsList;
    private String newsId = "";

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        ButterKnife.bind(this);

        initViews();

        setClickListener();
        getCommentsList();
    }

    private void setClickListener() {

        backIconImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        sendCommentImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (commentEdtTxt.getText().toString().trim().length() > 0) {
                    errorTxtView.setVisibility(View.GONE);

                    commentsList = new ArrayList<>();
                    GetCommentsDetailsListlResponseModel getCommentsDetailsListlResponseModel = new GetCommentsDetailsListlResponseModel();

                    getCommentsDetailsListlResponseModel.setComment(commentEdtTxt.getText().toString().trim());
                    getCommentsDetailsListlResponseModel.setUserName(appSettings.getUserName());
                    commentsList.add(getCommentsDetailsListlResponseModel);


                    if (commentsListAdapter == null) {
                        commentsListAdapter = new CommentsListAdapter(CommentsActivity.this, commentsList);
                        commentsRecylerView.setAdapter(commentsListAdapter);
                    } else {
                        commentsListAdapter.reloadData(commentsRecylerView, commentsList);
                    }

                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put(Constants.API_PARAMS.NEWS_ID, newsId);
                        jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
                        jsonObject.put(Constants.API_PARAMS.COMMENT, commentEdtTxt.getText().toString().trim());

                        sendMsgToApi(jsonObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    commentEdtTxt.setText("");

                    Utilities.playChatSound(CommentsActivity.this);

                }

            }
        });
    }

    private void sendMsgToApi(JSONObject jsonObject) {

        InputForAPI inputForAPI = new InputForAPI(CommentsActivity.this);
        inputForAPI.setUrl(UrlHelpers.NEWS_COMMENT);
        inputForAPI.setJsonObject(jsonObject);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.sendComment(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel != null) {

                    if (!commonSuccessResponseModel.getError().equalsIgnoreCase(getString(R.string.false_val))) {
                        Utilities.showToast(CommentsActivity.this, commonSuccessResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }

    private void getCommentsList() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.NEWS_ID, newsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        InputForAPI inputForAPI = new InputForAPI(CommentsActivity.this);
        inputForAPI.setUrl(UrlHelpers.LIST_COMMENTS);
        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getCommentsList(inputForAPI).observe(this, new Observer<GetCommentsListlResponseModel>() {
            @Override
            public void onChanged(@Nullable GetCommentsListlResponseModel getCommentsListlResponseModel) {

                if (getCommentsListlResponseModel != null) {

                    if (getCommentsListlResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                        setCommentsAdapter(getCommentsListlResponseModel.getListComments());

                    } else {
                        Utilities.showToast(CommentsActivity.this, getCommentsListlResponseModel.getErrorMessage());
                    }
                }
            }

        });

    }


    private void setCommentsAdapter(List<GetCommentsDetailsListlResponseModel> listComments) {

        if (listComments.size() > 0) {
            errorTxtView.setVisibility(View.GONE);
            commentsList = listComments;
            commentsListAdapter = new CommentsListAdapter(CommentsActivity.this, listComments);
            commentsRecylerView.setAdapter(commentsListAdapter);
        } else {
            errorTxtView.setVisibility(View.VISIBLE);
        }
    }


    private void initViews() {
        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        commentsRecylerView.setLayoutManager(linearLayoutManagerVertical);
        commentsRecylerView.addItemDecoration(new SpacesItemDecoration(40, 1));
        appSettings = new AppSettings(CommentsActivity.this);


        if (getIntent() != null) {

            newsId = getIntent().getStringExtra(Constants.INTENT_VALUES.NEWS_ID);

        }

    }
}
