package com.app.dinasuvadunews.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;

import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

public class SplashActivity extends AppCompatActivity {

    private boolean comingFromDeepLink = false;
    private String newsId = "";
    private String categoryId = "";
    ImageView splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);


        splash = findViewById(R.id.splash);


        DrawableImageViewTarget imageViewTarget = new DrawableImageViewTarget(splash);
        Glide.with(this).load(R.raw.splash).into(imageViewTarget);


        getDataFromDeepLink();
        getUDID();
        goToNextScreen();


    }

    private void getDataFromDeepLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        if (pendingDynamicLinkData != null) {

                            comingFromDeepLink = true;

                            Uri deepLink = pendingDynamicLinkData.getLink();
                            String module = deepLink.getLastPathSegment();


                            Log.e("wdwdk", "onSuccess:" + module);

                            if (module.equalsIgnoreCase(Constants.INTENT_VALUES.SHARE_NEWS_TYPE)) {
                                newsId = Utilities.replacebracket(deepLink.getQueryParameters("newsId").toString());
                                categoryId = Utilities.replacebracket(deepLink.getQueryParameters("categoryId").toString());
                                goToNewsDetailScreen();
                            } else if (module.equalsIgnoreCase(Constants.INTENT_VALUES.SHARE_VIDEO_TYPE)) {

                                new AppSettings(SplashActivity.this).setOpenVideoTab("true");
                                new AppSettings(SplashActivity.this).setOpenViralTab("false");
                            } else if (module.equalsIgnoreCase(Constants.INTENT_VALUES.SHARE_BLOG_TYPE)) {
                                String blogId = Utilities.replacebracket(deepLink.getQueryParameters("blogId").toString());
                                goToBlogScreen(blogId);
                            }
                        }
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //txtResult.append("\nonFailure");
                    }
                });
    }

    private void goToBlogScreen(String blogId) {

        Intent intent = new Intent(this, BloggerProfileActivity.class);
        intent.putExtra(Constants.INTENT_VALUES.BLOGER_ID, blogId);
        startActivity(intent);
        finish();
    }

    private void goToNewsDetailScreen() {

        Intent intent = new Intent(this, DetailNewsActivity.class);
        intent.putExtra(Constants.INTENT_VALUES.CATEGORY_ID, categoryId);
        intent.putExtra(Constants.INTENT_VALUES.NEWS_ID, newsId);
        intent.putExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, true);
        startActivity(intent);
        finish();

    }


    private void getUDID() {

        String androidId = Settings.Secure.getString(SplashActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
        new AppSettings(SplashActivity.this).setUdid(androidId);

        Utilities.log("Udid===>Is", new AppSettings(this).getUdid());

    }

    private void goToNextScreen() {


        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (!comingFromDeepLink) {
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                    finish();
                }
            }
        }, 2000);


    }
}
