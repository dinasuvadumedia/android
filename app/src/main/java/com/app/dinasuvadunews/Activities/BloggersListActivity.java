package com.app.dinasuvadunews.Activities;

import android.content.Intent;
import android.os.Build;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Adapters.SeeAllBloggersListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;

import com.app.dinasuvadunews.Fragments.BloggersNewFragment;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.GetBloggersListResponseModel;
import com.app.dinasuvadunews.Models.OurBloggersListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BloggersListActivity extends BaseWhiteActivity {

    @BindView(R.id.seeAllBloggersRecyclerView)
    RecyclerView seeAllBloggersRecyclerView;
    @BindView(R.id.backImgView)
    ImageView backImgView;
    SeeAllBloggersListAdapter seeAllBloggersListAdapter;
    private List<OurBloggersListResponseModel> bloggersList;


    CommonViewModel commonViewModel;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bloggers_listt);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        ButterKnife.bind(this);

        initViews();
        setClickListener();
        setAllBloggersList();
    }


    private void setClickListener() {

        backImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();

            }
        });
    }

    private void setAllBloggersList() {
        seeAllBloggersListAdapter = new SeeAllBloggersListAdapter(BloggersListActivity.this, bloggersList);
        seeAllBloggersRecyclerView.setAdapter(seeAllBloggersListAdapter);
        seeAllBloggersRecyclerView.addItemDecoration(new SpacesItemDecoration(40, 1));


        seeAllBloggersListAdapter.setClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                if (bloggersList.get(position).getIsFollow() == 0) {
                    updateFollowStatus(position, bloggersList.get(position).getBloggerId(), 1);
                } else
                    updateFollowStatus(position, bloggersList.get(position).getBloggerId(), 0);
            }
        });

    }

    private void updateFollowStatus(int pos, String bloggerId, Integer followVal) {

        InputForAPI inputForAPI = new InputForAPI(BloggersListActivity.this);
        inputForAPI.setUrl(UrlHelpers.ADD_FOLLOWERS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.BLOGGER_ID, bloggerId);
            jsonObject.put(Constants.API_PARAMS.STATUS, followVal);
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(this).getUdid());

            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getAddFollowStatus(inputForAPI).observe(this, new Observer<GetBloggersListResponseModel>() {
            @Override
            public void onChanged(@Nullable GetBloggersListResponseModel getBloggersListResponseModel) {

                if (getBloggersListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    reloadTheList(pos, followVal);
                } else {
                    Utilities.showToast(BloggersListActivity.this, getBloggersListResponseModel.getErrorMessage());
                }
            }

        });

    }

    private void reloadTheList(int pos, Integer followVal) {

        if (followVal == 0) {
            bloggersList.get(pos).setFollowers(bloggersList.get(pos).getFollowers() - 1);
        } else
            bloggersList.get(pos).setFollowers(bloggersList.get(pos).getFollowers() + 1);

        bloggersList.get(pos).setIsFollow(followVal);
        seeAllBloggersListAdapter.reloadTheVal(bloggersList);

    }

    private void initViews() {

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        Intent intent = getIntent();

//        if (intent != null)
//            bloggersList = (List<OurBloggersListResponseModel>) intent.getSerializableExtra("bloggersListData");
        bloggersList = BloggersNewFragment.bloggersList;

    }
}