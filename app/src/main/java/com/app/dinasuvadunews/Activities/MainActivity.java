package com.app.dinasuvadunews.Activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;


import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.drawerlayout.widget.DrawerLayout;

import com.app.dinasuvadunews.Adapters.TrendingVideosListAdapter;
import com.app.dinasuvadunews.Adapters.VideosMainListAdapter;

import com.app.dinasuvadunews.Fragments.NewsFragment;
import com.app.dinasuvadunews.Fragments.NotificationsFragment;
import com.app.dinasuvadunews.Fragments.VideosFragment;
import com.app.dinasuvadunews.Fragments.ViralFragment;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.CustomViews.NonSwipeableViewPager;
import com.app.dinasuvadunews.Helpers.CustomViews.ViewPagerAdapter;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity {
    //    @BindView(R.id.headerLayout)
//    LinearLayout headerLayout;
    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigationMenu;
    @BindView(R.id.viewPager)
    NonSwipeableViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    //    @BindView(R.id.titleTxtView)
//    TextView titleTxtView;
//    @BindView(R.id.navigationIcon)
//    ImageView navigationIcon;
//    @BindView(R.id.searchIcon)
//    ImageView searchIcon;
//    @BindView(R.id.bellIcon)
//    ImageView bellIcon;

    public static DrawerLayout drawerLayout;
    //    @BindView(R.id.notificationLayout)
//    RelativeLayout notificationLayout;
    @BindView(R.id.newsNaviLayout)
    LinearLayout newsNaviLayout;
    @BindView(R.id.videosNaviLayout)
    LinearLayout videosNaviLayout;
    @BindView(R.id.viralNaviLayout)
    LinearLayout viralNaviLayout;
    @BindView(R.id.notificationsNaviLayout)
    LinearLayout notificationsNaviLayout;
    @BindView(R.id.navigationCloseIcon)
    ImageView navigationCloseIcon;


    @BindView(R.id.newsNaviIcon)
    ImageView newsNaviIcon;
    @BindView(R.id.newsNaviTxt)
    TextView newsNaviTxt;

    @BindView(R.id.videoIcon)
    ImageView videoIcon;
    @BindView(R.id.videoTxt)
    TextView videoTxt;

    @BindView(R.id.viralIcon)
    ImageView viralIcon;
    @BindView(R.id.viralTxt)
    TextView viralTxt;

    @BindView(R.id.notificationImg)
    ImageView notificationImg;
    @BindView(R.id.notificationTxt)
    TextView notificationTxt;

    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.userNameTxtview)
    TextView userNameTxtview;
    private GoogleSignInClient mGoogleSignInClient;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        initViews();
        viewPagerListener();
        setViewPager();
        setCustomBottomNavigation();
        setBottomNavigationViews();
        clickListeners();
    }

    private void clickListeners() {

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (loginButton.getText().toString().trim().equalsIgnoreCase(getString(R.string.logout))) {
                    mGoogleSignInClient.signOut();
                    new AppSettings(MainActivity.this).setIsLoggedIn("false");
                    new AppSettings(MainActivity.this).setUserId("");
                    new AppSettings(MainActivity.this).setUserName("");

                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);

                } else {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                    closeDrawer();
                }

            }

        });

        navigationCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeDrawer();
            }
        });

        newsNaviLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSelectedNews();
            }
        });

        videosNaviLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showSelectedVideos();

            }
        });

        viralNaviLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showSelectedViral();
            }
        });

        notificationsNaviLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                showSelectedNotifi();
            }
        });

/*
        navigationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                drawerLayout.openDrawer(GravityCompat.START);

            }
        });


        notificationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  bottomNavigationMenu.setSelectedItemId(R.id.blog_item);

                startActivity(new Intent(MainActivity.this, NotificationActivity.class));

            }
        });
*/

    }

    private void closeDrawer() {

        drawerLayout.closeDrawer(Gravity.LEFT);
    }


    private void showSelectedNotifi() {

        newsNaviIcon.setImageDrawable(getDrawable(R.drawable.news_us));
        videoIcon.setImageDrawable(getDrawable(R.drawable.videos_us));
        viralIcon.setImageDrawable(getDrawable(R.drawable.viral_us));
        notificationImg.setImageDrawable(getDrawable(R.drawable.tab_noti));

        newsNaviTxt.setTextColor(getResources().getColor(R.color.grey));
        videoTxt.setTextColor(getResources().getColor(R.color.grey));
        viralTxt.setTextColor(getResources().getColor(R.color.grey));
        notificationTxt.setTextColor(getResources().getColor(R.color.blue));

        bottomNavigationMenu.setSelectedItemId(R.id.blog_item);


        closeDrawer();
    }


    private void showSelectedViral() {
        newsNaviIcon.setImageDrawable(getDrawable(R.drawable.news_us));
        videoIcon.setImageDrawable(getDrawable(R.drawable.videos_us));
        viralIcon.setImageDrawable(getDrawable(R.drawable.viral));
        notificationImg.setImageDrawable(getDrawable(R.drawable.tab_noti_us));

        newsNaviTxt.setTextColor(getResources().getColor(R.color.grey));
        videoTxt.setTextColor(getResources().getColor(R.color.grey));
        viralTxt.setTextColor(getResources().getColor(R.color.blue));
        notificationTxt.setTextColor(getResources().getColor(R.color.grey));

        bottomNavigationMenu.setSelectedItemId(R.id.viral_item);

        closeDrawer();
    }

    private void showSelectedVideos() {

        newsNaviIcon.setImageDrawable(getDrawable(R.drawable.news_us));
        videoIcon.setImageDrawable(getDrawable(R.drawable.video));
        viralIcon.setImageDrawable(getDrawable(R.drawable.viral_us));
        notificationImg.setImageDrawable(getDrawable(R.drawable.tab_noti_us));

        newsNaviTxt.setTextColor(getResources().getColor(R.color.grey));
        videoTxt.setTextColor(getResources().getColor(R.color.blue));
        viralTxt.setTextColor(getResources().getColor(R.color.grey));
        notificationTxt.setTextColor(getResources().getColor(R.color.grey));

        bottomNavigationMenu.setSelectedItemId(R.id.videos_item);

        closeDrawer();
    }

    private void showSelectedNews() {

        newsNaviIcon.setImageDrawable(getDrawable(R.drawable.news_icon));
        videoIcon.setImageDrawable(getDrawable(R.drawable.videos_us));
        viralIcon.setImageDrawable(getDrawable(R.drawable.viral_us));
        notificationImg.setImageDrawable(getDrawable(R.drawable.tab_noti_us));

        newsNaviTxt.setTextColor(getResources().getColor(R.color.blue));
        videoTxt.setTextColor(getResources().getColor(R.color.grey));
        viralTxt.setTextColor(getResources().getColor(R.color.grey));
        notificationTxt.setTextColor(getResources().getColor(R.color.grey));


        bottomNavigationMenu.setSelectedItemId(R.id.newss_item);

        closeDrawer();
    }

    private void initViews() {
        bottomNavigationMenu.setItemIconTintList(null);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(4);
        drawerLayout = findViewById(R.id.drawerLayout);


        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    private void setBottomNavigationViews() {
        bottomNavigationMenu.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.newss_item:
                        pauseVideoIfPlaying();
                        viewPager.setCurrentItem(0);
                        return true;
                    case R.id.videos_item:
                        pauseVideoIfPlaying();
                        viewPager.setCurrentItem(1);
                        return true;
                    case R.id.viral_item:
                        pauseVideoIfPlaying();
                        viewPager.setCurrentItem(2);
                        return true;
                    case R.id.blog_item:
                        pauseVideoIfPlaying();
                        viewPager.setCurrentItem(3);
                        return true;
                }
                return false;
            }
        });
    }

    private void pauseVideoIfPlaying() {
        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.playerPause();


        if (TrendingVideosListAdapter.mExoPlayerHelper != null)
            TrendingVideosListAdapter.mExoPlayerHelper.playerPause();
    }

    private void setViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragment(new NewsFragment(), "");
        viewPagerAdapter.addFragment(new VideosFragment(), "");
        viewPagerAdapter.addFragment(new ViralFragment(), "");
        viewPagerAdapter.addFragment(new NotificationsFragment(), "");
        viewPager.setAdapter(viewPagerAdapter);
    }


    private void viewPagerListener() {

        viewPager.setOnPageChangeListener(new NonSwipeableViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onPageSelected(int i) {

                if (i == 0) {
                    Utilities.setSystemBarTheme(MainActivity.this, true);
                    Utilities.setStatusBarColor(MainActivity.this, getResources().getColor(R.color.colorPrimary));
                   /* Utilities.setViewsColor(headerLayout, getResources().getColor(R.color.colorPrimary));
                    Utilities.setTextColor(titleTxtView, getResources().getColor(R.color.white));
                    Utilities.setImgTintColor(navigationIcon, R.color.white);
                    Utilities.setImgTintColor(bellIcon, R.color.white);
                    Utilities.hideView(searchIcon);
                    Utilities.showView(titleTxtView);*/
                } else if (i == 1) {
                    Utilities.setSystemBarTheme(MainActivity.this, false);
                    Utilities.setStatusBarColor(MainActivity.this, Color.WHITE);
                  /*  Utilities.setViewsColor(headerLayout, getResources().getColor(R.color.black));
                    Utilities.setTextColor(titleTxtView, getResources().getColor(R.color.white));
                    Utilities.setImgTintColor(navigationIcon, R.color.white);
                    Utilities.setImgTintColor(bellIcon, R.color.white);
                    Utilities.hideView(searchIcon);
                    Utilities.hideView(titleTxtView);*/

                } else if (i == 2) {
                    Utilities.setSystemBarTheme(MainActivity.this, false);
                    Utilities.setStatusBarColor(MainActivity.this, Color.WHITE);
                   /* Utilities.setViewsColor(headerLayout, getResources().getColor(R.color.white));
                    Utilities.setTextColor(titleTxtView, getResources().getColor(R.color.news_content));
                    Utilities.setImgTintColor(navigationIcon, R.color.news_content);
                    Utilities.setImgTintColor(bellIcon, R.color.grey_bell);
                    Utilities.hideView(searchIcon);
                    Utilities.hideView(titleTxtView);*/
                } else if (i == 3) {
                    Utilities.setSystemBarTheme(MainActivity.this, false);
                    Utilities.setStatusBarColor(MainActivity.this, Color.WHITE);
                  /*  Utilities.setViewsColor(headerLayout, getResources().getColor(R.color.white));
                    Utilities.setTextColor(titleTxtView, getResources().getColor(R.color.news_content));
                    Utilities.setImgTintColor(navigationIcon, R.color.news_content);
                    Utilities.setImgTintColor(bellIcon, R.color.grey_bell);
                    Utilities.hideView(searchIcon);
                    Utilities.hideView(titleTxtView);*/

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


    }

    private void setCustomBottomNavigation() {

    }


    @Override
    public void onStart() {
        super.onStart();

    }


    @Override
    public void onStop() {
        super.onStop();

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityStop();


        if (TrendingVideosListAdapter.mExoPlayerHelper != null)
            TrendingVideosListAdapter.mExoPlayerHelper.onActivityStop();

    }

    @Override
    public void onPause() {
        super.onPause();
        /*if (player != null) {
            player.onPause();
        }*/

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityPause();

        if (TrendingVideosListAdapter.mExoPlayerHelper != null)
            TrendingVideosListAdapter.mExoPlayerHelper.onActivityPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        /*if (player != null) {
            player.onResume();
        }*/
/*
        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityResume();
*/

        if (new AppSettings(MainActivity.this).getIsLoggedIn().equalsIgnoreCase(getResources().getString(R.string.true_val))) {
            userNameTxtview.setText(new AppSettings(MainActivity.this).getUserName());
            loginButton.setText(getString(R.string.logout));
            loginButton.setVisibility(View.VISIBLE);
        } else {
            loginButton.setText(getString(R.string.login));
            userNameTxtview.setText("");
            loginButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /*if (player != null) {
            player.onDestroy();
        }*/

        if (VideosMainListAdapter.mExoPlayerHelper != null)
            VideosMainListAdapter.mExoPlayerHelper.onActivityDestroy();


        if (TrendingVideosListAdapter.mExoPlayerHelper != null)
            TrendingVideosListAdapter.mExoPlayerHelper.onActivityDestroy();

    }
}
