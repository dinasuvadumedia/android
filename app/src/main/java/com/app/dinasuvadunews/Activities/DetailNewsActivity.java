package com.app.dinasuvadunews.Activities;


import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;


import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;

import com.app.dinasuvadunews.Adapters.CustomViewpagerAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Fragments.NewsSwipeListFragment;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsDetailResponseModel;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.relex.circleindicator.CircleIndicator;

public class DetailNewsActivity extends BaseActivity {
    @BindView(R.id.swipeNewsListViewPager)
    ViewPager swipeNewsListViewPager;
    @BindView(R.id.loadingLayout)
    View loadingLayout;
    @BindView(R.id.circleIndicator)
    CircleIndicator circleIndicator;
    @BindView(R.id.backIconImgView)
    ImageView backIconImgView;
    @BindView(R.id.moreImgView)
    ImageView moreImgView;
    @BindView(R.id.headerLayout)
    RelativeLayout headerLayout;

    CustomViewpagerAdapter customViewpagerAdapter;
    AppSettings appSettings;
    CommonViewModel newsFragmentViewModel;
    private String newsId = "";
    private String categoryId = "";
    private boolean comingFromPush = false;
    private List<GetAllRelatedNewsDetailResponseModel> listNews;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_news);
        //  getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        ButterKnife.bind(this);

        initViews();
        getAllNewsList();
        setClickListener();
    }

    private void getAllNewsList() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.CATEGORY_ID, categoryId);
            jsonObject.put(Constants.API_PARAMS.UDID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.NEWS_ID, newsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(DetailNewsActivity.this);
        inputForAPI.setUrl(UrlHelpers.GET_NEWS);
        HashMap<String, String> header = new HashMap<>();
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getAllRelatedNews(inputForAPI).observe(this, new Observer<GetAllRelatedNewsResponseModel>() {
            @Override
            public void onChanged(@Nullable GetAllRelatedNewsResponseModel getAllRelatedNewsResponseModel) {

                if (getAllRelatedNewsResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {
                    listNews = getAllRelatedNewsResponseModel.getListNews();
                    setViewPager(getAllRelatedNewsResponseModel.getListNews());
                } else
                    Utilities.showToast(DetailNewsActivity.this, getAllRelatedNewsResponseModel.getErrorMessage());
            }

        });


    }

    private void initViews() {

        customViewpagerAdapter = new CustomViewpagerAdapter(getSupportFragmentManager());
        appSettings = new AppSettings(DetailNewsActivity.this);
        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);


        if (getIntent() != null) {

            comingFromPush = getIntent().getBooleanExtra(Constants.INTENT_VALUES.COMING_FROM_PUSH, false);
            newsId = getIntent().getStringExtra(Constants.INTENT_VALUES.NEWS_ID);
            categoryId = getIntent().getStringExtra(Constants.INTENT_VALUES.CATEGORY_ID);
        }
    }


    private void setViewPager(List<GetAllRelatedNewsDetailResponseModel> listNews) {

        for (int i = 0; i < listNews.size(); i++) {
            customViewpagerAdapter.addFragment(NewsSwipeListFragment.newInstance(i, listNews, listNews.get(i).getSuggestedNews(), comingFromPush));

        }

        swipeNewsListViewPager.setAdapter(customViewpagerAdapter);
        loadingLayout.setVisibility(View.GONE);
        headerLayout.setVisibility(View.VISIBLE);
        circleIndicator.setViewPager(swipeNewsListViewPager);

    }


    private void setClickListener() {

        backIconImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (comingFromPush) {
                    startActivity(new Intent(DetailNewsActivity.this, MainActivity.class));
                    finish();
                } else {
                    finish();
                }
            }
        });


        moreImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Dialog dialog = Utilities.showShareExteralDialog(DetailNewsActivity.this);
                LinearLayout shareLayout = dialog.findViewById(R.id.shareLayout);

                shareLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        // Utilities.openExternalShare(getActivity(), listNewsResponse.get(position).getUrlToImage());

                        String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, listNews.get(swipeNewsListViewPager.getCurrentItem()).getUrlToImage(), listNews.get(swipeNewsListViewPager.getCurrentItem()).getId(), listNews.get(swipeNewsListViewPager.getCurrentItem()).getCategoryId());
                        Utilities.generateDynamicLinkForExternalShare(DetailNewsActivity.this, myDynamicUri, listNews.get(swipeNewsListViewPager.getCurrentItem()).getUrlToImage(), listNews.get(swipeNewsListViewPager.getCurrentItem()).getTitle());
                    }
                });

                dialog.show();
            }
        });
    }


    @Override
    protected void onSaveInstanceState(Bundle oldInstanceState) {
        super.onSaveInstanceState(oldInstanceState);
        oldInstanceState.clear();
    }


    @Override
    public void onBackPressed() {
        if (comingFromPush) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            super.onBackPressed();
        }

    }
}
