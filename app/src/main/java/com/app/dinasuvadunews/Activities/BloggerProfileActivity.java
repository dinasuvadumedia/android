package com.app.dinasuvadunews.Activities;


import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.CustomViews.GlideImageLoader;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.SingleBlogDetailsListResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class BloggerProfileActivity extends BaseActivity {

    private String blogId = "";
    CommonViewModel commonViewModel;
    @BindView(R.id.diagonal)
    ImageView diagonal;
    @BindView(R.id.backImgView)
    ImageView backImgView;
    @BindView(R.id.shareLayout)
    ImageView shareLayout;
    @BindView(R.id.descriptionTxtView)
    TextView descriptionTxtView;
    @BindView(R.id.profileImgView)
    CircleImageView profileImgView;
    @BindView(R.id.userNameTxtview)
    TextView userNameTxtview;
    @BindView(R.id.titleTxtView)
    TextView titleTxtView;
    private String blogImage = "";
    private String blogTitle = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogger_profile);
        Utilities.transperentStatusBar(BloggerProfileActivity.this);


        ButterKnife.bind(this);

        initViews();
        setClickListener();
        getBloggersFullDetailsList();
    }

    private void setClickListener() {

        shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String myDynamicUri = Utilities.generateCustomUriForBlog(Constants.INTENT_VALUES.SHARE_BLOG_TYPE, blogId);
                Utilities.generateDynamicLinkForExternalShare(BloggerProfileActivity.this, myDynamicUri, blogImage, blogTitle);

            }
        });


        backImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }

    private void initViews() {

        if (getIntent() != null)
            blogId = getIntent().getStringExtra(Constants.INTENT_VALUES.BLOGER_ID);

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
    }


    private void getBloggersFullDetailsList() {
        InputForAPI inputForAPI = new InputForAPI(BloggerProfileActivity.this);
        inputForAPI.setUrl(UrlHelpers.BLOG_DETAILS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.UDID, new AppSettings(BloggerProfileActivity.this).getUdid());
            jsonObject.put(Constants.API_PARAMS.BLOG_ID, blogId);

            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getBloggerFullDetailsList(inputForAPI).observe(this, new Observer<SingleBlogDetailsListResponseModel>() {
            @Override
            public void onChanged(@Nullable SingleBlogDetailsListResponseModel singleBlogDetailsListResponseModel) {

                if (singleBlogDetailsListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    setBlogDetails(singleBlogDetailsListResponseModel);

                } else {
                    Utilities.showToast(BloggerProfileActivity.this, singleBlogDetailsListResponseModel.getErrorMessage());
                }
            }

        });

    }

    private void setBlogDetails(SingleBlogDetailsListResponseModel singleBlogDetailsListResponseModel) {

        userNameTxtview.setText("By " + singleBlogDetailsListResponseModel.getBlogerInfoDetails().getBloggername());
        new GlideImageLoader(BloggerProfileActivity.this).loadSingleImgUrl(this, singleBlogDetailsListResponseModel.getBlogerInfoDetails().getBloggerImage(), profileImgView);

        blogImage = singleBlogDetailsListResponseModel.getBlogDetails().getImages();
        blogTitle = singleBlogDetailsListResponseModel.getBlogDetails().getTitle();

        titleTxtView.setText(singleBlogDetailsListResponseModel.getBlogDetails().getTitle());
        descriptionTxtView.setText(singleBlogDetailsListResponseModel.getBlogDetails().getDescriptions());

        new GlideImageLoader(BloggerProfileActivity.this).loadSingleImgUrl(this, singleBlogDetailsListResponseModel.getBlogDetails().getImages(), diagonal);

    }

}
