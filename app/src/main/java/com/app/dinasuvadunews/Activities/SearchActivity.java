package com.app.dinasuvadunews.Activities;

import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.app.dinasuvadunews.Adapters.SearchListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Models.AllNewsDetailsResponseModel;
import com.app.dinasuvadunews.Models.AllNewsResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchActivity extends BaseActivity {

    @BindView(R.id.backIconImgView)
    ImageView backIconImgView;
    @BindView(R.id.searchEdtTxt)
    EditText searchEdtTxt;
    CommonViewModel commonViewModel;
    @BindView(R.id.searchRecyclerView)
    RecyclerView searchRecyclerView;
    @BindView(R.id.searchImgView)
    ImageView searchImgView;

    private SearchListAdapter searchListAdapter;
    private LinearLayoutManager linearLayoutManagerVertical;
    private int totalPages = 0;
    private int currentPageNum = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        ButterKnife.bind(this);

        initViews();
        setListeners();
        editListener();
        setRecyclerScrollListener();
    }


    private void setRecyclerScrollListener() {

        searchRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                    if (searchListAdapter != null) {

                        if (totalPages > 1) {
                            if (searchListAdapter.getItemCount() > 0 && linearLayoutManagerVertical.findLastVisibleItemPosition() >= searchListAdapter.getItemCount() - 1) {
                                try {
                                    if (totalPages == currentPageNum) {
                                    } else {
                                        currentPageNum = currentPageNum + 1;
                                        getMySearchList(searchEdtTxt.getText().toString().trim());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                }
            }
        });

    }


    private void editListener() {

        searchEdtTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (searchEdtTxt.getText().toString().trim().length() > 0) {
                    getMySearchList(searchEdtTxt.getText().toString().trim());
                } else {
                    searchRecyclerView.setVisibility(View.GONE);
                }
            }
        });
    }

    private void setListeners() {

        searchImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });

        backIconImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });


    }


    private void getMySearchList(String text) {
        InputForAPI inputForAPI = new InputForAPI(
                SearchActivity.this);
        inputForAPI.setUrl(UrlHelpers.SEARCH_NEWS);

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(Constants.API_PARAMS.SEARCH_TEXT, text);
            jsonObject.put(Constants.API_PARAMS.PAGE_NUMBER, currentPageNum);

            inputForAPI.setJsonObject(jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }


        HashMap<String, String> header = new HashMap<>();
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_NAME);
        header.put(Constants.HEADERS.Content_Type, Constants.HEADERS.CONTENT_JSON_NAME);
        inputForAPI.setHeaders(header);

        commonViewModel.getMySearchList(inputForAPI).observe(this, new Observer<AllNewsResponseModel>() {
            @Override
            public void onChanged(@Nullable AllNewsResponseModel mySearchListResponseModel) {

                if (mySearchListResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    setSearchListAdapter(mySearchListResponseModel.getNewsfeed(), mySearchListResponseModel);

                } else {
                    Utilities.showToast(SearchActivity.this, mySearchListResponseModel.getErrorMessage());
                }
            }


        });

    }

    private void setSearchListAdapter(List<AllNewsDetailsResponseModel> mySearchListResponseModel, AllNewsResponseModel searchListResponseModel) {

        if (mySearchListResponseModel.size() > 0) {
            totalPages = searchListResponseModel.getTotalPage();

            searchRecyclerView.setVisibility(View.VISIBLE);

            if (currentPageNum == 1) {
                searchListAdapter = new SearchListAdapter(SearchActivity.this, mySearchListResponseModel);
                searchRecyclerView.setAdapter(searchListAdapter);
            } else {

                searchListAdapter.reloadList(mySearchListResponseModel);
            }
            searchListAdapter.setOnClickListener(new com.app.dinasuvadunews.Interfaces.CommonClickListenerForNewsList() {
                @Override
                public void clickListenerForNewsList(int position, String imgUrl, String title, String newsId, String catgId) {


                    Dialog dialog = Utilities.showShareExteralDialog(SearchActivity.this);
                    LinearLayout shareLayout = dialog.findViewById(R.id.shareLayout);

                    shareLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                            //   Utilities.openExternalShare(getActivity(), value);

                            String myDynamicUri = Utilities.generateCustomUri(Constants.INTENT_VALUES.SHARE_NEWS_TYPE, imgUrl, newsId, catgId);
                            Utilities.generateDynamicLinkForExternalShare(SearchActivity.this, myDynamicUri, imgUrl, title);
                        }
                    });

                    dialog.show();

                }
            });
        } else {

            searchRecyclerView.setVisibility(View.GONE);
        }

    }


    private void initViews() {
        linearLayoutManagerVertical = new LinearLayoutManager(this);
        searchRecyclerView.setLayoutManager(linearLayoutManagerVertical);

        commonViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
    }
}
