package com.app.dinasuvadunews.Activities;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.R;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;

import net.alexandroid.utils.exoplayerhelper.ExoPlayerHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoPlayerActivity extends BaseActivity {

    private static final String TAG = VideoPlayerActivity.class.getSimpleName();
    @BindView(R.id.backImgView)
    ImageView backImgView;
    private ExoPlayerHelper mExoPlayerHelper;
    private String url = "";

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        ButterKnife.bind(this);


        initViews();
        setClickListeners();
        playVideo(savedInstanceState);

    }

    private void playVideo(Bundle savedInstanceState) {

        SimpleExoPlayerView exoPlayerView = findViewById(R.id.exoPlayerView);
        mExoPlayerHelper = new ExoPlayerHelper.Builder(this, exoPlayerView)
                .addMuteButton(false, false)
                .setUiControllersVisibility(true)
                .setAutoPlayOn(true).setRepeatModeOn(false)
                .setRepeatModeOn(false)
                .setAutoPlayOn(false)
                .setVideoUrls(url)
                .addSavedInstanceState(savedInstanceState)
                .enableLiveStreamSupport()
                .addProgressBarWithColor(getResources().getColor(R.color.colorAccent))
                .createAndPrepare();

        mExoPlayerHelper.playerPlay();
    }

    private void setClickListeners() {

        backImgView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    private void initViews() {

        if (getIntent() != null) {
            url = getIntent().getStringExtra(Constants.INTENT_VALUES.VIDEO_URL);
        }
    }


    @Override
    public void onBackPressed() {
        /*if (player != null && player.onBackPressed()) {
            return;
        }*/
        super.onBackPressed();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mExoPlayerHelper.onActivityStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*if (player != null) {
            player.onPause();
        }*/
        mExoPlayerHelper.onActivityPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if (player != null) {
            player.onResume();
        }*/
        mExoPlayerHelper.onActivityResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /*if (player != null) {
            player.onDestroy();
        }*/
        mExoPlayerHelper.onActivityDestroy();
    }


}
