package com.app.dinasuvadunews.Activities;

import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;


import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.app.dinasuvadunews.Helpers.Utilities;

import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

class BaseWhiteActivity extends AppCompatActivity {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utilities.setSystemBarTheme(BaseWhiteActivity.this, false);
        Utilities.setStatusBarColor(BaseWhiteActivity.this, Color.WHITE);
    }

    public void onStart() {
        super.onStart();
        ButterKnife.bind(this);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }


}
