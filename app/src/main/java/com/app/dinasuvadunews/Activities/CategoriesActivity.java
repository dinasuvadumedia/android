package com.app.dinasuvadunews.Activities;


import android.os.Build;

import android.os.Bundle;


import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.dinasuvadunews.Adapters.ChooseCategoriesListAdapter;
import com.app.dinasuvadunews.Adapters.SeasonalListAdapter;
import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.ApiCalls.UrlHelpers;
import com.app.dinasuvadunews.Helpers.AppSettings;
import com.app.dinasuvadunews.Helpers.Constants;
import com.app.dinasuvadunews.Helpers.SpacesItemDecoration;
import com.app.dinasuvadunews.Helpers.Utilities;
import com.app.dinasuvadunews.Interfaces.CommonClickListener;
import com.app.dinasuvadunews.Models.AllCategories;
import com.app.dinasuvadunews.Models.CategoriesDetails;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.R;
import com.app.dinasuvadunews.ViewModel.CommonViewModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CategoriesActivity extends BaseWhiteActivity {


    private static CommonClickListener mcommonClickListener;
    @BindView(R.id.seasonalRecyclerView)
    RecyclerView seasonalRecyclerView;
    @BindView(R.id.categoriesRecyclerView)
    RecyclerView categoriesRecyclerView;

    ChooseCategoriesListAdapter chooseCategoriesListAdapter;
    SeasonalListAdapter seasonalListAdapter;
    CommonViewModel newsFragmentViewModel;
    AppSettings appSettings;
    private boolean isChangeHappend = false;
    private JSONArray getJsonFormat;

    public static void reloadTheApi(CommonClickListener commonClickListener) {
        mcommonClickListener = commonClickListener;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        ButterKnife.bind(this);


        initViews();
        setSeasionalAdapter();
        getCategoriesApi();


    }

    private void setCategoriesAdapter(List<CategoriesDetails> categories) {

        getJsonFormat = convertListToJson(categories);

        chooseCategoriesListAdapter = new ChooseCategoriesListAdapter(this, getJsonFormat);
        categoriesRecyclerView.setAdapter(chooseCategoriesListAdapter);

        chooseCategoriesListAdapter.setOnClickListener(new CommonClickListener() {
            @Override
            public void clickListener(int position) {

                isChangeHappend = true;

                try {
                    if (getJsonFormat.optJSONObject(position).optInt(Constants.API_PARAMS.IS_SELECTED) == 1)
                        getJsonFormat.optJSONObject(position).put(Constants.API_PARAMS.IS_SELECTED, 0);
                    else
                        getJsonFormat.optJSONObject(position).put(Constants.API_PARAMS.IS_SELECTED, 1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                chooseCategoriesListAdapter.reloadTheList(getJsonFormat);

            }
        });

    }

    private JSONArray convertListToJson(List<CategoriesDetails> categories) {

        JSONArray jsonArray = new JSONArray();

        for (int i = 0; i < categories.size(); i++) {

            JSONObject jsonObject = new JSONObject();

            try {
                jsonObject.put(Constants.API_PARAMS.CATEGORY_ID, categories.get(i).getCategoryId());
                jsonObject.put(Constants.API_PARAMS.NAME, categories.get(i).getName());
                jsonObject.put(Constants.API_PARAMS.IS_SELECTED, categories.get(i).getIsSelected());

                jsonArray.put(jsonObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return jsonArray;
    }

    private void setSeasionalAdapter() {
        seasonalListAdapter = new SeasonalListAdapter(CategoriesActivity.this, new JSONArray());
        seasonalRecyclerView.setAdapter(seasonalListAdapter);
        seasonalRecyclerView.addItemDecoration(new SpacesItemDecoration(40, 0));
    }


    private void initViews() {
        LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        seasonalRecyclerView.setLayoutManager(linearLayoutManagerVertical);

        categoriesRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));

        newsFragmentViewModel = ViewModelProviders.of(this).get(CommonViewModel.class);
        appSettings = new AppSettings(CategoriesActivity.this);

    }

    public void onBackClick(View view) {

        handleBackProcess();
    }

    private void handleBackProcess() {

        if (isChangeHappend)
            updateTheCategories();
        else
            finish();

    }

    private void updateTheCategories() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.DEVICE_ID, appSettings.getUdid());
            jsonObject.put(Constants.API_PARAMS.CATEGORYS, getJsonFormat.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(CategoriesActivity.this);
        inputForAPI.setUrl(UrlHelpers.ADD_USER_CATEGORY);
        HashMap<String, String> header = new HashMap<>();
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getAddCategoriesResponse(inputForAPI).observe(this, new Observer<CommonSuccessResponseModel>() {
            @Override
            public void onChanged(@Nullable CommonSuccessResponseModel commonSuccessResponseModel) {

                if (commonSuccessResponseModel.getError().equalsIgnoreCase(getResources().getString(R.string.false_val))) {

                    mcommonClickListener.clickListener(0);
                    finish();
                } else
                    Utilities.showToast(CategoriesActivity.this, commonSuccessResponseModel.getErrorMessage());
            }

        });


    }


    private void getCategoriesApi() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put(Constants.API_PARAMS.DEVICE_ID, appSettings.getUdid());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        InputForAPI inputForAPI = new InputForAPI(CategoriesActivity.this);
        inputForAPI.setUrl(UrlHelpers.LIST_CATEGORY);
        HashMap<String, String> header = new HashMap<>();
        inputForAPI.setHeaders(header);
        inputForAPI.setJsonObject(jsonObject);

        newsFragmentViewModel.getAllCategoriesList(inputForAPI).observe(this, new Observer<AllCategories>() {
            @Override
            public void onChanged(@Nullable AllCategories allCategories) {

                if (allCategories.getError().equalsIgnoreCase(getResources().getString(R.string.false_val)))
                    setCategoriesAdapter(allCategories.getCategories());
                else
                    Utilities.showToast(CategoriesActivity.this, allCategories.getErrorMessage());
            }

        });


    }


    @Override
    public void onBackPressed() {
        handleBackProcess();
    }
}
