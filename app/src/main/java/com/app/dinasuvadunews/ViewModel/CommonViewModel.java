package com.app.dinasuvadunews.ViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.app.dinasuvadunews.ApiCalls.InputForAPI;
import com.app.dinasuvadunews.Models.AllCategories;
import com.app.dinasuvadunews.Models.AllNewsResponseModel;
import com.app.dinasuvadunews.Models.CommonSuccessResponseModel;
import com.app.dinasuvadunews.Models.GetAllRelatedNewsResponseModel;
import com.app.dinasuvadunews.Models.GetBloggersListResponseModel;
import com.app.dinasuvadunews.Models.GetCommentsListlResponseModel;
import com.app.dinasuvadunews.Models.GetNotificationsResponseModel;
import com.app.dinasuvadunews.Models.HomeCategoriesList;
import com.app.dinasuvadunews.Models.LoginResponseModel;
import com.app.dinasuvadunews.Models.MyBloggersDetailsListResponseModel;
import com.app.dinasuvadunews.Models.SingleBlogDetailsListResponseModel;
import com.app.dinasuvadunews.Models.VideosCategoriesList;
import com.app.dinasuvadunews.Models.VideosListResponseModel;
import com.app.dinasuvadunews.Models.ViralCategoriesList;
import com.app.dinasuvadunews.Models.ViralListResponseModel;
import com.app.dinasuvadunews.Repository.CommonFragmentRespository;

public class CommonViewModel extends AndroidViewModel {

    CommonFragmentRespository homeFragmentRespository;

    public CommonViewModel(@NonNull Application application) {
        super(application);
        homeFragmentRespository = new CommonFragmentRespository(application);
    }

    public LiveData<HomeCategoriesList> getHomeCategoriesList(InputForAPI inputs) {
        return homeFragmentRespository.getHomeCategoriesList(inputs);
    }

    public LiveData<AllCategories> getAllCategoriesList(InputForAPI inputs) {
        return homeFragmentRespository.getAllCategoriesList(inputs);
    }

    public LiveData<AllNewsResponseModel> getAllNewsAp(InputForAPI inputs) {
        return homeFragmentRespository.getAllNewsList(inputs);
    }


    public LiveData<VideosCategoriesList> getAllVideosCategories(InputForAPI inputs) {
        return homeFragmentRespository.getAllVideosCategoriesList(inputs);
    }

    public LiveData<VideosListResponseModel> getVideosList(InputForAPI inputForAPI) {

        return homeFragmentRespository.getVideosList(inputForAPI);

    }

    public LiveData<ViralCategoriesList> getAllViralCategories(InputForAPI inputs) {
        return homeFragmentRespository.getAllViralCategoriesList(inputs);
    }


    public LiveData<ViralListResponseModel> getViralList(InputForAPI inputForAPI) {
        return homeFragmentRespository.getViralList(inputForAPI);
    }

    public LiveData<CommonSuccessResponseModel> getAddCategoriesResponse(InputForAPI inputForAPI) {
        return homeFragmentRespository.getAddCategoryResponse(inputForAPI);
    }

    public LiveData<GetAllRelatedNewsResponseModel> getAllRelatedNews(InputForAPI inputForAPI) {
        return homeFragmentRespository.getAllRelatedNewsList(inputForAPI);
    }

    public LiveData<CommonSuccessResponseModel> getLikeNewsResponse(InputForAPI inputForAPI) {
        return homeFragmentRespository.getLikeNewsResponseData(inputForAPI);
    }

    public LiveData<LoginResponseModel> getLoginResponse(InputForAPI inputForAPI) {
        return homeFragmentRespository.getLoginResponseData(inputForAPI);
    }

    public LiveData<LoginResponseModel> getRegisterResponse(InputForAPI inputForAPI) {
        return homeFragmentRespository.getRegisterResponseData(inputForAPI);
    }

    public LiveData<GetCommentsListlResponseModel> getCommentsList(InputForAPI inputForAPI) {
        return homeFragmentRespository.getCommentsListResponseData(inputForAPI);
    }

    public LiveData<CommonSuccessResponseModel> sendComment(InputForAPI inputForAPI) {
        return homeFragmentRespository.getSendCommentResponseData(inputForAPI);
    }

    public LiveData<CommonSuccessResponseModel> getVideoLikeNewsResponse(InputForAPI inputForAPI) {
        return homeFragmentRespository.getVideoLikeNewsResponseData(inputForAPI);
    }

    public LiveData<GetNotificationsResponseModel> getNotificationsList(InputForAPI inputForAPI) {
        return homeFragmentRespository.getNotificationsList(inputForAPI);
    }

    public LiveData<CommonSuccessResponseModel> updateNotificationView(InputForAPI inputForAPI) {

        return homeFragmentRespository.updateNotificationView(inputForAPI);
    }


    public LiveData<CommonSuccessResponseModel> getDeviceTokenResponse(InputForAPI inputForAPI) {

        return homeFragmentRespository.getDeviceTokenResponseData(inputForAPI);
    }

    public LiveData<GetBloggersListResponseModel> getAllBloggersList(InputForAPI inputForAPI) {

        return homeFragmentRespository.getBloggersListResponseData(inputForAPI);

    }

    public LiveData<GetBloggersListResponseModel> getAddFollowStatus(InputForAPI inputForAPI) {

        return homeFragmentRespository.getAdddFollowResponseData(inputForAPI);

    }

    public LiveData<MyBloggersDetailsListResponseModel> getAllBloggerDetailsList(InputForAPI inputForAPI) {

        return homeFragmentRespository.getBlogDetailsResponseData(inputForAPI);
    }

    public LiveData<SingleBlogDetailsListResponseModel> getBloggerFullDetailsList(InputForAPI inputForAPI) {

        return homeFragmentRespository.getSingleBlogDetailsResponseData(inputForAPI);
    }

    public LiveData<AllNewsResponseModel> getMySearchList(InputForAPI inputForAPI) {

        return homeFragmentRespository.getMySearchListResponseData(inputForAPI);
    }
}
